<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Backend Toolbar HTML Event Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

/***********************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S TOOLBAR FUNCTIONALITY  */
/***********************************************************************/
class clsEventCalendarToolbarHTML {
	// Initialize variables

	// Create control panel menu
	static public function _CONTROLPANEL() {
		global $adminLanguage;

		mosMenuBar::startTable();
		mosMenuBar::custom( 'cp_save', 'save.png', 'save_f2.png', $adminLanguage->A_SAVE, false );
		mosMenuBar::spacer();
		mosMenuBar::custom( 'cp_apply', 'apply.png', 'apply_f2.png', $adminLanguage->A_APPLY, false );
		mosMenuBar::spacer();
		mosMenuBar::cancel( 'cp_cancel' );
		mosMenuBar::spacer();
        mosMenuBar::wiki( 'Event_Calendar_(component)' );
		mosMenuBar::endTable();
	}

	// Create event list menu
	static public function _DEFAULT() {
		mosMenuBar::startTable();
		mosMenuBar::publishList();
		mosMenuBar::spacer();
		mosMenuBar::unpublishList();
		mosMenuBar::spacer();
		mosMenuBar::addNew();
		mosMenuBar::spacer();
		mosMenuBar::editList();
		mosMenuBar::spacer();
		mosMenuBar::spacer();
		mosMenuBar::deleteList();
		mosMenuBar::spacer();
        mosMenuBar::wiki( 'Event_Calendar_(component)' );
		mosMenuBar::endTable();
	}

	// Create edit/add event form
	static public function _EDIT(){
		mosMenuBar::startTable();
		mosMenuBar::save();
		mosMenuBar::spacer();
		mosMenuBar::apply();
		mosMenuBar::spacer();
		mosMenuBar::cancel();
		mosMenuBar::spacer();
        mosMenuBar::wiki( 'Event_Calendar_(component)' );
		mosMenuBar::endTable();
	}
}
?>
