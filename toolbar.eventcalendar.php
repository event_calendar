<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Backend Toolbar Event Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Includes
require_once($mainframe->getCfg('absolute_path').'/administrator/components/com_eventcalendar/toolbar.eventcalendar.html.php'); // Component's html file for the administration area

// Intialize variables

// Display toolbar
switch ($task) {
	case 'cp':
		clsEventCalendarToolbarHTML::_CONTROLPANEL();
		break;
	case 'new':
		clsEventCalendarToolbarHTML::_EDIT();
		break;
	case 'edit':
		clsEventCalendarToolbarHTML::_EDIT();
		break;
	default:
		clsEventCalendarToolbarHTML::_DEFAULT();
		break;
}
?>
