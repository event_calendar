<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.
 *
 * Backend HTML Event Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Includes

/*****************************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S BACK-END HTML FUNCTIONALITY  */
/*****************************************************************************/
class clsEventCalendarAdminHTML {

	// Initialize variables
	
	/***************************/
	/*  Display control panel  */
	/***************************/ 
	static public function showControlPanelHTML( $config_values, $groups ) {
		global $objEventCalendar, $fmanager, $adminLanguage, $mainframe; // Pass global variables and objects

		// Load core HTML library
		mosCommonHTML::loadOverlib();
		?>

		<!-- Some more jscript functions -->
		<script type="text/javascript">
			function addTimeSpan() {
				if ((document.adminForm.addstart.value.match(/[0-9]?[0-9]:[0-9][0-9]/)) && (document.adminForm.addend.value.match(/[0-9]?[0-9]:[0-9][0-9]/))) {
					text = document.adminForm.addstart.value + " - " + document.adminForm.addend.value;
					value = document.adminForm.addstart.value + "-" + document.adminForm.addend.value;
					neu = new Option(text, value, false, false);
					listbox = document.getElementById( 'TimeTableSelect' );
					listbox.options[listbox.length] = neu;
				} else alert ( '<?php echo $objEventCalendar->lng->ALERT_WRONG_TIME_FORMAT; ?>' )
			}

			function submitbutton(task) {
				if (task == 'cp_remtemp') {
					listbox = document.getElementById('css_filename');
				
					if (listbox.value == 'default.css') {
						alert('<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_SFX_ND; ?>');
						return false;
					} else {
						var answer = confirm('<?php echo $objEventCalendar->lng->CP_MSG_TEMPLATE_REM_Q; ?>' + listbox.value + "?")
						
						if (answer == false) {
							return false;
						}
					}
				} else if (task == 'cp_addtemp') {
					userfile = document.getElementById('userfile');
					
					if (userfile.value == '') {
						alert('<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_SFX_NF; ?>');
						return false;
					}
				} else {				
					field = document.getElementById( 'timetable_selected' );
					fields = document.getElementById( 'TimeTableSelect' );
					for (i=0;i<fields.length;i++) {
						if (fields.options[i].selected == true) {
							field.value = i;
						} else {
							field.value = 0;
						}
						fields.options[i].selected = true;
					}
				}
				submitform(task);
			}
		</script>
		
		<!-- Screen header -->
		<table class="adminheading" width="100%">
			<tr>
				<th width="100%" style="background:url(<?php echo $objEventCalendar->live_apath; ?>/images/calendar.png) no-repeat" style="text-align:left;">
					<?php echo $objEventCalendar->lng->GEN_COMPONENT_TITLE; ?> <small>[<?php echo $objEventCalendar->lng->CP; ?>]</small>
				</th>
			</tr>
		</table>

		<form action="index2.php" method="post" name="adminForm" enctype="multipart/form-data">
			<input type="hidden" name="option" value="com_eventCalendar" />
			<input type="hidden" name="task" value="conf_save" />

		<table class="adminform" style="height: 400px;">
			<tr>
				<td width="50%" valign="top">
					<?php 
						$tabulator = new mosTabs( 0 ); 
						$tabulator->startPane( "Configure" );
						$tabulator->startTab( $objEventCalendar->lng->CP_TAB_GENERAL, "general" );
					?>
					<table width="100%">
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_WHO_POST; ?>:
							</td>
							<td>
								<?php 
									$row->access = $config_values->get('who_can_post_events', 30);
									$access = mosAdminMenus::Access( $row );
									$access = preg_replace("/access/", "who_can_post_events", $access );
									echo $access;
								?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_WHO_POST_TOOLTIP, $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_WHO_POST ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_WHO_EDIT; ?>:
							</td>
							<td>
								<?php 
									$row->access = $config_values->get('who_can_edit_events', 30);
									$access = mosAdminMenus::Access( $row );
									$access = preg_replace("/access/", "who_can_edit_events", $access );
									echo $access;
								?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_WHO_EDIT_TOOLTIP, $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_WHO_EDIT ); ?>
							</td>
						</tr>
					</table>
					<?php
						$tabulator->endTab();
						$tabulator->startTab( $objEventCalendar->lng->CP_TAB_DATEFORMAT, "dateformat" );
					?>
					<table width="100%">
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT; ?>:
							</td>
							<td>
								<?php 
									$dateformat[] = mosHTML::makeOption( "d-m-Y", $objEventCalendar->lng->CP_TAB_DATEFORMAT_f1 );
									$dateformat[] = mosHTML::makeOption( "m-d-Y", $objEventCalendar->lng->CP_TAB_DATEFORMAT_f2 );
									$dateformat[] = mosHTML::makeOption( "Y-m-d", $objEventCalendar->lng->CP_TAB_DATEFORMAT_f3 );
									echo mosHTML::selectList($dateformat, "date_format", "","value", "text", $config_values->get('date_format', "d-m-Y"));
								?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DATEFORMAT_f_TOOLTIP, $objEventCalendar->lng->CP_TAB_DATEFORMAT); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START; ?>:
							</td>
							<td>
								<?php 
									$weekdays[] = mosHTML::makeOption( "1", $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_MON );
									$weekdays[] = mosHTML::makeOption( "2", $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_TUE );
									$weekdays[] = mosHTML::makeOption( "3", $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_WED );
									$weekdays[] = mosHTML::makeOption( "4", $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_THU );
									$weekdays[] = mosHTML::makeOption( "5", $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_FRI );
									$weekdays[] = mosHTML::makeOption( "6", $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SUN );
									$weekdays[] = mosHTML::makeOption( "0", $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SAT );
									echo mosHTML::selectList($weekdays, "week_startingday", "","value", "text", $config_values->get('week_startingday', 0));
								?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_TOOLTIP, $objEventCalendar->lng->CP_TAB_DATEFORMAT_START); ?>
							</td>
						</tr>
					</table>
					<?php	
						$tabulator->endTab();
						$tabulator->startTab( $objEventCalendar->lng->CP_TAB_DISPLAY, "display" );
					?>
					<table width="100%">
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW; ?>:
							</td>
							<td>
								<?php 
									$rows[] = mosHTML::makeOption( "monthview", $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_MONTH );
									$rows[] = mosHTML::makeOption( "weekview", $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_WEEK );
									$rows[] = mosHTML::makeOption( "dayview", $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_DAY );
									echo mosHTML::SelectList( $rows, "default_view", "class='inputbox' size='3'", "value", "text", $config_values->get('default_view', 'monthview') );
								?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_TOOLTIP, $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_VIEW; ?>:
							</td>
							<td>
								<input type="radio" name="view_nav" value="1" <?php echo ($config_values->get('view_nav', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="view_nav" value="0" <?php echo ($config_values->get('view_nav', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_VIEW_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_VIEW ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_PRINT; ?>:
							</td>
							<td>
								<input type="radio" name="view_print" value="1" <?php echo ($config_values->get('view_print', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="view_print" value="0" <?php echo ($config_values->get('view_print', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_PRINT_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_PRINT ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_RSS; ?>:
							</td>
							<td>
								<input type="radio" name="view_rss" value="1" <?php echo ($config_values->get('view_rss', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="view_rss" value="0" <?php echo ($config_values->get('view_rss', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_RSS_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_NAV_RSS ); ?>
							</td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_RESERVATION; ?>:
							</td>
							<td>
								<input type="radio" name="view_reservation" value="1" <?php echo ($config_values->get('view_reservation', false))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="view_reservation" value="0" <?php echo ($config_values->get('view_reservation', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_RESERVATION_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_RESERVATION ); ?>
							</td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_WEEKNUM; ?>:
							</td>
							<td>
								<input type="radio" name="show_weeknumber" value="1" <?php echo ($config_values->get('show_weeknumber', false))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="show_weeknumber" value="0" <?php echo ($config_values->get('show_weeknumber', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_WEEKNUM_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_WEEKNUM ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_WEEKNUM_LINK; ?>:
							</td>
							<td>
								<input type="radio" name="week_number_links" value="1" <?php echo ($config_values->get('week_number_links', false))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="week_number_links" value="0" <?php echo ($config_values->get('week_number_links', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_WEEKNUM_LINK_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_WEEKNUM_LINK ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_PER; ?>:
							</td>
							<td>
								<input type="radio" name="view_periodicity" value="1" <?php echo ($config_values->get('view_periodicity', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="view_periodicity" value="0" <?php echo ($config_values->get('view_periodicity', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_PER_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_PER ); ?>
							</td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_VIEW; ?>:
							</td>
							<td>
								<input type="radio" name="view_catlist" value="1" <?php echo ($config_values->get('view_catlist', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="view_catlist" value="0" <?php echo ($config_values->get('view_catlist', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_VIEW_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_VIEW ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_COL; ?>:
							</td>
							<td>
								<?php
									$col_catlist[] = mosHTML::makeOption( "2", 2 );
									$col_catlist[] = mosHTML::makeOption( "3", 3 );
									$col_catlist[] = mosHTML::makeOption( "4", 4 );
									echo mosHTML::selectList($col_catlist, "col_catlist", "","value", "text", $config_values->get('col_catlist', 2));
								?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_COL_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_CAT_COL ); ?>
							</td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_DISPLAY_SFX; ?>:
							</td>
							<td>
								<?php
								$files = $fmanager->listFiles($objEventCalendar->hpath.'/templates/');
								foreach ($files as $file) {
									if (!is_dir($file) && ($file != '.') && ($file != '..')) {
										if ($file != 'index.html') {
											$css_filename[] = mosHTML::makeOption( $file, $file );
										}
									}
								}
								echo mosHTML::selectList($css_filename, "css_filename", "class='inputbox' size='5' style='width:327px; text-align: right;'", "value", "text", $config_values->get('css_filename', 'default.css'));
								?><br /><?php
								if (is_writable($objEventCalendar->hpath."/templates")) { ?>
									<input type="submit" value='-' class="button" title="<?php echo $adminLanguage->A_REMOVE; ?>" onClick="return submitbutton('cp_remtemp');" />&nbsp;|
									<input id="userfile" name="userfile" type="file" class="inputbox" />
									<input type="submit" value='+' class="button" title="<?php echo $adminLanguage->A_ADD; ?>" onClick="return submitbutton('cp_addtemp');" />
								<?php } else { 
									echo $objEventCalendar->lng->$CP_TAB_DISPLAY_SFX_NWP;
								} ?>
									
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_DISPLAY_SFX_TOOLTIP, $objEventCalendar->lng->CP_TAB_DISPLAY_SFX ); ?>
							</td>
						</tr>
					</table>
					<?php
						$tabulator->endTab();
						$tabulator->startTab( $objEventCalendar->lng->CP_TAB_TIMETABLE, "timetable" );
					?>
					<table width="100%">
						<tr>
							<td valign="top">
								<select name="timetable[]" id="TimeTableSelect" multiple="multiple" size="16" style="width:120px;">
								<?php 
									$timetable = unserialize( $config_values->get('timetable', 'a:1:{i:0;s:10:"0:00-23:59";}' ) );
									foreach ($timetable AS $timespan) {
										echo "<option>" . $timespan . "</option>";
									}
								?> 
								</select>
								<input type="hidden" name="timetable_selected" id="timetable_selected" />
								<script type="text/javascript">
									fields = document.getElementById( 'TimeTableSelect' );
									fields.options[<?php echo $config_values->get('timetable_selected', 0) ?>].selected = true;
								</script>
							</td>
							<td valign="top" style="border:1px solid #CFCFCF;padding:5px;text-align:left;">
								<br />
								<?php echo $objEventCalendar->lng->CP_TAB_TIMETABLE_STR; ?>: <input class="inputbox" type="text" name="addstart" value="" size="5" /><br /><br />
								<?php echo $objEventCalendar->lng->CP_TAB_TIMETABLE_END; ?>: <input class="inputbox" type="text" name="addend" value="" size="5" /><br /><br />
								<input type="button" name="addTimestart" value="<?php echo $objEventCalendar->lng->CP_TAB_TIMETABLE_ADD; ?>" onClick="addTimeSpan();" />
								<input type="button" name="addTimeend" value="<?php echo $objEventCalendar->lng->CP_TAB_TIMETABLE_DEL; ?>" onClick="document.getElementById('TimeTableSelect').options[document.getElementById('TimeTableSelect').selectedIndex] = null;" /><br />
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_TIMETABLE_TOOLTIP, $objEventCalendar->lng->CP_TAB_TIMETABLE ); ?>
							</td>
						</tr>
					</table>
					<?php
						$tabulator->endTab();
						$tabulator->startTab( $objEventCalendar->lng->CP_TAB_RSS, "rss" );
					?>
					<table width="100%">
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_CACHE; ?>:
							</td>
							<td>
								<input type="radio" name="rss_cache" value="1" <?php echo ($config_values->get('rss_cache', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="rss_cache" value="0" <?php echo ($config_values->get('rss_cache', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_CACHE_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_CACHE ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_CACHETIME; ?>:
							</td>
							<td>
								<input class="inputbox" type="text" name="rss_cachetime" value="<?php echo $config_values->get('rss_cachetime', '3600'); ?>" />
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_CACHE_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_CACHE ); ?>
							</td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_NUM; ?>:
							</td>
							<td>
								<input class="inputbox" type="text" name="rss_number" value="<?php echo $config_values->get('rss_number', '5'); ?>" />
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_NUM_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_NUM ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_TITLE; ?>:
							</td>
							<td>
								<input class="inputbox" type="text" size="25" name="rss_title" value="<?php echo $config_values->get('rss_title', 'EventCalendar RSS feeds'); ?>" />
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_TITLE_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_TITLE ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_DES; ?>:
							</td>
							<td>
								<textarea class="inputbox" name="rss_description" cols="30" rows="5"><?php echo $config_values->get('rss_description', 'EventCalendar web syndication.'); ?></textarea>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_TITLE_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_TITLE ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_MULTILANG; ?>:
							</td>
							<td>
								<input type="radio" name="rss_multilang" value="1" <?php echo ($config_values->get('rss_multilang', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="rss_multilang" value="0" <?php echo ($config_values->get('rss_multilang', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_MULTILANG_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_MULTILANG ); ?>
							</td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_IMG; ?>:
							</td>
							<td>
								<?php echo mosAdminMenus::Images('rss_img', $config_values->get('rss_img', 'elxis_rss.png'), '', '/images/M_images'); ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_IMG_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_IMG ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_IMGALT; ?>:
							</td>
							<td>
								<input type="text" name="rss_imgalt" class="inputbox" value="<?php echo ($config_values->get('rss_imgalt', "EventCalendar RSS feeds")); ?>" />
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_IMGALT_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_IMGALT ); ?>
							</td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_TEXTLIM ?>:
							</td>
							<td>
								<input type="radio" name="rss_limittext" value="1" <?php echo ($config_values->get('rss_limittext', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="rss_limittext" value="0" <?php echo ($config_values->get('rss_limittext', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_TEXTLIM_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_TEXTLIM ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_TEXTLEN ?>:
							</td>
							<td>
								<input type="text" name="rss_textlength" class="inputbox" value="<?php echo ($config_values->get('rss_textlength', '20')); ?>" />
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_TEXTLEN_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_TEXTLEN ); ?>
							</td>
						</tr>
						<tr><td colspan="3"><hr /></td></tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_RSS_LIVE; ?>:
							</td>
							<td>
								<input type="radio" name="rss_live" value="1" <?php echo ($config_values->get('rss_live', true))?'checked="checked"':""; ?> /><?php echo $objEventCalendar->lng->GEN_YES; ?> <input type="radio" name="rss_live" value="0" <?php echo ($config_values->get('rss_live', false))?"":'checked="checked"'; ?> /><?php echo $objEventCalendar->lng->GEN_NO; ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_RSS_LIVE_TOOLTIP, $objEventCalendar->lng->CP_TAB_RSS_LIVE ); ?>
							</td>
						</tr>
					</table>
					<?php
						$tabulator->endTab();
						
						if ( (file_exists($mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/')) && (!file_exists($mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/extensions/eventcalendar.sitemap.php')) )  {
							$tabulator->startTab( "IOS Sitemap", "sitemap" ); ?>
							

					<table width="100%">
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_SITEMAP_TEXT; ?>
								<input type="checkbox" name="sitemap_ext" id="sitemap_ext" /><?php echo $objEventCalendar->lng->CP_TAB_SITEMAP_CHECK; ?>
								<?php echo $objEventCalendar->lng->CP_TAB_SITEMAP_NOTE; ?>
							</td>
						</tr>
					</table>
						
							<?php $tabulator->endTab();
						}
						$tabulator->startTab( $objEventCalendar->lng->CP_TAB_PP, "paypal" );
					?>
					<table width="100%">
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_PP_MAIL; ?>:
							</td>
							<td>
								<input class="inputbox" type="text" value="<?php echo $config_values->get('pp_mail', 'mypaypal@email.com')?>" size="50" name="pp_mail" id="pp_mail" />
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_PP_MAIL_TOOLTIP, $objEventCalendar->lng->CP_TAB_PP_MAIL ); ?>
							</td>
						</tr>
						<tr>
							<td valign="top">
								<?php echo $objEventCalendar->lng->CP_TAB_PP_CURRENCY; ?>:
							</td>
							<td><?php
								$pp_curlist[] = mosHTML::makeOption( "AUD", "AUD - Australian Dollar" );
								$pp_curlist[] = mosHTML::makeOption( "CAD", "CAD - Canadian Dollar" );
								$pp_curlist[] = mosHTML::makeOption( "CHF", "CHF - Swiss Franc" );
								$pp_curlist[] = mosHTML::makeOption( "CZK", "CZK - Czech Koruna" );
								$pp_curlist[] = mosHTML::makeOption( "DKK", "DKK - Danish Krone" );
								$pp_curlist[] = mosHTML::makeOption( "EUR", "EUR - Euro" );
								$pp_curlist[] = mosHTML::makeOption( "GBP", "GBP - UK Pound" );
								$pp_curlist[] = mosHTML::makeOption( "HKD", "HKD - Hong Kong Dollar" );
								$pp_curlist[] = mosHTML::makeOption( "HUF", "HUF - Hugarian Fiorint" );
								$pp_curlist[] = mosHTML::makeOption( "JPY", "JPY - Japan Yen" );
								$pp_curlist[] = mosHTML::makeOption( "NOK", "NOK - Norwegian Krone" );
								$pp_curlist[] = mosHTML::makeOption( "NZD", "NZD - New Zealand Dollar" );
								$pp_curlist[] = mosHTML::makeOption( "PLN", "PLN - Polish New Zloty" );
								$pp_curlist[] = mosHTML::makeOption( "SEK", "SEK - Swedish Krona" );
								$pp_curlist[] = mosHTML::makeOption( "SGD", "SGD - Singapore Dollar" );
								$pp_curlist[] = mosHTML::makeOption( "USD", "USD - US Dollar" );	
								echo mosHTML::selectList($pp_curlist, "pp_curlist", "", "value", "text", $config_values->get('pp_curlist', "EUR")); ?>
							</td>
							<td align="right" valign="top">
								<?php echo mosToolTip( $objEventCalendar->lng->CP_TAB_PP_CURRENCY_TOOLTIP, $objEventCalendar->lng->CP_TAB_PP_CURRENCY ); ?>
							</td>
						</tr>
					</table>
					<?php
						$tabulator->endTab();
						$tabulator->endPane();
					?>
				</td>
				<td width="50%"></td>
			</tr>
		</table>
		</form>
		<?php
	}
		
	/************************/
	/*  Display event list  */
	/************************/ 
	static public function listEventsHTML($results, $pageNav, $catlist) {
		global $my, $adminLanguage, $objEventCalendar, $mosConfig_live_site; // Pass global variables and objects 
		?>
		<form action="index2.php" method="get" name="adminForm">
		<input type="hidden" name="option" value="com_eventcalendar" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="field" value="<?php echo mosGetParam( $_REQUEST, "field", "") ?>" />
		<input type="hidden" name="order" value="<?php echo mosGetParam( $_REQUEST, "order", "none") ?>" />

	<script type="text/javascript" src="<?php echo $objEventCalendar->live_apath; ?>/eventcalendar.ajax.js"></script>

		<table class="adminheading" width="100%">
			<tr>
				<th rowspan="2" style="background:url(<?php echo $objEventCalendar->live_apath ?>/images/calendar.png) no-repeat">
					<?php echo $objEventCalendar->lng->GEN_COMPONENT_TITLE; ?> <span style="font-size: small;" dir="ltr"> [<?php echo $objEventCalendar->lng->GEN_MANAGE_EVENTS ?>]</span>
				</th>

				<td align="right" valign="top"><?php echo $adminLanguage->A_FILTER ?>: &nbsp;</td>
				<td valign="top">
					<input name="search" value="<?php echo mosGetParam($_REQUEST, "search", "") ?>" class="inputbox" onchange="document.adminForm.submit();" type="text" />
				</td>
				<td align="right" valign="top">
					<?php echo $catlist; ?>
				</td>
			</tr>
		</table>

		<table class="adminlist">
			<tr>
				<th width="2%">#</th>
				<th><input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count($results); ?>);" /></th>
				<th width="30%" align="left"><?php echo clsEventCalendarAdminHTML::sortIcon("title") ?><?php echo $adminLanguage->A_TITLE ?></th>
				<th width="5%"><nobr><?php echo clsEventCalendarAdminHTML::sortIcon("published") ?><?php echo $adminLanguage->A_PUBLISHED ?></nobr></th>
				<th width="20%" align="left"><nobr><?php echo clsEventCalendarAdminHTML::sortIcon("catid") ?><?php echo $adminLanguage->A_CATEGORY ?></nobr></th>
				<th width="7%"><?php echo $adminLanguage->A_ID ?></th>
				<th width="10%"><?php echo clsEventCalendarAdminHTML::sortIcon("start_date") ?><?php echo $adminLanguage->A_START ?></th>
				<th width="10%"><?php echo clsEventCalendarAdminHTML::sortIcon("end_date") ?><?php echo $adminLanguage->A_END ?></th>
				<th width="10%" align="left"><?php echo $objEventCalendar->lng->LST_RECURSION ?></th>
				<th width="8%" align="left"><?php echo clsEventCalendarAdminHTML::sortIcon("language") ?><?php echo $adminLanguage->A_LANGUAGE ?></th>
			</tr>
			<?php 
			$toggle = true; 
			$count = -1;
			$unpublished = 0;

			foreach ($results AS $event) { 
				$count++;
				$toggle = ($toggle)?false:true;
				$cat_params = new mosParameters ($event->cat_params);
				?>
				<tr class="<?php echo ($toggle)?"row0":"row1" ?>">
					<td><?php echo $pageNav->rowNumber( $count ); ?></td>
					<td><?php echo mosCommonHTML::CheckedOutProcessing( $event, $count ); ?></td>
					<td>
						<?php
						if ( $event->checked_out && ( $event->checked_out != $my->id ) ) {
							echo '<b>' . htmlspecialchars( $event->title, ENT_QUOTES ) . '</b>';
						} else {
						?>
							<a href="index2.php?option=com_eventcalendar&task=edit&hidemainmenu=1&cid=<?php echo $event->id ?>" title="<?php echo $adminLanguage->A_EDIT ?>">
								<?php echo htmlspecialchars($event->title, ENT_QUOTES); ?>
							</a>
							<?php
						}
						?>
					</td>
					<td align="center">
						<?php if ($event->published == 1) {
							$img = 'publish_g.png';
							$alt = $adminLanguage->A_PUBLISHED;
						} else if ($event->published == 0){
							$img = 'publish_x.png';
							$alt = $adminLanguage->A_UNPUBLISHED;
						} else if ($event->published == -1) {
							$img = 'publish_y.png';
							$alt = $adminLanguage->A_UNPUBLISHED;						
						} ?>
						<div id="constatus<?php echo $count; ?>">
						<a href="javascript: void(0);" onclick="changeContentState('<?php echo $count; ?>', '<?php echo $event->id; ?>', '<?php echo (($event->published == 0) || ($event->published == -1)) ? 1 : 0; ?>'); return nd();">
							<img src="images/<?php echo $img; ?>" width="12" height="12" border="0" alt="<?php echo $alt; ?>" />
						</a>
						</div>

					</td>
					<td class="category" style="color:<?php echo $cat_params->get('color', '') ?>;">
						<?php echo $event->cat_name ?>
					</td>
					<td align="center"><?php echo $event->id ?></td>
					<td align="center"><?php echo $event->start_date ?></td>
					<td align="center"><?php echo $event->end_date ?></td>
					<td>
						<?php
						switch ($event->recur_type) {
							case "week":
								echo $objEventCalendar->lng->LST_REC_WEEK." ";
								echo (strrpos($event->recur_week,"1") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_MON;
								echo (strrpos($event->recur_week,"2") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_TUE;
								echo (strrpos($event->recur_week,"3") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_WED;
								echo (strrpos($event->recur_week,"4") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_THU;
								echo (strrpos($event->recur_week,"5") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_FRI;
								echo (strrpos($event->recur_week,"6") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SUN;
								echo (strrpos($event->recur_week,"0") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SAT;
								break;
							case "month":
								echo $objEventCalendar->lng->LST_REC_MONTH." ";
								if ($event->recur_month == 1) {
									echo $event->recur_month.$objEventCalendar->lng->LST_REC_MONTH_SUFFIX_1;
								} else if ($event->recur_month == 2) {
									echo $event->recur_month.$objEventCalendar->lng->LST_REC_MONTH_SUFFIX_2;
								} else {
									echo $event->recur_month.$objEventCalendar->lng->LST_REC_MONTH_SUFFIX_ALL;
								}
								break;
							case "year":
								echo $objEventCalendar->lng->LST_REC_YEAR." ".$event->recur_year_d."/".$event->recur_year_m;
								break;
							case "day":
							default:
								echo $objEventCalendar->lng->LST_REC_DAY;
								break;
						}
						if ($event->recur_count) {
							echo $objEventCalendar->lng->LST_FOR." ($event->recur_count ".$objEventCalendar->lng->LST_TIMES;
						}
						?>
					</td>
					<td>
						<?php 
						if (trim($event->language) != '') {
							$clangs = explode(',',$event->language);
							if (count($clangs) > 2) {
								echo count($clangs).' '.$adminLanguage->A_MENU_LANGUAGES;
							} else {
								foreach ($clangs as $clang) {
									if (trim($clang) != '') {
										echo '<img src="'.$mosConfig_live_site.'/language/'.$clang.'/'.$clang.'.gif" alt="'.$clang.'" title="'.$clang.'" border="0" /> ';
									}
								}
							}
						} else {
							echo '<img src="images/flag_un.gif" alt="'.$adminLanguage->A_ALL.'" title="'.$adminLanguage->A_ALL.'" border="0" />';
						}
						?>
					</td>
				</tr>
			<?php
			}
			if ($unpublished) {
				?>
				<tr>
					<td colspan="10"></td>
				</tr>
			<?php
			}
			?>
		</table>
	<?php
		echo $pageNav->getListFooter();
	?>
	</form>
	<?php
	}
	
	/************************************************/
	/*  Return the the source-code for a sort-icon  */
	/************************************************/
	function sortIcon( $field ) {
		$valid_array = Array("option", "field", "state", "published", "catid");
		
		if ( mosGetParam( $_REQUEST, "field", "") == $field)
			$state = mosGetParam( $_REQUEST, "order", NULL);
		
		$params = array_keys( $_REQUEST );
		$base = "";

		for ( $i=0; $i < count( $_REQUEST ); $i++ ) {
			if ( in_array($params[$i], $valid_array) ) {
				$base = $base . "&" . $params[$i] . "=" . $_REQUEST[$params[$i]];
			}
		}

		$base = "index2.php?" . substr( $base, 1 );

		if (isset($state)) {
			return mosHTML::sortIcon( $base, $field, $state ) . "&nbsp;";
		} else {
			return mosHTML::sortIcon( $base, $field ) . "&nbsp;";
		}
	}

	/****************************/
	/*  Display edit/add event  */
	/****************************/
	function editEventHTML($event = null) {
		global $objEventCalendar, $adminLanguage, $database, $mosConfig_lifetime, $my;
		
        //CSRF prevention
        $tokname = 'token'.$my->id;
		$mytoken = md5(uniqid(rand(), TRUE));
        $_SESSION[$tokname] = $mytoken;
        
		mosCommonHTML::loadOverlib();
		mosCommonHTML::loadCalendar();
?>
		<script type="text/javascript">
			function submitbutton(pressbutton, section) {
				var form = document.adminForm;
				if (pressbutton == 'cancel') {
					submitform( pressbutton );
					return;
				}

				if ( form.title.value == "" ) {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_TITLE ?>');
				} else if ( form.seotitle.value == "" ) {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_SEOTITLE ?>');
				} else if ( form.catid.value == "0" ) {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_CATEGORY ?>');
				} else if ( form.start_date.value == "" ) {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_START_DATE ?>');
				} else if ( form.end_date.value == "" ) {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_END_DATE ?>');
				} else {
					submitform(pressbutton);
				}
			}

			function changeColor() {
				var form = document.adminForm;
				<?php
				//get the total number of records
				$query = "SELECT * FROM #__categories WHERE section='com_eventcalendar'";
				$database->setQuery( $query );
				$results = $database->loadObjectList();
				
				foreach ($results as $cat) {
					$cat_par = new mosParameters($cat->params);
					echo 'var color'.$cat->id.' = "'.$cat_par->get('color', '#888888').'";';
				}
				?>
				switch(form.catid.value) {
					<?php
					foreach ($results as $cat) {
						echo 'case "'.$cat->id.'":';	?>
						form.color<?php echo (isset($event))?$event->id:"" ?>display.value = color<?php echo $cat->id ?>;
						form.color<?php echo (isset($event))?$event->id:"" ?>display.style.backgroundColor = color<?php echo $cat->id ?>;
						break;
					<?php
					}
					?>
				}
			}

			//JavaScript functions for recur-exception-adding/-removing:
			//adds an entry to the except-dates-list from the edit-form-field
			function addToList() {
				var dates = document.adminForm.dateexcept.value;
				var neu = new Option (dates, dates, false, false);
				document.adminForm.daten.options[document.adminForm.daten.options.length] = neu;
				document.adminForm.dateexcept.value = '';
				document.adminForm.dateexcept.focus();
				writeHiddenEntry();
			}

			//removes an entry from the excepts-list
			function remList() {
				document.adminForm.daten.options[document.adminForm.daten.selectedIndex] = null;
				writeHiddenEntry();
			}

			//writes hidden entries for the except-dates | here the dates are stored in raw / as timestamp
			function writeHiddenEntry() {
				document.adminForm.recur_except.value = '';
				var i = 0;
				for(i=0;i<document.adminForm.daten.length;i++) {
					var nextEntry = document.adminForm.daten.options[i].value;
					document.adminForm.recur_except.value = document.adminForm.recur_except.value + ',' + nextEntry;
				}
			}
			
			//show or hide recursion options
			function recurChange(index) {
				trWeek = document.getElementById('weekly');
				trMonth = document.getElementById('monthly');
				trYear = document.getElementById('yearly');
				
				if (index == 0) {
					trWeek.style.visibility = 'collapse';
					trMonth.style.visibility = 'collapse';
					trYear.style.visibility = 'collapse';
				} else if (index == 1) {
					trWeek.style.visibility = 'visible';
					trMonth.style.visibility = 'collapse';
					trYear.style.visibility = 'collapse';
				} else if (index == 2) {
					trWeek.style.visibility = 'collapse';
					trMonth.style.visibility = 'visible';
					trYear.style.visibility = 'collapse';
				} else if (index == 3) {
					trWeek.style.visibility = 'collapse';
					trMonth.style.visibility = 'collapse';
					trYear.style.visibility = 'visible';
				} else {
					<?php getEditorContents( 'editor1', 'description' ); ?>
					submitform(pressbutton);
			}
			}
		</script>
		<!-- include AJAX scripts -->
		<script type="text/javascript" src="<?php echo $objEventCalendar->live_apath; ?>/eventcalendar.ajax.js"></script>
		<!-- include the colorPicker -->
		<script src="<?php echo $objEventCalendar->live_hpath ?>/includes/js/colorpicker.js" type="text/javascript"></script>
		
		<div class="countdown">
			<?php echo $adminLanguage->A_TIMESESSEXP; ?>: <span id="countdown"></span>
		</div>

		<div id="colorPicker" style="position:absolute;border:solid 1px #000000;width:140px;height:129px;visibility:hidden;"></div>

		<form action="index2.php" method="post" name="adminForm">
		<input type="hidden" name="option" value="com_eventcalendar" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="id" value="<?php echo (isset($event))?$event->id:""; ?>" />
		<input type="hidden" name="<?php echo $tokname; ?>" value="<?php echo $mytoken; ?>" autocomplete="off" />

			<div id="Calendar" class="overdiv"></div>

			<table width="100%">
				<tr>
					<td colspan="2">
						<table class="adminheading">
							<tr>
								<th  style="background:url(<?php echo $objEventCalendar->live_apath ?>/images/calendar.png) no-repeat">
									<?php echo $objEventCalendar->lng->GEN_COMPONENT_TITLE ?>:
									<?php 
									switch ( $objEventCalendar->task ) {
										case 'new':
											echo $adminLanguage->A_NEW;
											break;
										case 'edit':
											echo $adminLanguage->A_EDIT
												.' <span style="font-size: small;" dir="ltr">[ '
												.$adminLanguage->A_CATEGORY
												.': '
												.$event->category.'
												]</span>';
											break;
									} ?>
								</th>
							</tr>
						</table>
					</td>
				</tr>
				<tr valign="top">
					<td width="60%">
						<table class="adminform">
							<tr>
								<th colspan="3" class="title"><?php echo $objEventCalendar->lng->EDT_DETAILS ?></th>
							</tr>
							<tr>
								<td><?php echo $adminLanguage->A_CATEGORY; ?>:</td>
								<td>
									<?php
										if (isset($event)) {
											$active = $event->catid;
											$cat_params = new mosParameters ($event->cat_params);
											$cat_color = $cat_params->get('color', '#888888');
										} else {
											$active = NULL;
											$cat_color = '#888888';
										}
										echo mosAdminMenus::ComponentCategory('catid','com_eventcalendar', $active, 'onChange="return changeColor()"');	?>
									<input class="inputbox" style="background-color:<?php echo $cat_color ?>;" type="text" size="7" name="color<?php echo (isset($event))?$event->id:""; ?>display" id="color<?php echo (isset($event))?$event->id:""; ?>display" value="<?php echo $cat_color ?>" onBlur="sendColor(this.value,'color<?php echo (isset($event))?$event->id:""; ?>display')" />&nbsp;<input class="button" type="button" value="..." onClick="ColorPickerA('color<?php echo (isset($event))?$event->id:""; ?>display');" />
									<input type="hidden" name="color[<?php echo (isset($event))?$event->id:""; ?>]" id="color<?php echo (isset($event))?$event->id:""; ?>" value="<?php echo $cat_color ?>" />
								</td>
								<td></td>
							</tr>
							<tr>
								<td><?php echo $adminLanguage->A_TITLE; ?>:</td>
								<td><input type="text" name="title" size="60" class="inputbox" value="<?php echo (isset($event))?htmlspecialchars( $event->title, ENT_QUOTES ):"" ?>" /></td>
								<td></td>
							</tr>
							<tr>
								<td valign="top"><?php echo $adminLanguage->A_SEOTITLE; ?>:</td>
								<td>
									<input type="text" name="seotitle" id="seotitle" dir="ltr" class="inputbox" value="<?php echo (isset($event))?$event->seotitle:""; ?>" size="30" maxlength="100" /><br />
									<a href="javascript:;" onclick="suggestSEO()"><?php echo $adminLanguage->A_SEOTSUG; ?></a> &nbsp; | &nbsp; 
									<a href="javascript:;" onclick="validateSEO()"><?php echo $adminLanguage->A_SEOTVAL; ?></a><br />
									<div id="valseo" style="height: 20px;"></div>                              
								</td>
								<td>
									<?php echo mosToolTip($adminLanguage->A_SEOTHELP, $adminLanguage->A_SEOTITLE); ?>
								</td>
							</tr>
							<tr>
								<td><?php echo $objEventCalendar->lng->EDT_START ?>:</td>
								<td>
									<input type="text" name="start_date" id="start_date" size="20" class="inputbox" value="<?php echo (isset($event))?$event->start_date:'' ?>" />
									&nbsp;<input type="button" value=" ... " class="button" onclick="return showCalendar('start_date')" />
								</td>
								<td>
									<?php echo mosToolTip($objEventCalendar->lng->EDT_START_END_TOOLTIP, $objEventCalendar->lng->EDT_START); ?>
								</td>
							</tr>
							<tr>
								<td><?php echo $objEventCalendar->lng->EDT_END ?>:</td>
								<td>
									<input type="text" name="end_date" id="end_date" size="20" class="inputbox" value="<?php echo (isset($event))?$event->end_date:'' ?>" />
									&nbsp;<input type="button" value=" ... " class="button" onclick="return showCalendar('end_date')" />
								</td>
								<td>
									<?php echo mosToolTip($objEventCalendar->lng->EDT_START_END_TOOLTIP, $objEventCalendar->lng->EDT_END); ?>
								</td>
							</tr>
							<tr>
								<td><?php echo $adminLanguage->A_PUBLISHED ?>:</td>
								<td colspan="2">
									<?php echo mosHTML::yesnoRadioList( 'published', '', intval( isset($event) && $event->published ) ); ?>
								</td>
								
							</tr>
							<tr>
								<td valign="top"><?php echo $adminLanguage->A_LANGUAGE ?>:</td>
								<td colspan="2">
									<?php echo mosAdminMenus::SelectLanguages( 'languages', (isset($event)?$event->language:''), $adminLanguage->A_ALL_LANGS ); ?>
								</td>
								
							</tr>
							<tr>
								<td valign="top"><?php echo $adminLanguage->A_DESCRIPTION ?>:</td>
								<td colspan="2">
									<?php
									//parameters : areaname, content, hidden field, width, height, rows, cols
									editorArea( 'editor1',  ((isset($event))?$event->description:'') , 'description', '450', '300', '60', '20' );
									?>
								</td>
							</tr>
						</table>
					</td>
					<td width="40%">
						<?php 
							$tabulator = new mosTabs( 0 ); 
							$tabulator->startPane( "Configure" );
							$tabulator->startTab( $objEventCalendar->lng->EDT_REPEAT_OPT, "repeat" );
						?>
						<table class="adminform">
							<tr valign="top">
								<td><?php echo $objEventCalendar->lng->EDT_REPEAT_TYPE ?>:</td>
								<td>
									<select name="recur_type" size="1" class="selectbox" onchange="recurChange(this.selectedIndex)">
										<option value="day" <?php echo (isset($event) && $event->recur_type == 'day' || !isset($event))?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->LST_REC_DAY ?></option>
										<option value="week" <?php echo (isset($event) && $event->recur_type == 'week')?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->LST_REC_WEEK ?></option>
										<option value="month" <?php echo (isset($event) && $event->recur_type == 'month')?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->LST_REC_MONTH ?></option>
										<option value="year" <?php echo (isset($event) && $event->recur_type == 'year')?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->LST_REC_YEAR ?></option>
									</select>
								</td>
								<td>
									<?php echo mosToolTip($objEventCalendar->lng->EDT_REPEAT_TYPE_TOOLTIP, $objEventCalendar->lng->EDT_REPEAT_TYPE) ?>
								</td>
							</tr>
							<tr valign="top" name="weekly" id="weekly" style="visibility: <?php echo ($event->recur_type == 'week')?'visible':'collapse'; ?>">
								<td><?php echo $objEventCalendar->lng->EDT_WEEKLY_OPT ?>:</td>
								<td>
									<select name="recur_week" size="7" multiple="multiple">
										<option value="1" <?php echo (isset($event) && strrpos($event->recur_week,'1') !== false || !isset($event))?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_MON ?></option>
										<option value="2" <?php echo (isset($event) && strrpos($event->recur_week,'2') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_TUE ?></option>
										<option value="3" <?php echo (isset($event) && strrpos($event->recur_week,'3') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_WED ?></option>
										<option value="4" <?php echo (isset($event) && strrpos($event->recur_week,'4') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_THU ?></option>
										<option value="5" <?php echo (isset($event) && strrpos($event->recur_week,'5') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_FRI ?></option>
										<option value="6" <?php echo (isset($event) && strrpos($event->recur_week,'6') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SUN ?></option>
										<option value="0" <?php echo (isset($event) && strrpos($event->recur_week,'0') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SAT ?></option>
									</select>
								</td>
								<td>
									<?php echo mosToolTip($objEventCalendar->lng->EDT_WEEKLY_OPT_TOOLTIP, $objEventCalendar->lng->EDT_WEEKLY_OPT); ?>
								</td>
							</tr>
							<tr valign="top" name="monthly" id="monthly" style="visibility: <?php echo ($event->recur_type == 'month')?'visible':'collapse'; ?>">
								<td><?php echo $objEventCalendar->lng->EDT_MONTHLY_OPT ?>:</td>
								<td>
									<select name="recur_month" size="1" class="selectbox">
										<option value="1" <?php echo (isset($event) && strrpos($event->recur_month,'1') !== false || !isset($event))?'selected="selected"':"" ?>>1</option>
										<option value="2" <?php echo (isset($event) && strrpos($event->recur_month,'2') !== false)?'selected="selected"':"" ?>>2</option>
										<option value="3" <?php echo (isset($event) && strrpos($event->recur_month,'3') !== false)?'selected="selected"':"" ?>>3</option>
										<option value="4" <?php echo (isset($event) && strrpos($event->recur_month,'4') !== false)?'selected="selected"':"" ?>>4</option>
										<option value="5" <?php echo (isset($event) && strrpos($event->recur_month,'5') !== false)?'selected="selected"':"" ?>>5</option>
										<option value="6" <?php echo (isset($event) && strrpos($event->recur_month,'6') !== false)?'selected="selected"':"" ?>>6</option>
										<option value="7" <?php echo (isset($event) && strrpos($event->recur_month,'7') !== false)?'selected="selected"':"" ?>>7</option>
										<option value="8" <?php echo (isset($event) && strrpos($event->recur_month,'8') !== false)?'selected="selected"':"" ?>>8</option>
										<option value="9" <?php echo (isset($event) && strrpos($event->recur_month,'9') !== false)?'selected="selected"':"" ?>>9</option>
										<option value="10" <?php echo (isset($event) && strrpos($event->recur_month,'10') !== false)?'selected="selected"':"" ?>>11</option>
										<option value="11" <?php echo (isset($event) && strrpos($event->recur_month,'11') !== false)?'selected="selected"':"" ?>>12</option>
										<option value="12" <?php echo (isset($event) && strrpos($event->recur_month,'12') !== false)?'selected="selected"':"" ?>>12</option>
										<option value="13" <?php echo (isset($event) && strrpos($event->recur_month,'13') !== false)?'selected="selected"':"" ?>>13</option>
										<option value="14" <?php echo (isset($event) && strrpos($event->recur_month,'14') !== false)?'selected="selected"':"" ?>>14</option>
										<option value="15" <?php echo (isset($event) && strrpos($event->recur_month,'15') !== false)?'selected="selected"':"" ?>>15</option>
										<option value="16" <?php echo (isset($event) && strrpos($event->recur_month,'16') !== false)?'selected="selected"':"" ?>>16</option>
										<option value="17" <?php echo (isset($event) && strrpos($event->recur_month,'17') !== false)?'selected="selected"':"" ?>>17</option>
										<option value="18" <?php echo (isset($event) && strrpos($event->recur_month,'18') !== false)?'selected="selected"':"" ?>>18</option>
										<option value="19" <?php echo (isset($event) && strrpos($event->recur_month,'19') !== false)?'selected="selected"':"" ?>>19</option>
										<option value="20" <?php echo (isset($event) && strrpos($event->recur_month,'20') !== false)?'selected="selected"':"" ?>>20</option>
										<option value="21" <?php echo (isset($event) && strrpos($event->recur_month,'21') !== false)?'selected="selected"':"" ?>>21</option>
										<option value="22" <?php echo (isset($event) && strrpos($event->recur_month,'22') !== false)?'selected="selected"':"" ?>>22</option>
										<option value="23" <?php echo (isset($event) && strrpos($event->recur_month,'23') !== false)?'selected="selected"':"" ?>>23</option>
										<option value="24" <?php echo (isset($event) && strrpos($event->recur_month,'24') !== false)?'selected="selected"':"" ?>>24</option>
										<option value="25" <?php echo (isset($event) && strrpos($event->recur_month,'25') !== false)?'selected="selected"':"" ?>>25</option>
										<option value="26" <?php echo (isset($event) && strrpos($event->recur_month,'26') !== false)?'selected="selected"':"" ?>>26</option>
										<option value="27" <?php echo (isset($event) && strrpos($event->recur_month,'27') !== false)?'selected="selected"':"" ?>>27</option>
										<option value="28" <?php echo (isset($event) && strrpos($event->recur_month,'28') !== false)?'selected="selected"':"" ?>>28</option>
										<option value="29" <?php echo (isset($event) && strrpos($event->recur_month,'29') !== false)?'selected="selected"':"" ?>>29</option>
										<option value="30" <?php echo (isset($event) && strrpos($event->recur_month,'30') !== false)?'selected="selected"':"" ?>>30</option>
										<option value="31" <?php echo (isset($event) && strrpos($event->recur_month,'31') !== false)?'selected="selected"':"" ?>>31</option>
									</select>
								</td>
								<td>
									<?php echo mosToolTip($objEventCalendar->lng->EDT_MONTHLY_OPT_TOOLTIP, $objEventCalendar->lng->EDT_MONTHLY_OPT); ?>
								</td>
							</tr>
							<tr valign="top" name="yearly" id="yearly" style="visibility: <?php echo ($event->recur_type == 'year')?'visible':'collapse'; ?>">
								<td><?php echo $objEventCalendar->lng->EDT_YEARLY_OPT ?>:</td>
								<td>
									<select name="recur_year_d" size="1" class="selectbox">
										<option value="1" <?php echo (isset($event) && strrpos($event->recur_year_d,'1') !== false || !isset($event))?'selected="selected"':"" ?>>1</option>
										<option value="2" <?php echo (isset($event) && strrpos($event->recur_year_d,'2') !== false)?'selected="selected"':"" ?>>2</option>
										<option value="3" <?php echo (isset($event) && strrpos($event->recur_year_d,'3') !== false)?'selected="selected"':"" ?>>3</option>
										<option value="4" <?php echo (isset($event) && strrpos($event->recur_year_d,'4') !== false)?'selected="selected"':"" ?>>4</option>
										<option value="5" <?php echo (isset($event) && strrpos($event->recur_year_d,'5') !== false)?'selected="selected"':"" ?>>5</option>
										<option value="6" <?php echo (isset($event) && strrpos($event->recur_year_d,'6') !== false)?'selected="selected"':"" ?>>6</option>
										<option value="7" <?php echo (isset($event) && strrpos($event->recur_year_d,'7') !== false)?'selected="selected"':"" ?>>7</option>
										<option value="8" <?php echo (isset($event) && strrpos($event->recur_year_d,'8') !== false)?'selected="selected"':"" ?>>8</option>
										<option value="9" <?php echo (isset($event) && strrpos($event->recur_year_d,'9') !== false)?'selected="selected"':"" ?>>9</option>
										<option value="10" <?php echo (isset($event) && strrpos($event->recur_year_d,'10') !== false)?'selected="selected"':"" ?>>11</option>
										<option value="11" <?php echo (isset($event) && strrpos($event->recur_year_d,'11') !== false)?'selected="selected"':"" ?>>12</option>
										<option value="12" <?php echo (isset($event) && strrpos($event->recur_year_d,'12') !== false)?'selected="selected"':"" ?>>12</option>
										<option value="13" <?php echo (isset($event) && strrpos($event->recur_year_d,'13') !== false)?'selected="selected"':"" ?>>13</option>
										<option value="14" <?php echo (isset($event) && strrpos($event->recur_year_d,'14') !== false)?'selected="selected"':"" ?>>14</option>
										<option value="15" <?php echo (isset($event) && strrpos($event->recur_year_d,'15') !== false)?'selected="selected"':"" ?>>15</option>
										<option value="16" <?php echo (isset($event) && strrpos($event->recur_year_d,'16') !== false)?'selected="selected"':"" ?>>16</option>
										<option value="17" <?php echo (isset($event) && strrpos($event->recur_year_d,'17') !== false)?'selected="selected"':"" ?>>17</option>
										<option value="18" <?php echo (isset($event) && strrpos($event->recur_year_d,'18') !== false)?'selected="selected"':"" ?>>18</option>
										<option value="19" <?php echo (isset($event) && strrpos($event->recur_year_d,'19') !== false)?'selected="selected"':"" ?>>19</option>
										<option value="20" <?php echo (isset($event) && strrpos($event->recur_year_d,'20') !== false)?'selected="selected"':"" ?>>20</option>
										<option value="21" <?php echo (isset($event) && strrpos($event->recur_year_d,'21') !== false)?'selected="selected"':"" ?>>21</option>
										<option value="22" <?php echo (isset($event) && strrpos($event->recur_year_d,'22') !== false)?'selected="selected"':"" ?>>22</option>
										<option value="23" <?php echo (isset($event) && strrpos($event->recur_year_d,'23') !== false)?'selected="selected"':"" ?>>23</option>
										<option value="24" <?php echo (isset($event) && strrpos($event->recur_year_d,'24') !== false)?'selected="selected"':"" ?>>24</option>
										<option value="25" <?php echo (isset($event) && strrpos($event->recur_year_d,'25') !== false)?'selected="selected"':"" ?>>25</option>
										<option value="26" <?php echo (isset($event) && strrpos($event->recur_year_d,'26') !== false)?'selected="selected"':"" ?>>26</option>
										<option value="27" <?php echo (isset($event) && strrpos($event->recur_year_d,'27') !== false)?'selected="selected"':"" ?>>27</option>
										<option value="28" <?php echo (isset($event) && strrpos($event->recur_year_d,'28') !== false)?'selected="selected"':"" ?>>28</option>
										<option value="29" <?php echo (isset($event) && strrpos($event->recur_year_d,'29') !== false)?'selected="selected"':"" ?>>29</option>
										<option value="30" <?php echo (isset($event) && strrpos($event->recur_year_d,'30') !== false)?'selected="selected"':"" ?>>30</option>
										<option value="31" <?php echo (isset($event) && strrpos($event->recur_year_d,'31') !== false)?'selected="selected"':"" ?>>31</option>
									</select>
									/
									<select name="recur_year_m" size="1" class="selectbox">
										<option value="1" <?php echo (isset($event) && strrpos($event->recur_year_m,'1') !== false)?'selected="selected"':"" ?>>1</option>
										<option value="2" <?php echo (isset($event) && strrpos($event->recur_year_m,'2') !== false)?'selected="selected"':"" ?>>2</option>
										<option value="3" <?php echo (isset($event) && strrpos($event->recur_year_m,'3') !== false)?'selected="selected"':"" ?>>3</option>
										<option value="4" <?php echo (isset($event) && strrpos($event->recur_year_m,'4') !== false)?'selected="selected"':"" ?>>4</option>
										<option value="5" <?php echo (isset($event) && strrpos($event->recur_year_m,'5') !== false)?'selected="selected"':"" ?>>5</option>
										<option value="6" <?php echo (isset($event) && strrpos($event->recur_year_m,'6') !== false)?'selected="selected"':"" ?>>6</option>
										<option value="7" <?php echo (isset($event) && strrpos($event->recur_year_m,'7') !== false)?'selected="selected"':"" ?>>7</option>
										<option value="8" <?php echo (isset($event) && strrpos($event->recur_year_m,'8') !== false)?'selected="selected"':"" ?>>8</option>
										<option value="9" <?php echo (isset($event) && strrpos($event->recur_year_m,'9') !== false)?'selected="selected"':"" ?>>9</option>
										<option value="10" <?php echo (isset($event) && strrpos($event->recur_year_m,'10') !== false)?'selected="selected"':"" ?>>11</option>
										<option value="11" <?php echo (isset($event) && strrpos($event->recur_year_m,'11') !== false)?'selected="selected"':"" ?>>12</option>
										<option value="12" <?php echo (isset($event) && strrpos($event->recur_year_m,'12') !== false)?'selected="selected"':"" ?>>12</option>
									</select>
								</td>
								<td>
									<?php echo mosToolTip($objEventCalendar->lng->EDT_YEARLY_OPT_TOOLTIP, $objEventCalendar->lng->EDT_YEARLY_OPT); ?>
								</td>
							</tr>
						</table>
						<?php	
							$tabulator->endTab();
							$tabulator->startTab( $objEventCalendar->lng->EDT_EXCEPT, "except" );
						?>
						<table>
							<tr>
								<td><?php echo $objEventCalendar->lng->EDT_EXCEPT_ADD ?>:</td>
								<td>
									<input style="margin-top:6px;" type="text" size="10" name="dateexcept" id="exceptions" class="inputbox"/>
									&nbsp;<input type="button" value="  ... " onclick="return showCalendar('exceptions','dd.mm.yyyy')" class="button" />
									&nbsp;&nbsp;<input type="button" value=" + " onclick="addToList()" class="button" /><input type="button" value=" - " onclick="remList()" class="button" />
								</td>
								<td>
									<?php echo mosToolTip("If you have got a repeated event and at some of the dates there are for example holidays and you do not want the event to take place, you may define an exception and add it to the exception-list.","exception-dates");?>
								</td>
							</tr>
							<tr valign="top">
								<td><?php echo $objEventCalendar->lng->EDT_EXCEPT_LIST ?>:</td>
								<td colspan="2">
									<select size="10" class="selectbox" style="width:173px;" name="daten">
									<?php 
										if (isset($event) && @$event->recur_except) {
											$except_dates = split("\n",$event->recur_except);
											$exceptdates = "";
											foreach ($except_dates AS $except) {
												if ($except > 1) {
													echo '<option value="'.$except.'">'.$except.'</option>';
													$exceptdates .= ','.$except;
												}
											}
										}
									?>
									</select> 
									<input type="hidden" name="recur_except" value="<?php echo (isset($event) && @$event->recur_except)?$exceptdates:"" ?>" />
								</td>
							</tr>
						</table>
						<?php	
							$tabulator->endTab();
							$tabulator->startTab( $objEventCalendar->lng->EDT_INFO, "info" );
						?>
						<table class="adminform">
							<tr>
								<td><?php echo $objEventCalendar->lng->EDT_INFO_PERSON ?></td>
								<td><input class="inputbox" type="text" size="50" name="contact" value="<?php echo (isset($event))?htmlspecialchars( $event->contact, ENT_QUOTES ):'' ?>" /></td>
								<td><?php echo mosToolTip($objEventCalendar->lng->EDT_INFO_PERSON_TOOLTIP, $objEventCalendar->lng->EDT_INFO_PERSON);?></td>
							</tr>
							<tr>
								<td><?php echo $objEventCalendar->lng->EDT_INFO_WEB ?></td>
								<td><input class="inputbox" type="text" size="50" name="url" value="<?php echo (isset($event))?$event->url:'' ?>" /></td>
								<td><?php echo mosToolTip($objEventCalendar->lng->EDT_INFO_WEB_TOOLTIP, $objEventCalendar->lng->EDT_INFO_WEB);?></td>
							</tr>
							<tr>
								<td><?php echo $objEventCalendar->lng->EDT_INFO_MAIL ?></td>
								<td><input class="inputbox" type="text" size="50" name="email" value="<?php echo (isset($event))?$event->email:'' ?>" /></td>
								<td><?php echo mosToolTip($objEventCalendar->lng->EDT_INFO_MAIL_TOOLTIP, $objEventCalendar->lng->EDT_INFO_MAIL);?></td>
							</tr>
						</table>
						<?php	
							$tabulator->endTab();
							$tabulator->startTab( $objEventCalendar->lng->CP_TAB_PP, "paypal" );
						?>
						<table class="adminform">
							<tr>
								<td><?php echo $objEventCalendar->lng->EDT_PP_PRICE ?>:</td>
								<td><input class="inputbox" type="text" size="10" name="pp_price" value="<?php echo (isset($event) AND ($event->pp_price))?htmlspecialchars( $event->pp_price, ENT_QUOTES ):'0.0' ?>" /></td>
								<td><?php echo mosToolTip($objEventCalendar->lng->EDT_PP_PRICE_TOOLTIP, $objEventCalendar->lng->EDT_PP_PRICE);?></td>
							</tr>
						</table>
						<?php	
							$tabulator->endTab();
							$tabulator->endPane();
						?>
					</td>
				</tr>
			</table>
		</form>
		
		<script type="text/javascript">
			//start session countdown
			function sessioncountdown(secs) {
				var cel = document.getElementById('countdown');
				if (secs > 0) {
					var dmins = Math.ceil(secs/60);
					var text = '';
					if (dmins > 1) {
						text = '<strong>'+dmins+'</strong> <?php echo $adminLanguage->A_MINUTES; ?>';
					} else if (secs == 60) {
						text = '<strong>1</strong> <?php echo $adminLanguage->A_MINUTE; ?>';
					} else if (secs > 30) {
						text = '<strong>'+secs+'</strong> <?php echo $adminLanguage->A_SECONDS; ?>';
					} else if (secs > 1) {
						text = '<span style="color:red;"><strong>'+secs+'</strong> <?php echo $adminLanguage->A_SECONDS; ?></span>';
					} else if (secs == 1) {
						text = '<span style="color:red;"><strong>1</strong> <?php echo $adminLanguage->A_SECOND; ?></span>';
					}
					cel.innerHTML = text;
					secs = secs -1;
					setTimeout("sessioncountdown("+secs+")",1000);				
				} else {
					cel.innerHTML = '<span style="color: red; font-weight: bold;"><?php echo $adminLanguage->A_SESSEXPIRED; ?></span>';
				}
			}

			sessioncountdown('<?php echo $mosConfig_lifetime; ?>');
		</script>
	<?php }
} ?>
