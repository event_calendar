<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Sitemap extension
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link			
 */

defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

if (!defined('EVCALBASE')) {
	global $_VERSION;
	if ($_VERSION->RELEASE > 2008) {
		define('EVCALBASE', 'events');
	} else {
		define('EVCALBASE', 'com_eventcalendar');
	}
}

class sitemap_eventcalendar {

	/***************/
	/* CONSTRUCTOR */
	/***************/
	public function __construct() {

	}

	/****************************/
	/* MENU ITEM TYPE VALIDATOR */
	/****************************/
	public function isOfType( $parent ) {
		if (strpos($parent->link, 'option=com_eventcalendar')) { return true; }
		return false;
	}


	/********************/
	/* RETURN NODE TREE */
	/********************/
	public function getTree($parent) {
		global $database, $xartis, $mainframe, $my;

		$query = "SELECT id FROM #__menu"
		."\n WHERE link = 'index.php?option=com_eventcalendar' AND published = '1'"
		."\n AND ((language IS NULL) OR (language LIKE '%".$xartis->maplang."%'))";
		$database->setQuery($query, '#__', 1, 0);
		$_Itemid = intval($database->loadResult());

		$database->setQuery("SELECT * FROM #__categories WHERE section = 'com_eventcalendar' ORDER BY title ASC");
		$rows = $database->loadObjectList();

		$list = array();
		if ($rows) {
			foreach ($rows as $row) {
				$node = new stdClass;
				$node->name = $row->title;
				$node->link = 'index.php?option=com_eventcalendar&amp;task=catview&amp;catid='.$row->id;
				$node->seolink = EVCALBASE.'/'.$row->seotitle.'/';
				$node->id = $_Itemid;
				$node->modified = time() + ($mainframe->getCfg('offset') * 3600);
				$node->changefreq = 'daily';
				$node->priority = '0.8';
				$node->pid = 0;
				$node->icon = 'category';
		    	$list[$row->id] = $node;
		    	unset($node);
			}
		}

		return $list;
	}

}


global $xartis;
$tmp = new sitemap_eventcalendar;
$xartis->addExtension($tmp);
unset($tmp);

?>
