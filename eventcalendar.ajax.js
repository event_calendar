/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * AJAX handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link			
 *
 * ======================= NOTE =======================
 *
 * Based on Ioannis Sannos (datahell) code
 *
 */

function newobj() {
    var ro;
    if(window.XMLHttpRequest){ // Non-IE browsers
        ro = new XMLHttpRequest();
    } else if (window.ActiveXObject){ // IE
        ro=new ActiveXObject("Msxml2.XMLHTTP");
        if (!ro) {
            ro=new ActiveXObject("Microsoft.XMLHTTP");
        }
    }
    return ro;
}

var http = newobj();

var cajax = new sack();

/* GENERAL FUNCTIONS */
// Administrator
function whenLoadingcon(){
	var e = document.getElementById(cajax.element);
	e.innerHTML = "<img src='images/loading.gif' border='0'>";
}

function whenLoadedcon(){
	var e = document.getElementById(cajax.element);
	e.innerHTML = "<img src='images/loading.gif' border='0'>";
}

function whenInteractivecon(){
	var e = document.getElementById(cajax.element);
	e.innerHTML = "<img src='images/loading.gif' border='0'>";
}
// Front-end
function whenLoadingconHTML(){
	var e = document.getElementById(cajax.element);
	e.innerHTML = "<img src='administrator/images/loading.gif' border='0'>";
}

function whenLoadedconHTML(){
	var e = document.getElementById(cajax.element);
	e.innerHTML = "<img src='administrator/images/loading.gif' border='0'>";
}

function whenInteractiveconHTML(){
	var e = document.getElementById(cajax.element);
	e.innerHTML = "<img src='administrator/images/loading.gif' border='0'>";
}

/* CHANGE PUBLISHING STATE */
function changeContentState(elem, id, state){
    ajelem = 'constatus'+elem;
	var e = document.getElementById(ajelem);
	e.style.display = "";

    cajax.setVar("option", 'com_eventcalendar');
    cajax.setVar("task", 'ajaxpub');
    cajax.setVar("elem", elem);
    cajax.setVar("id", id);
    cajax.setVar("state", state);

	cajax.requestFile = "index3.php";

	cajax.method = 'POST';
	cajax.element = ajelem;
	cajax.onLoading = whenLoadingcon;
	cajax.onLoaded = whenLoadedcon;
	cajax.onInteractive = whenInteractivecon;
	cajax.runAJAX();
}

/* VALIDATE SEO TITLE */
function validateSEO() {
    var seotitle = document.adminForm.seotitle.value;
    var coid = document.adminForm.id.value;
    var cocatid = document.adminForm.catid.options[document.adminForm.catid.selectedIndex].value;

	cajax.setVar("option", 'com_eventcalendar');
	cajax.setVar("task", 'validate');
	cajax.setVar("coid", coid);
	cajax.setVar("cocatid", cocatid);
	cajax.setVar("seotitle", seotitle);

	cajax.requestFile = "index3.php";

	cajax.method = 'POST';
	cajax.element = 'valseo';
	cajax.onLoading = whenLoadingcon;
	cajax.onLoaded = whenLoadedcon;
	cajax.onInteractive = whenInteractivecon;
	cajax.runAJAX();
}

/* SUGGEST SEO TITLE */
function suggestSEO() {
    var cotitle = document.adminForm.title.value;
    var coid = document.adminForm.id.value;
    var cocatid = document.adminForm.catid.options[document.adminForm.catid.selectedIndex].value;

    if (cotitle == '') {
        alert('Please write a title!');
    } else {
        var rnd = Math.random();
        try{
            http.open('POST', 'index3.php');
            http.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            http.setRequestHeader('charset', 'utf-8');
            http.onreadystatechange = showresultsug;
            http.send('option=com_eventcalendar&task=suggest&cotitle='+cotitle+'&coid='+coid+'&cocatid='+cocatid+'&rnd='+rnd);
        }
        catch(e){}
        finally{}
    }
}

/* SHOW SUGGESTION RESULTS */
function showresultsug() {
    var stitle = document.getElementById('seotitle');
	if(http.readyState == 4) {
		if(http.status!=200) {
			alert('Error, please retry'); 
		}
        var reply = http.responseText;
        var update = new Array();
        update = reply.split('|');
        if (update[1]==1) {
            stitle.value = update[2];
		} else {
		  alert(update[2]);
		}
	}
}

/* SET RESERVATIONS */
function showReserve(user_id, event_id, state){
    var e = document.getElementById('reserve');
	e.style.display = "";

	cajax.element = 'reserve';
	
    cajax.setVar("option", 'com_eventcalendar');
    cajax.setVar("task", 'ajaxrsv');
    cajax.setVar("elem", 'rerserve');
    cajax.setVar("user-id", user_id);
    cajax.setVar("eventid", event_id);
    cajax.setVar("state", state);

	cajax.requestFile = "index2.php";
	cajax.method = 'POST';

	cajax.onLoading = whenLoadingconHTML;
	cajax.onLoaded = whenLoadedconHTML;
	cajax.onInteractive = whenInteractiveconHTML;
	
	cajax.runAJAX();
}
