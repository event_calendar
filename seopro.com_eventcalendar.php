<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * SEO Pro Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

if (!defined('EVCALBASE')) {
	global $_VERSION;
	if ($_VERSION->RELEASE > 2008) {
		define('EVCALBASE', 'events');
	} else {
		define('EVCALBASE', 'com_eventcalendar');
	}
}

// Generate SEO Pro page
function seogen_com_eventcalendar($link) {
	global $mosConfig_live_site, $database;

	if (trim($link) == '') { return ''; }

	$database->setQuery( "SELECT params FROM #__components WHERE link = 'option=com_eventcalendar'" );
	$text = $database->loadResult();
	$config_values = new mosParameters( $text );

	$vars = array();
	$vars['option'] = 'com_eventcalendar';
	$vars['task'] = '';
	$vars['catid'] = '';
	$vars['eventid'] = '';
	$vars['year'] = '';
	$vars['month'] = '';
	$vars['week'] = '';
	$vars['day'] = '';
	$vars['Itemid'] = '';
	$half = preg_split('/[\?]/', $link);
	if (isset($half[1])) {
		$half2 = preg_split('/[\#]/', $half[1]);
		$parts = preg_split('/[\&]/', $half2[0], -1, PREG_SPLIT_NO_EMPTY);
		if (count($parts) >0) {
			foreach ($parts as $part) {
				list($x, $y) = preg_split('/[\=]/', $part, 2);
				$x = strtolower($x);
				$vars[$x] = $y;
			}
		}
	}
	
	// Get event SEO title
	if ($vars['eventid']) {
		$query = "SELECT seotitle FROM #__eventcalendar WHERE id='".$vars['eventid']."'";
		$database->setQuery($query, '#__', 1, 0);
		$eventseo = $database->loadResult();
	} else {
		$eventseo = '';
	}
	
	// Get categories SEO titles
	if ($vars['catid']) {							// categories are set
		$catid = split( " ", $vars['catid'] );
		if (count($catid) == 1) {
			$key = array_keys($catid);
			$catid = $catid[$key[0]];
		}
	} else {										// no category is set
		$catid = '';
	}
	$query = "SELECT seotitle FROM #__categories WHERE section='com_eventcalendar'";
		if (is_array( $catid )) {
			$query .= "\n AND id IN (" . implode( ",", $catid ) . ")";
		} else if ($catid) {
			$query .= "\n AND id = " .$catid;
		}
	$database->setQuery($query);
	$catseo = $database->loadResultArray();	
	
	// Generate SEO Pro page depending task
	switch ($vars['task']) {
		case 'save':
			return $mosConfig_live_site.'/'.EVCALBASE.'/editevent/save.html';
			break;
		case 'cancel':
			return $mosConfig_live_site.'/'.EVCALBASE.'/editevent/cancel.html';
			break;
		case 'editevent':
			return $mosConfig_live_site.'/'.EVCALBASE.'/editevent/'.( ($eventseo)?($eventseo.'.html'):'' );
			break;
		case 'eventview':
			return $mosConfig_live_site.'/'.EVCALBASE.'/'.$eventseo.'.html';
			break;			
		case 'monthview':
			return $mosConfig_live_site.'/'.EVCALBASE.'/'.(($catid)?implode("+", $catseo):'all').'/'.$vars['year'].'/'.$vars['month'].'/';
			break;
		case 'weekview':
			$vars['week'] = (strlen($vars['week']) == 1)?'0'.$vars['week']:$vars['week'];
			return $mosConfig_live_site.'/'.EVCALBASE.'/'.(($catid)?implode("+", $catseo):'all').'/'.$vars['year'].'/'.$vars['week'].'.html';
			break;
		case 'dayview':
			$vars['day'] = (strlen($vars['day']) == 1)?'0'.$vars['day']:$vars['day'];
			return $mosConfig_live_site.'/'.EVCALBASE.'/'.(($catid)?implode("+", $catseo):'all').'/'.$vars['year'].'/'.$vars['month'].'/'.$vars['day'].'.html';
			break;
		case 'catview':
			return  $mosConfig_live_site.'/'.EVCALBASE.'/'.(($catid)?implode("+", $catseo):'all').'/';
			break;
		case 'syndication':
			return  $mosConfig_live_site.'/'.EVCALBASE.'/syndication.html';
			break;
		default:
			return $mosConfig_live_site.'/'.EVCALBASE.'/';
			break;
	}
}

// Restore original page
function seores_com_eventcalendar($seolink='', $register_globals=0) { 
	global $database, $lang;

	$seolink = urldecode($seolink);
	$seolink = trim(preg_replace('/(&amp;)/', '&', $seolink));
	$link = preg_split('/[\?]/', $seolink);
	$itemsyn = intval(mosGetParam( $_SESSION, 'itemsyn', 0 ));
	
	$QUERY_STRING = array();
	
	$_GET['option'] = 'com_eventcalendar';
	$_REQUEST['option'] = 'com_eventcalendar';
	$QUERY_STRING[] = 'option=com_eventcalendar';

	if ( ($link[0] == EVCALBASE.'/') || ($link[0] == EVCALBASE) )  { 											// URL = ../events/ OR URL = ../events
		$_GET['catid'] = '';
		$_REQUEST['catid'] = '';
		$QUERY_STRING[] = 'catid=';

		$database->setQuery( "SELECT params FROM #__components WHERE link = 'option=com_eventcalendar'" );
		$text = $database->loadResult();
		$com_params = new mosParameters( $text );
		
		$task = $com_params->get('default_view', 'monthview');
		$_GET['task'] = $task;
		$_REQUEST['task'] = $task;
		$QUERY_STRING[] = 'task='.$task;

		$_GET['year'] = date("Y", time());
		$_REQUEST['year'] = date("Y", time());
		$QUERY_STRING[] = 'year=' . date("Y", time());
		
		$_GET['month'] = date("m", time());
		$_REQUEST['month'] = date("m", time());
		$QUERY_STRING[] = 'month=' . date("m", time());

		$_GET['day'] = date("d", time());
		$_REQUEST['day'] = date("d", time());
		$QUERY_STRING[] = 'day=' . date("d", time());
	} else {
		$parts = preg_split('/[\/]/', $link[0]);
		if ((!isset($parts[2])) && (substr($parts[1], -4) == 'html')) {		// URL = ../events/[$event_seotitle].html or ../events/syndication.html
			$eventseo = preg_replace('/(\.html)$/', '', $parts[1]);
			if ($eventseo == 'syndication') {
				$_GET['task'] = 'syndication';
				$_REQUEST['task'] = 'syndication';
				$QUERY_STRING[] = 'task=syndication';
			} else {
				$_GET['task'] = 'eventview';
				$_REQUEST['task'] = 'eventview';
				$QUERY_STRING[] = 'task=eventview';
				
				$query = "SELECT id FROM #__eventcalendar WHERE seotitle='".$eventseo."'";
				$database->setQuery($query, '#__', 1, 0);
				$eventid = $database->loadResult();
				
				if ($eventid) {
					$_GET['eventid'] = $eventid;
					$_REQUEST['eventid'] = $eventid;
					$QUERY_STRING[] = 'eventid='.$eventid;
				} else {
					pageNotFound();
				}
			}
		} else {
			switch ($parts[1]) {
				case 'editevent':
					if (isset($parts[2]) && ($parts[2] != '')) { 				// URL = ../events/editevent/[$event_seotitle].html
						$eventseo = preg_replace('/(\.html)$/', '', $parts[2]);
						if ($eventseo == 'save') {
							$_GET['task'] = 'save';
							$_REQUEST['task'] = 'save';
							$QUERY_STRING[] = 'task=save';
						} else if ($eventseo == 'cancel') {
							$_GET['task'] = 'cancel';
							$_REQUEST['task'] = 'cancel';
							$QUERY_STRING[] = 'task=cancel';
						} else {
							$_GET['task'] = 'editevent';
							$_REQUEST['task'] = 'editevent';
							$QUERY_STRING[] = 'task=editevent';

							$query = "SELECT id FROM #__eventcalendar WHERE seotitle='".$eventseo."'";
							$database->setQuery($query, '#__', 1, 0);
							$eventid = $database->loadResult();
							
							if ($eventid) {
								$_GET['eventid'] = $eventid;
								$_REQUEST['eventid'] = $eventid;
								$QUERY_STRING[] = 'eventid='.$eventid;
							} else {
								pageNotFound();
							}
						}
					} else {													// URL = ../events/editevent/
						$_GET['task'] = 'editevent';
						$_REQUEST['task'] = 'editevent';
						$QUERY_STRING[] = 'task=editevent';
					}
					break;
				default:														// URL = ../events/[$catids]/.. [NOTE $catids = '' for all categories]	
					$catseo = $parts[1];
					if ($catseo == 'all') {
						$query = "SELECT id FROM #__categories WHERE section='com_eventcalendar'";
					} else {
						$catseo = explode( "+", $parts[1] );
						if (count($catseo) == 1) {
							$query = "SELECT id FROM #__categories WHERE seotitle='".$catseo[0]."'";
						} else {
							$query = "SELECT id FROM #__categories WHERE seotitle IN (" . implode(",", $catseo) . ")";
						}
					}
					$database->setQuery($query, '#__');
					$catid = $database->loadResultArray();
					
					if ($catid) {
						$_GET['catid'] = implode("+", $catid);
						$_REQUEST['catid'] = implode("+", $catid);
						$QUERY_STRING[] = 'catid='.implode("+", $catid);
						
						if (!(isset($parts[2]) && ($parts[2] != ''))) {				// URL = ../events/[$catids]/
							$_GET['task'] = 'catview';
							$_REQUEST['task'] = 'catview';
							$QUERY_STRING[] = 'task=catview';
						} else {													// URL = ../events/[$catids]/YYYY/..
							$_GET['year'] = $parts[2];
							$_REQUEST['year'] = $parts[2];
							$QUERY_STRING[] = 'year='.$parts[2];
							
							if (isset($parts[3]) && ($parts[3] != '')) {
								if (strlen($parts[3]) > 2) { 							// URL = ../events/[$catids]/YYYY/WW.html
									$week = intval(preg_replace('/(\.html)$/', '', $parts[3]));
									$_GET['task'] = 'weekview';
									$_REQUEST['task'] = 'weekview';
									$QUERY_STRING[] = 'task=weekview';
									
									$_GET['week'] = $week;
									$_REQUEST['week'] = $week;
									$QUERY_STRING[] = 'week='.$week;
								} else {												// URL = ../events/[$catids]/YYYY/MM/..
									$_GET['month'] = $parts[3];
									$_REQUEST['month'] = $parts[3];
									$QUERY_STRING[] = 'month='.$parts[3];
									if (isset($parts[4]) && ($parts[4] != '')) {		// URL = ../events/[$catids]/YYYY/MM/DD.html
										$day = intval(preg_replace('/(\.html)$/', '', $parts[4]));
										$_GET['task'] = 'dayview';
										$_REQUEST['task'] = 'dayview';
										$QUERY_STRING[] = 'task=dayview';
										
										$_GET['day'] = $day;
										$_REQUEST['day'] = $day;
										$QUERY_STRING[] = 'day='.$day;
									} else {											// URL = ../events/[$catids]/YYYY/MM/
										$_GET['day'] = '01';
										$_REQUEST['day'] = '01';
										$QUERY_STRING[] = 'day=01';								
									}
								}
							} else {													// URL = ../events/[$catids]/YYYY/
								$_GET['task'] = 'monthview';
								$_REQUEST['task'] = 'monthview';
								$QUERY_STRING[] = 'task=monthview';
								
								$_GET['month'] = '01';
								$_REQUEST['month'] = '01';
								$QUERY_STRING[] = 'month=01';								

								$_GET['day'] = '01';
								$_REQUEST['day'] = '01';
								$QUERY_STRING[] = 'day=01';								
							}
						}
					} else {
						pageNotFound();
					}
					break;			
			} // close switch
		}
	}

	$query = "SELECT id FROM #__menu WHERE link='index.php?option=com_eventcalendar' AND published='1'"
			."\n AND ((language IS NULL) OR (language LIKE '%$lang%'))";

	if ($itemsyn) { $_Itemid = $itemsyn; }
	if (isset($_Itemid) && ($_Itemid > 0)) {
		$Itemid = $_Itemid;
	} else {
		$database->setQuery($query, '#__', 1, 0);
		$Itemid = intval($database->loadResult());
	}

	$_GET['Itemid'] = $Itemid;
	$_REQUEST['Itemid'] = $Itemid;
	$QUERY_STRING[] = 'Itemid='.$Itemid;

    $qs = '';
    if (count($QUERY_STRING) > 0) { $qs = implode('&',$QUERY_STRING); }
	if (trim($link[1]) != '') { $qs .= ($qs == '') ? $link[1] : '&'.$link[1]; }

    $_SERVER['QUERY_STRING'] = $qs;
	$_SERVER['REQUEST_URI'] = ($qs != '') ? '/index.php?'.$qs : '/index.php';
}

?>
