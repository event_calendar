<?php

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Event handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

if (!defined('EVCALBASE')) {
	global $_VERSION;
	if ($_VERSION->RELEASE > 2008) {
		define('EVCALBASE', 'events');
	} else {
		define('EVCALBASE', 'com_eventcalendar');
	}
}

/*********************************************************/
/*  THE CLASS THAT WILL CONTAIN THE EVENT FUNCTIONALITY  */
/*********************************************************/
class mosEventCalendar_Event extends mosDBTable {
	
	// Declare variables
	public $id;
	public $title;
	public $seotitle;
	public $language;
	public $description;
	public $contact;
	public $url;
	public $email;
	public $catid;
	public $published;
	public $start_date; 
	public $end_date;
	public $recur_type;
	public $recur_week;
	public $recur_month;
	public $recur_year_d;
	public $recur_year_m;
	public $recur_except;
	public $checked_out = '0';
	public $checked_out_time = '1979-12-19 00:00:00';
	public $rsv = '';
	public $pp_price = '';

	public $category;		// Extended fields not in eventcalendar-database
	public $cat_params;
	
	/*****************/
	/*  Constructor  */
	/*****************/
    public function __construct($database) {
        $this->mosDBTable( '#__eventcalendar', 'id', $database);
    }

	/******************************************************/
	/*  Implement load function to have additional stuff  */
	/*  like category and color loaded as well as type    */
	/******************************************************/
	function load( $oid = null ) {
	
		$k = $this->_tbl_key;
		if ($oid !== null) {
			$this->$k = $oid;
		}

		$oid = $this->$k;
		if ($oid === null) {
			return false;
		}

		$class_vars = get_class_vars( get_class( $this ) );
		foreach ($class_vars as $name => $value) {
			if (($name != $k) and ($name != "_db") and ($name != "_tbl") and ($name != "_tbl_key")) {
				$this->$name = $value;
			}
		}

		$query = "SELECT e.*, c.name AS category, c.params AS cat_params"
			. "\n FROM $this->_tbl e"
			. "\n LEFT JOIN #__categories c ON c.id = e.catid"
			. "\n WHERE e.$this->_tbl_key = $oid";
		$this->_db->setQuery( $query );

		return $this->_db->loadObject( $this );
	}

	/****************************************************/
	/*  Encode the color of a category from its params  */
	/****************************************************/
	function getColor() {
		$params = new mosParameters( $this->cat_params );
		return $params->get( 'color' , '');
	}

	/*******************************************/
	/*  Implement the database check function  */
	/*******************************************/
	function check( $error_msg ) {
		$error_msg = "";

		return true;
	}

	/*******************************************/
	/*  Implement the database store function  */
	/*******************************************/
	function store( $updateNulls=false ) {
		$k = $this->_tbl_key;

		//eliminate the unvalid params
		$this->category	= null;
		$this->cat_params = null;

		if ($this->$k) {
			$ret = $this->_db->updateObject( $this->_tbl, $this, $this->_tbl_key, $updateNulls );
		} else {
			$ret = $this->_db->insertObject( $this->_tbl, $this, $this->_tbl_key );
		}
		if( !$ret ) {
			$this->_error = strtolower( get_class( $this ) ) . '::store failed <br />' . $this->_db->getErrorMsg();
			return false;
		} else {
			return true;
		}
	}

	/******************************************/
	/*  Implement the database bind function  */
	/******************************************/
	//de- / encoding of some passed variables as dates to timestamps and arrays to strings is necessary
	function bind( $array, $ignore='' ) {
		global $database;

		if (!is_array( $array )) {
			$this->_error = strtolower(get_class( $this ))."::bind failed.";
			return false;
		}

		// recur_week array to string conversion
		if (array_key_exists( 'recur_week', $array )) {
			if (is_array( $array['recur_week'] )) {
				$array['recur_week'] = implode( $array['recur_week'] );
			}
			// Conversion to string, otherwise Joomla! will set it to an empty string when only sunday is selected
			$array['recur_week'] = (string) intval( $array['recur_week'] );
		}

		// recur exception needs to be imploded for saving as params-string in recur_excepts
		$recur_except = split( ',', $array['recur_except'] );
		// if the first item of the array is empty that item has to be deleted
		if (is_array( $recur_except ) && (count( $recur_except ) >= 1) && $recur_except[0] == '') {
			array_shift($recur_except);
		}

		$array['recur_except'] = isset( $recur_except ) ? implode( "\n", $recur_except ) : '';

		//check if events for this category have to be published by an administrator
		$category = new mosCategory ( $database );
		$category->load( $array['catid'] );
		$parameter = new mosParameters( $category->params );
		if ($parameter->get( 'autopublish' )) {
			$array['published'] = '1';
		}

		return mosBindArrayToObject( $array, $this, $ignore );
	}

	/***********************************************************/
	/*  Bind this object to an array of a raw database result  */
	/***********************************************************/
	function bindRaw( $array, $ignore='' ) {
		return mosBindArrayToObject( $array, $this, $ignore, null, false );
	}
}
