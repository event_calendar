<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Greek Language File
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 * 
 * Special thanx to 
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class clsEventCalendarLanguage {
	
	// Set translation variables
	public $INS_HEADER = 'Εγκατάσταση του eventCalendar <small><small>από τον Απόστολο Κουτσουλέλο</small></small>';
	public $INS_ERROR_MENU_PARAMS = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Δεν στάθηκε δυνατή η αποθήκευση των προκαθορισμένων παραμέτρων του component.';
	public $INS_ERROR_MENU_MAIN = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Κεντρικό Μενού] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_EVENTS = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Διαχείριση Εκδηλώσεων] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_ANNI = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Διαχείριση Επετείων] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_CATEGORIES = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Διαχείριση Κατηγοριών] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_CONFIGURATION = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το αντικείμενο μενού [Ρυθμίσεις] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_MENU_COMPONENTID = 'Σφάλμα κατά την ενημέρωση της βάσης δεδομένων. Το υπάρχον αντικείμενο μενού [componentid] δεν ενημερώθηκε.<br/>';
	public $INS_ERROR_SEOPRO = 'Σφάλμα κατά την αντιγραφή της SEO PRO επέκτασης στον φάκελο includes/seopro/. Παρακαλώ αντιγράψτε το και μετονομάστε το χειροκίνητα!<br/>';
	public $INS_ERROR_SITEMAP = 'Σφάλμα κατά την αντιγραφή της επέκτασης του IOS Sitemap στον φάκελο admnistrator/components/com_sitemap/extensions/. Παρακαλώ αντιγράψτε το χειροκίνητα!<br/>';
	public $INS_ERROR_NOTICE_TITLE = 'Παρατηρήσεις εγκατάστασης';
	public $INS_ERROR_NOTICE = 'Τα παραπάνω σφάλματα είναι δευτερεύοντα, κυρίως κάνουν το περιβάλλον χρήστη του eventCalendar φιλικότερο. Δεν χρειάζεται να ανησυχείτε για αυτά!';
	public $INS_TITLE = 'Event Calendar - Σύστημα διαχείρισης εκδηλώσεων για το Elxis CMS 2008.x και 2009.x+';
	public $INS_BODY = '<br/><b>Το Event Calendar εγκαταστάθηκε επιτυχώς</b><br/><br/>Το Event Calendar είναι ένα σύστημα διαχείρισης και δημοσίευσης εκδηλώσεων. Σας επιτρέπει να καταχωρείτε εκδηλώσεις, οι οποίες προβάλλονται στους τελικούς χρήστες μέσω ενός πίνακα, που θυμίζει επιτραπέζιο ημερολόγιο. Υποστηρίζει επαναλαμβανόμενες εκδηλώσεις, καθώς και ημερήσια, εβδομαδιαία και μηνιαία προβολή.<br/><br/><b>Οδηγίες χρήσης:</b> Παρακαλώ ανατρέξτε στο <a href="http://wiki.elxis.org" target="_blank">Elxis Wiki</a>::<a href="http://wiki.elxis.org/wiki/Event_Calendar_(component)" target="_blank">Event Calendar (component)</a>.<br/><br/>';
	public $GEN_COMPONENT_TITLE = 'Event Calendar';
	public $GEN_MANAGE_CATEGORIES = 'Διαχειριστής Κατηγοριών';
	public $GEN_MANAGE_EVENTS = 'Διαχειριστής Εκδηλώσεων';
	public $GEN_SUPPORT = 'Οδηγίες<br/>(Elxis Wiki)';
	public $GEN_YES = 'Ναι';
	public $GEN_NO = 'Όχι';
	public $CP = 'Πίνακας Ελέγχου';
	public $CP_TAB_GENERAL = 'Γενικά';
	public $CP_TAB_GENERAL_DEFAULT_VIEW = 'Προκαθορισμένη προβολή';
	public $CP_TAB_GENERAL_DEFAULT_VIEW_TOOLTIP = 'Μπορείτε να ορίσετε την μορφή της προβολής που θα επιλέγεται από το σύστημα, εάν δεν έχει οριστεί καμία.';
	public $CP_TAB_GENERAL_DEFAULT_VIEW_DAY = 'Ημερήσια';
	public $CP_TAB_GENERAL_DEFAULT_VIEW_WEEK = 'Εβδομαδιαία';
	public $CP_TAB_GENERAL_DEFAULT_VIEW_MONTH = 'Μηνιαία';
	public $CP_TAB_GENERAL_DEFAULT_WHO_POST = 'Επίπεδο πρόσβασης για ανάρτηση';
	public $CP_TAB_GENERAL_DEFAULT_WHO_POST_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα είναι επιτρεπτή η ανάρτηση εκδηλώσεων από public, registered ή ειδικούς χρήστες.';
	public $CP_TAB_GENERAL_DEFAULT_WHO_EDIT = 'Επίπεδο πρόσβασης για τροποποίηση';
	public $CP_TAB_GENERAL_DEFAULT_WHO_EDIT_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα είναι επιτρεπτή η τροποποίηση εκδηλώσεων από public, registered ή ειδικούς χρήστες.';
	public $CP_TAB_DATEFORMAT = 'Μορφή Ημερομηνίας';
	public $CP_TAB_DATEFORMAT_START = 'Πρώτη ημέρα της εβδομάδας';
	public $CP_TAB_DATEFORMAT_START_TOOLTIP = 'Μπορείτε να ορίστε την πρώτη ημέρα της εβδομάδας στην εβδομαδιαία και μηνιαία προβολή.';
	public $CP_TAB_DATEFORMAT_START_MON = 'Δευτέρα';
	public $CP_TAB_DATEFORMAT_START_TUE = 'Τρίτη';
	public $CP_TAB_DATEFORMAT_START_WED = 'Τετάρτη';
	public $CP_TAB_DATEFORMAT_START_THU = 'Πέμπτη';
	public $CP_TAB_DATEFORMAT_START_FRI = 'Παρασκευή';
	public $CP_TAB_DATEFORMAT_START_SUN = 'Σάββατο';
	public $CP_TAB_DATEFORMAT_START_SAT = 'Κυριακή';
	public $CP_TAB_DATEFORMAT_f1 = 'd-m-Y';
	public $CP_TAB_DATEFORMAT_f2 = 'm-d-Y';
	public $CP_TAB_DATEFORMAT_f3 = 'Y-m-d';
	public $CP_TAB_DATEFORMAT_f_TOOLTIP = 'Ορίστε την μορφή της εμφανιζόμενης ημερομηνίας, όπου Υ = έτος, m = μήνας, d = ημέρα.';
	public $CP_TAB_DISPLAY = 'Εμφάνιση';
	public $CP_TAB_DISPLAY_WEEKNUM = 'Εμφάνιση αριμθού εβδομάδων';
	public $CP_TAB_DISPLAY_WEEKNUM_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζεται ο αριθμός της εβδομάδας στην αρχή κάθε σειράς.';
	public $CP_TAB_DISPLAY_WEEKNUM_LINK = 'Εμφάνιση αριθμού εβδομάδων ως σύνδεσμο';
	public $CP_TAB_DISPLAY_WEEKNUM_LINK_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζονται οι αριθμοί των εβδομάδων ως σύνδεσμοι προς τις αντίστοιχες εβδομαδιαίες προβολές.';
	public $CP_TAB_DISPLAY_NAV_VIEW = 'Εμφάνιση πλοήγησης';
	public $CP_TAB_DISPLAY_NAV_VIEW_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζεται το menu πλοήγησης στην κορυφή του component.';
	public $CP_TAB_DISPLAY_NAV_PRINT = 'Εμφάνιση εικονιδίου εκτύπωσης';
	public $CP_TAB_DISPLAY_NAV_PRINT_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζεται ένα εικονίδιο εκτύπωσης στην προβολή εκδήλωσης.';
	public $CP_TAB_DISPLAY_NAV_RSS = 'Εμφάνιση εικονιδίου RSS';
	public $CP_TAB_DISPLAY_NAV_RSS_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζεται ένα εικονίδιο RSS στην κορυφή του component.';
	public $CP_TAB_DISPLAY_DAY_VIEW_ID = 'Εμφάνιση ταυτότητας ημέρας';
	public $CP_TAB_DISPLAY_DAY_VIEW_ID_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζεται η ταυτότητα της ημέρας (ανατολή/δύση ηλίου, φάση σελήνης, ονομαστικές εορτές).';
	public $CP_TAB_DISPLAY_DAY_VIEW_HISTORY = 'Εμφάνιση ιστορικής ταυτότητας ημέρας';
	public $CP_TAB_DISPLAY_DAY_VIEW_HISTORY_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζεται η ιστορική ταυτότητα της ημέρας.';
	public $CP_TAB_DISPLAY_CAT_VIEW = 'Εμφάνιση κατηγοριών';
	public $CP_TAB_DISPLAY_CAT_VIEW_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζονται οι κατηγορίες των εκδηλώσεων στο τέλος του component.';
	public $CP_TAB_DISPLAY_CAT_PER = 'Εμφάνιση περιοδικότητας';
	public $CP_TAB_DISPLAY_CAT_PER_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζεται η περιοδικότητα της εκδήλωσης στην προβολή της εκδήλωσης.';
	public $CP_TAB_DISPLAY_CAT_COL = 'Αριθμός στηλών κατηγοριών';
	public $CP_TAB_DISPLAY_CAT_COL_TOOLTIP = 'Μπορείτε να ορίστε των αριθμό των στηλών με τις οποίες θα εμφανίζονται οι κατηγορίες των εκδηλώσεων στο τέλος του component.';
	public $CP_TAB_DISPLAY_SFX = 'Component Template';
	public $CP_TAB_DISPLAY_SFX_TOOLTIP = 'Επιλέξτε το template του component.';
	public $CP_TAB_DISPLAY_SFX_NWP = 'Ο φάκελος <i>/components/com_eventcalendar/templates/</i> δεν <br/> έχει δικαιώματα εγγραφής!';
	public $CP_TAB_DISPLAY_SFX_ND = 'Δεν μπορείτε να αφαιρέσετε το προκαθορισμένο template!';
	public $CP_TAB_DISPLAY_SFX_NF = 'Πρέπει να ορίσετε ένα αρχείο template (zip) για αποστολή!';
	public $CP_TAB_DISPLAY_RESERVATION = 'Συμμετοχή χρηστών';
	public $CP_TAB_DISPLAY_RESERVATION_TOOLTIP = 'Επιλέξτε έαν θα εμφανίζεται το σύστημα συμμετοχής των χρηστών στις εκδηλώσεις.';
	public $CP_TAB_TIMETABLE = 'Χρονοδιάγραμμα';
	public $CP_TAB_TIMETABLE_STR = 'Χρόνος έναρξης διαγράμματος';
	public $CP_TAB_TIMETABLE_END = 'Χρόνος λήξης διαγράμματος';
	public $CP_TAB_TIMETABLE_ADD = 'Προσθήκη';
	public $CP_TAB_TIMETABLE_DEL = 'Διαγραφή';
	public $CP_TAB_TIMETABLE_TOOLTIP = 'Μπορείτε να ορίσετε το χρονοδιάγραμμα που θα εμφανίζεται στην ημερήσια προβολή εκδηλώσεων';
	public $CP_TAB_RSS = 'RSS';
	public $CP_TAB_RSS_CACHE = 'Προσωρινή μνήμη';
	public $CP_TAB_RSS_CACHE_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα αποθηκεύονται τα αρχεία δελτίωυ τύπου στην προσωρινή μνήμη.';
	public $CP_TAB_RSS_CACHETIME = 'Χρόνος προσωρινής μνήμης';
	public $CP_TAB_RSS_CACHETIME_TOOLTIP = 'Μπορείτε να ορίσετε τον χρόνο ανανέωσης της προσωρινής μνήμης (σε δευτερόλεπτα).';
	public $CP_TAB_RSS_NUM = 'Πλήθος αντικειμένων';
	public $CP_TAB_RSS_NUM_TOOLTIP = 'Μπορείτε να ορίσετε το πλήθος των εμφανιζόμενων εκδηλώσεων.';
	public $CP_TAB_RSS_TITLE = 'Τίτλος';
	public $CP_TAB_RSS_TITLE_TOOLTIP = 'Μπορείτε να ορίστε τον τίτλο των αρχείων δελτίου τύπου.';
	public $CP_TAB_RSS_DES = 'Περιγραφή';
	public $CP_TAB_RSS_DES_TOOLTIP = 'Μπορείτε να ορίστε την περιγραφή των αρχείων δελτίου τύπου.';
	public $CP_TAB_RSS_MULTILANG = 'Πολυγλωσσικά feeds';
	public $CP_TAB_RSS_MULTILANG_TOOLTIP = 'Μπορείτε να ορίσετε αν θα δημιουργούνται ξεχωριστά feeds για κάθε γλώσσα.';
	public $CP_TAB_RSS_LIVE = 'Live Bookmarks (for Firefox)';
	public $CP_TAB_RSS_LIVE_TOOLTIP = 'Μπορείτε να ορίσετε αν θα δημιουργούνται live bookmarks για τον Firefox.';
	public $CP_TAB_RSS_IMG = 'Εικόνα';
	public $CP_TAB_RSS_IMG_TOOLTIP = 'Μπορείτε να ορίσετε την εικόνα των RSS feeds.';
	public $CP_TAB_RSS_IMGALT = 'Εναλλακτικό κείμενο εικόνας';
	public $CP_TAB_RSS_IMGALT_TOOLTIP = 'Μπορείτε να ορίσετε το εναλλακτικό κείμενο της εικόνας των RSS feeds.';
	public $CP_TAB_RSS_TEXTLIM = 'Όριο κειμένου';
	public $CP_TAB_RSS_TEXTLIM_TOOLTIP = 'Μπορείτε να ορίσετε εάν θα εμφανίζεται όλο ή μέρος του κειμένου για κάθε εκδήλωση.';
	public $CP_TAB_RSS_TEXTLEN = 'Μήκος κειμένου';
	public $CP_TAB_RSS_TEXTLEN_TOOLTIP = 'Μπορείτε να ορίσετε το μήκος του κειμένου για κάθε εμφανιζόμενη εκδήλωση.';
	public $CP_TAB_SITEMAP_TEXT = 'Το IOS Sitemap component είναι εγκατεστημένο, αλλά η επέκταση του EventCalendar για το IOS Sitemap δεν είναι! Εάν επιθυμείτε να ενεργοποιήσετε την επέκταση, παρακαλώ τσεκάρετε το παρακάτω κουτάκι. Έχετε υπόψη σας ότι μετά την ενεργοποίηση, αυτή η επιλογή δεν θα είναι διαθέσιμη ξανά.<br/><br/>';
	public $CP_TAB_SITEMAP_CHECK = 'Ενεργοποίηση της επέκτασης για το IOS Sitemap';
	public $CP_TAB_SITEMAP_NOTE = '<br/><br/><b>ΠΡΟΣΟΧΗ: </b>Πρέπει να έχετε τα σωστά δικαιώματα (δικαίωμα εγγραφής στον κατάλογο επεκτάσεων του IOS Sitemap) για να ενεργοποιήσετε την επέκταση!';
	public $CP_TAB_PP = 'PayPal';
	public $CP_TAB_PP_MAIL = 'Το e-mail σας στο PayPal';
	public $CP_TAB_PP_MAIL_TOOLTIP = 'Η ηλεκτρονική διεύθυνσή σας στο PayPal.';
	public $CP_TAB_PP_CURRENCY = 'Νόμισμα';
	public $CP_TAB_PP_CURRENCY_TOOLTIP = 'Επιλέξτε το νόμισμά σας.';
	public $CP_MSG_CONFIGURATION_STORED = 'Event Calendar: Οι νέες ρυθμίσεις αποθηκεύτηκαν';
	public $CP_MSG_CONFIGURATION_ERROR = 'Event Calendar: Αποτυχία αποθήκευσης των νέων ρυθμίσεων';
	public $CP_MSG_TEMPLATE_REM_Q = 'Αφαίρεση του template ';
	public $CP_MSG_TEMPLATE_REM_OK = 'Event Calendar: Το template αφαιρέθηκε επιτυχώς!';
	public $CP_MSG_TEMPLATE_REM_ERROR = 'Event Calendar: Δεν επιλέχθηκε template!';
	public $CP_MSG_TEMPLATE_ADD_OK = 'Event Calendar: Το αρχείο template προστέθηκε επιτυχώς!';
	public $CP_MSG_TEMPLATE_ADD_ERROR = 'Event Calendar: Δεν επιλέχθηκε αρχείο template!';
	
	public $LST_ALL_EVENTS = 'Όλες οι εκδηλώσεις';
	public $LST_OLD_EVENTS = 'Παλιές εκδηλώσεις';
	public $LST_RECURSION = 'Επανάληψη';
	public $LST_REC_DAY = 'Κάθε μέρα';
	public $LST_REC_WEEK = 'Κάθε εβδομάδα την';
	public $LST_REC_MONTH = 'Κάθε μήνα την';
	public $LST_REC_MONTH_SUFFIX_1 = 'η';
	public $LST_REC_MONTH_SUFFIX_2 = 'η';
	public $LST_REC_MONTH_SUFFIX_ALL = 'η';
	public $LST_REC_YEAR = 'Κάθε έτος την';
	public $LST_REC_FOR = 'για';
	public $LST_REC_TIMES = 'φορές';
	public $EDT_SUBMIT = 'Υποβολή εκδήλωσης';
	public $EDT_DETAILS = 'Λεπτομέρειες';
	public $EDT_START = 'Έναρξη εκδήλωσης';
	public $EDT_END = 'Λήξη εκδήλωσης';
	public $EDT_START_END_TOOLTIP = 'Ορίστε τον χρόνο και την ώρα έναρξης και λήξης δημοσίευσης της εκδήλωσης, π.χ. 2009-12-31 15:30:00.';
	public $EDT_REPEAT_OPT = 'Επιλογές επανάληψης';
	public $EDT_REPEAT_TYPE = 'Περιοδικότητα';
	public $EDT_REPEAT_TYPE_TOOLTIP = 'Μπορείτε να επιλέξτε την περιοδικότητα της εκδήλωσης.';
	public $EDT_WEEKLY_OPT = 'Ημέρα εβδομαδιαίας περιοδικότητας';
	public $EDT_WEEKLY_OPT_TOOLTIP = 'Εάν η εκδήλωση εμφανίζεται μια φορά την εβδομάδα, επιλέξτε την ημέρα που θα εμφανίζεται.';
	public $EDT_MONTHLY_OPT = 'Ημέρα μηνιαίας περιοδικότητας';
	public $EDT_MONTHLY_OPT_TOOLTIP = 'Εάν η εκδήλωση εμφανίζεται μια φορά τoν μήνα, επιλέξτε την ημέρα που θα εμφανίζεται.';
	public $EDT_YEARLY_OPT = 'Ημέρα ετήσιας περιοδικότητας';
	public $EDT_YEARLY_OPT_TOOLTIP = 'Εάν η εκδήλωση εμφανίζεται μια φορά τo έτος, επιλέξτε την ημέρα και τον μήνα που θα εμφανίζεται.';
	public $EDT_EXCEPT = 'Εξαιρέσεις';
	public $EDT_EXCEPT_ADD = 'Προσθήκη εξαίρεσης';
	public $EDT_EXCEPT_LIST = 'Κατάλογος εξαιρέσεων';
	public $EDT_INFO = 'Πληροφορίες επικοινωνίας';
	public $EDT_INFO_PERSON = 'Υπεύθυνος εκδήλωσης';
	public $EDT_INFO_PERSON_TOOLTIP = 'Μπορείτε να ορίσετε το όνομα του υπεύθυνου της εκδήλωσης.';
	public $EDT_INFO_WEB = 'Ιστοσελίδα';
	public $EDT_INFO_WEB_TOOLTIP = 'Μπορείτε να ορίσετε την επίσημη ιστοσελίδα της εκδήλωσης.';
	public $EDT_INFO_MAIL = 'Επικοινωνία';
	public $EDT_INFO_MAIL_TOOLTIP = 'Μπορείτε να ορίσετε την επίσημη διεύθυνση ηλ. ταχυδρομείου της εκδήλωσης. Συνιστάται να επικοινωνήσετε πρώτα με τον υπεύθυνο της εκδήλωσης για να αποφευφθεί άσκοπη ηλεκτρονική κίνηση.';
	public $EDT_RSV = 'Συμμετοχή';
	public $EDT_RSV_YES = 'Έχετε ήδη δηλώσει συμμετοχή στην εκδήλωση!';
	public $EDT_RSV_NO = 'Δεν έχετε δηλώσει συμμετοχή στην εκδήλωση!';
	public $EDT_RSV_NONE = 'Καμία συμμετοχή ακόμη...';
	public $EDT_RSV_SUB = 'Δηλώστε συμμετοχή τώρα!!';
	public $EDT_RSV_CAN = 'Ακύρωση συμμετοχής';
	public $EDT_PP_BUY = 'Αγοράστε μέσω PayPal';
	public $EDT_PP_PRICE = 'Τιμή';
	public $EDT_PP_PRICE_TOOLTIP = 'Ορίστε την τιμή συμμετοχής.';
	public $EDT_MSG_STORED = 'Η εκδήλωση αποθηκεύτηκε επιτυχώς';
	public $EDT_MSG_ERROR = 'Αδυναμία αποθήκευσης της εκδήλωσης';
	public $ALERT_WRONG_TIME_FORMAT = 'Λανθασμένη μορφή ώρας για την έναρξη ή λήξη!\n Παρακαλώ χρησιμοποιήστε την hh:mm';
	public $ALERT_EDIT_NO_TITLE = 'Πρέπει να ορίσετε τίτλο για την εκδήλωση!';
	public $ALERT_EDIT_NO_SEOTITLE = 'Πρέπει να ορίσετε τίτλο SEO για την εκδήλωση!';
	public $ALERT_EDIT_NO_CATEGORY = 'Πρέπει να ορίσετε κατηγορία για την εκδήλωση!';
	public $ALERT_EDIT_NO_START_DATE = 'Πρέπει να ορίσετε ημερομηνία έναρξης της εκδήλωσης!';
	public $ALERT_EDIT_NO_START_TIME = 'Πρέπει να ορίσετε ώρα έναρξης της εκδήλωσης!';
	public $ALERT_EDIT_NO_END_DATE = 'Πρέπει να ορίσετε ημερομηνία λήξης της εκδήλωσης!';
	public $ALERT_EDIT_NO_END_TIME = 'Πρέπει να ορίσετε ώρα λήξης της εκδήλωσης!';
	public $FRONT_VIEW = 'Προβολή';
	public $FRONT_VIEW_CAT = 'Προβολή όλων των εκδηλώσεων για την κατηγορία';
	public $FRONT_VIEW_CAT_TITLE = 'Προβολή εκδηλώσεων ανά κατηγορία';
	public $FRONT_VIEW_DAY_YESTERDAY = 'χθες';
	public $FRONT_VIEW_DAY_TODAY = 'Σήμερα';
	public $FRONT_VIEW_DAY_TOMMOROW = 'αύριο';
	public $FRONT_VIEW_WEEK_NEXT = 'επομ.';
	public $FRONT_VIEW_WEEK_NEXT_TITLE = 'Επόμενη εβδομάδα';
	public $FRONT_VIEW_WEEK_PREV = 'προηγ.';
	public $FRONT_VIEW_WEEK_PREV_TITLE = 'Προηγούμενη εβδομάδα';

	// Just an empty constructor
	public function __construct() {
	}
}
?>
