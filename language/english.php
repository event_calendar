<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * English Language File
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */
 
// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

class clsEventCalendarLanguage {

	// Set translation variables
	public $INS_HEADER = 'Installation of eventCalendar <small><small>by Apostolos Koutsoulelos</small></small>';
	public $INS_ERROR_MENU_PARAMS = 'Error updating database. Unable to store component default parameters.';
	public $INS_ERROR_MENU_MAIN = 'Error updating database. Backend Menu Items (main menu) not updated.<br/>';
	public $INS_ERROR_MENU_EVENTS = 'Error updating database. Backend Menu Items (events) not updated.<br/>';
	public $INS_ERROR_MENU_ANNI = 'Error updating database. Backend Menu Items (anniversaries) not updated.<br/>';
	public $INS_ERROR_MENU_CATEGORIES = 'Error updating database. Backend Menu Items (categories) not updated.<br/>';
	public $INS_ERROR_MENU_CONFIGURATION = 'Error updating database. Backend Menu Items (configuration) not updated.<br/>';
	public $INS_ERROR_MENU_COMPONENTID = 'Error updating database. Existing Menu Items (componentid) not updated.<br/>';
	public $INS_ERROR_SEOPRO = 'Error coping SEO PRO extension to folder includes/seopro/. Please copy and rename it manually!<br/>';
	public $INS_ERROR_SITEMAP = 'Error coping IOS Sitemap extension to folder admnistrator/components/com_sitemap/extensions/. Please copy it manually!<br/>';
	public $INS_ERROR_NOTICE_TITLE = 'Installation notices';
	public $INS_ERROR_NOTICE = 'All the above errors are very small errors, mainly making the user interface of eventCalendar a little bit more friendly. You do not need to worry about them!';
	public $INS_TITLE = 'Εvent Calendar - An event management system Elxis CMS 2008.x and 2009.x+';
	public $INS_BODY = '<br/><b>Event Calendar was successfully installed</b><br/><br/>Event Calendar is an event management and publishing system. It lets you store events, which are displayed to the end-users through a table similar to a desktop calendar. It supports repeating events, as well as day, week and month display.<br/><br/><b>Instuctions:</b> Please refer to <a href="http://wiki.elxis.org" target="_blank">Elxis Wiki</a>::<a href="http://wiki.elxis.org/wiki/Event_Calendar_(component)" target="_blank">Event Calendar (component)</a>.<br/><br/>';
	public $GEN_COMPONENT_TITLE = 'Event Calendar';
	public $GEN_MANAGE_CATEGORIES = 'Category Manager';
	public $GEN_MANAGE_EVENTS = 'Event Manager';
	public $GEN_SUPPORT = 'Instructions<br/>(Elxis Wiki)';
	public $GEN_YES = 'Yes';
	public $GEN_NO = 'No';

	public $CP = 'Control Panel';
	public $CP_TAB_GENERAL = 'General';
	public $CP_TAB_GENERAL_DEFAULT_VIEW = 'Default view';
	public $CP_TAB_GENERAL_DEFAULT_VIEW_TOOLTIP = 'You can set the sort of view selected by the script if there is passed none.';
	public $CP_TAB_GENERAL_DEFAULT_VIEW_DAY = 'Day';
	public $CP_TAB_GENERAL_DEFAULT_VIEW_WEEK = 'Week';
	public $CP_TAB_GENERAL_DEFAULT_VIEW_MONTH = 'Month';
	public $CP_TAB_GENERAL_DEFAULT_WHO_POST = 'Who can post events';
	public $CP_TAB_GENERAL_DEFAULT_WHO_POST_TOOLTIP = 'You can set wether it is possible to post events as public, registered or special user.';
	public $CP_TAB_GENERAL_DEFAULT_WHO_EDIT = 'Who can edit events';
	public $CP_TAB_GENERAL_DEFAULT_WHO_EDIT_TOOLTIP = 'You can set wether it is possible to edit events as public, registered or special user.';
	public $CP_TAB_DATEFORMAT = 'Date Format';
	public $CP_TAB_DATEFORMAT_START = 'Starting day of week';
	public $CP_TAB_DATEFORMAT_START_TOOLTIP = 'Sets the day weeks will start in week and month-view.';
	public $CP_TAB_DATEFORMAT_START_MON = 'Monday';
	public $CP_TAB_DATEFORMAT_START_TUE = 'Tuesday';
	public $CP_TAB_DATEFORMAT_START_WED = 'Wednesday';
	public $CP_TAB_DATEFORMAT_START_THU = 'Thursday';
	public $CP_TAB_DATEFORMAT_START_FRI = 'Friday';
	public $CP_TAB_DATEFORMAT_START_SUN = 'Sunday';
	public $CP_TAB_DATEFORMAT_START_SAT = 'Saturday';
	public $CP_TAB_DATEFORMAT_f1 = 'd-m-Y';
	public $CP_TAB_DATEFORMAT_f2 = 'm-d-Y';
	public $CP_TAB_DATEFORMAT_f3 = 'Y-m-d';
	public $CP_TAB_DATEFORMAT_f_TOOLTIP = 'Set the format of the displayed date, where Υ = year, m = month, d = day.';
	public $CP_TAB_DISPLAY = 'Display';
	public $CP_TAB_DISPLAY_WEEKNUM = 'Display week-numbers';
	public $CP_TAB_DISPLAY_WEEKNUM_TOOLTIP = 'You can select whether the week-number in month-view is displayed in front of the rows or not.';
	public $CP_TAB_DISPLAY_WEEKNUM_LINK = 'Display week-numbers as links';
	public $CP_TAB_DISPLAY_WEEKNUM_LINK_TOOLTIP = 'You can select whether the week-numbers in the monthly view are displayed as links to corresponding week-views or not.';
	public $CP_TAB_DISPLAY_NAV_VIEW = 'Display navigation';
	public $CP_TAB_DISPLAY_NAV_VIEW_TOOLTIP = 'You can select whether a navigation menu is displayed at the top of the component.';
	public $CP_TAB_DISPLAY_NAV_PRINT = 'Display print button';
	public $CP_TAB_DISPLAY_NAV_PRINT_TOOLTIP = 'You can select whether a print icon is displayed at the top of the event view.';
	public $CP_TAB_DISPLAY_NAV_RSS = 'Display RSS button';
	public $CP_TAB_DISPLAY_NAV_RSS_TOOLTIP = 'You can select whether a RSS icon is displayed at the top of the component.';
	public $CP_TAB_DISPLAY_DAY_VIEW_ID = 'Display day id';
	public $CP_TAB_DISPLAY_DAY_VIEW_ID_TOOLTIP = 'You can select whether the identity of the day is displayed (sunrise/set, moon phase, anniversaries).';
	public $CP_TAB_DISPLAY_DAY_VIEW_HISTORY = 'Display today\'s history';
	public $CP_TAB_DISPLAY_DAY_VIEW_HISTORY_TOOLTIP = 'You can select whether today\'s  history is displayed.';
	public $CP_TAB_DISPLAY_CAT_VIEW = 'Display categories';
	public $CP_TAB_DISPLAY_CAT_VIEW_TOOLTIP = 'You can set wether the events categories are displayed at the bottom of the component.';
	public $CP_TAB_DISPLAY_CAT_PER = 'Display periodicity';
	public $CP_TAB_DISPLAY_CAT_PER_TOOLTIP = 'You can set wether the event periodicity is displayed when at event view.';
	public $CP_TAB_DISPLAY_CAT_COL = 'Category columns number';
	public $CP_TAB_DISPLAY_CAT_COL_TOOLTIP = 'You can set the number of columns in which the events categories are displayed.';
	public $CP_TAB_DISPLAY_SFX = 'Component Template';
	public $CP_TAB_DISPLAY_SFX_TOOLTIP = 'Select the component template.';
	public $CP_TAB_DISPLAY_SFX_NWP = 'Folder <i>/components/com_eventcalendar/templates/</i> has <br/> no write permissions!';
	public $CP_TAB_DISPLAY_SFX_ND = 'You can not remove the default template!';
	public $CP_TAB_DISPLAY_SFX_NF = 'You must set a template file (zip) to upload!';
	public $CP_TAB_DISPLAY_RESERVATION = 'User reservations';
	public $CP_TAB_DISPLAY_RESERVATION_TOOLTIP = 'You can set whether users may reserve for an event.';
	public $CP_TAB_TIMETABLE = 'Timetable';
	public $CP_TAB_TIMETABLE_STR = 'Starting time of block';
	public $CP_TAB_TIMETABLE_END = 'Ending time of block';
	public $CP_TAB_TIMETABLE_ADD = 'Add';
	public $CP_TAB_TIMETABLE_DEL = 'Delete';
	public $CP_TAB_TIMETABLE_TOOLTIP = 'You can set the timetable for the daily view of events';
	public $CP_TAB_RSS = 'RSS';
	public $CP_TAB_RSS_CACHE = 'Cache';
	public $CP_TAB_RSS_CACHE_TOOLTIP = 'You can set whether the syndication files are cached.';
	public $CP_TAB_RSS_CACHETIME = 'Cache time';
	public $CP_TAB_RSS_CACHETIME_TOOLTIP = 'You can set the duration the syndication files are cached (in seconds).';
	public $CP_TAB_RSS_NUM = 'Number';
	public $CP_TAB_RSS_NUM_TOOLTIP = 'You can set the number of displayed events.';
	public $CP_TAB_RSS_TITLE = 'Title';
	public $CP_TAB_RSS_TITLE_TOOLTIP = 'You can set a title of event syndication.';
	public $CP_TAB_RSS_DES = 'Description';
	public $CP_TAB_RSS_DES_TOOLTIP = 'You can set a description for event syndication.';
	public $CP_TAB_RSS_MULTILANG = 'Multilingual feeds';
	public $CP_TAB_RSS_MULTILANG_TOOLTIP = 'You can set whether seperate syndication files are created for each language.';
	public $CP_TAB_RSS_LIVE = 'Live Bookmarks (for Firefox)';
	public $CP_TAB_RSS_LIVE_TOOLTIP = 'You can set whether live bookmarks for Firefox are created.';
	public $CP_TAB_RSS_IMG = 'Image';
	public $CP_TAB_RSS_IMG_TOOLTIP = 'You can set the image of the RSS feeds.';
	public $CP_TAB_RSS_IMGALT = 'Image alternative text';
	public $CP_TAB_RSS_IMGALT_TOOLTIP = 'You can set the alternative text of RSS feeds image.';
	public $CP_TAB_RSS_TEXTLIM = 'Text limit';
	public $CP_TAB_RSS_TEXTLIM_TOOLTIP = 'You can set whether whole or part of the event text is displayed.';
	public $CP_TAB_RSS_TEXTLEN = 'Text length';
	public $CP_TAB_RSS_TEXTLEN_TOOLTIP = 'You can set the length of the displayed text.';
	public $CP_TAB_SITEMAP_TEXT = 'IOS Sitemap component is installed, but EventCalendar extension for IOS Sitemap is not! If you wish to activate the extension, please check the box below. Please bear in mind that after activation, this option will not be available again.<br/><br/>';
	public $CP_TAB_SITEMAP_CHECK = 'Activate IOS Sitemap extension';
	public $CP_TAB_SITEMAP_NOTE = '<br/><br/><b>NOTE: </b>You must have the right privileges (write permission to IOS Sitemap extension folder) in order to activate the extension!';
	public $CP_TAB_PP = 'PayPal';
	public $CP_TAB_PP_MAIL = 'Your PayPal e-mail';
	public $CP_TAB_PP_MAIL_TOOLTIP = 'Your PayPal e-mail.';
	public $CP_TAB_PP_CURRENCY = 'Currency';
	public $CP_TAB_PP_CURRENCY_TOOLTIP = 'Select your currency.';
	public $CP_MSG_CONFIGURATION_STORED = 'Event Calendar: New settings stored';
	public $CP_MSG_CONFIGURATION_ERROR = 'Event Calendar: Unable to store new setting in database';
	public $CP_MSG_TEMPLATE_REM_Q = 'Remove template ';
	public $CP_MSG_TEMPLATE_REM_OK = 'Event Calendar: The template was successfully removed!';
	public $CP_MSG_TEMPLATE_REM_ERROR = 'Event Calendar: No template was selected!';
	public $CP_MSG_TEMPLATE_ADD_OK = 'Event Calendar: The template file was successfully added!';
	public $CP_MSG_TEMPLATE_ADD_ERROR = 'Event Calendar: No template file selected!';
	
	public $LST_ALL_EVENTS = 'All events';
	public $LST_OLD_EVENTS = 'Old events';
	public $LST_RECURSION = 'Recursion';
	public $LST_REC_DAY = 'Every day';
	public $LST_REC_WEEK = 'Once a week on:';
	public $LST_REC_MONTH = 'Once a month on';
	public $LST_REC_MONTH_SUFFIX_1 = 'st';
	public $LST_REC_MONTH_SUFFIX_2 = 'nd';
	public $LST_REC_MONTH_SUFFIX_ALL = 'th';
	public $LST_REC_YEAR = 'Once a year on';
	public $LST_REC_FOR = 'for';
	public $LST_REC_TIMES = 'times';
	
	public $EDT_SUBMIT = 'Submit event';
	public $EDT_DETAILS = 'Details';
	public $EDT_START = 'Event starts';
	public $EDT_END = 'Event ends';
	public $EDT_START_END_TOOLTIP = 'Declaim a date range for publishing the event, e.g. 2009-12-31 15:30:00.';
	public $EDT_REPEAT_OPT = 'Repeating options';
	public $EDT_REPEAT_TYPE = 'Periodicity';
	public $EDT_REPEAT_TYPE_TOOLTIP = 'Select the periodicity of the event.';
	public $EDT_WEEKLY_OPT = 'Weekly repeating day';
	public $EDT_WEEKLY_OPT_TOOLTIP = 'If the event comes up every week, you can choose the days, it should appear in.';
	public $EDT_MONTHLY_OPT = 'Monthly repeating day';
	public $EDT_MONTHLY_OPT_TOOLTIP = 'If the event comes up every month, you can choose the days, it should appear in.';
	public $EDT_YEARLY_OPT = 'Annual repeating day';
	public $EDT_YEARLY_OPT_TOOLTIP = 'If the event comes up every year, you can choose the days, it should appear in.';
	public $EDT_EXCEPT = 'Exceptions';
	public $EDT_EXCEPT_ADD = 'Add exception';
	public $EDT_EXCEPT_LIST = 'Exception list';
	public $EDT_INFO = 'Contact Information';
	public $EDT_INFO_PERSON = 'Contact Person';
	public $EDT_INFO_PERSON_TOOLTIP = "This information is optional.<br /> You can add a person's name beeing involved in the events organisation.";
	public $EDT_INFO_WEB = 'Website';
	public $EDT_INFO_WEB_TOOLTIP = "This information is optional.<br /> If there is a homepage for this event you can make it public here.";
	public $EDT_INFO_MAIL = 'Email';
	public $EDT_INFO_MAIL_TOOLTIP = 'This information is optional.<br /> You can submit an email-adress supporting detailed information about the event. You should talk back with its owner because of caused traffic.';
	public $EDT_RSV = 'Reservation';
	public $EDT_RSV_YES = 'You have already reserved for this event!';
	public $EDT_RSV_NO = 'You have not reserved for this event!';
	public $EDT_RSV_NONE = 'No reservation yet...';
	public $EDT_RSV_SUB = 'Reserve Now!!';
	public $EDT_RSV_CAN = 'Cancel Reservation';
	public $EDT_PP_BUY = 'Buy with PayPal';
	public $EDT_MSG_STORED = 'The event have been successfully stored!';
	public $EDT_MSG_ERROR = 'Unable to store event';
	
	public $ALERT_WRONG_TIME_FORMAT = 'Wrong time format for starting or ending time!\n Please use hh:mm';
	public $ALERT_EDIT_NO_TITLE = 'There must be a title for this event!';
	public $ALERT_EDIT_NO_SEOTITLE = 'There must be a SEO title for this event!';
	public $ALERT_EDIT_NO_CATEGORY = 'You must select a category for this event!';
	public $ALERT_EDIT_NO_START_DATE = 'There must be a starting date for this event!';
	public $ALERT_EDIT_NO_START_TIME = 'There must be a starting time for this event!';
	public $ALERT_EDIT_NO_END_DATE = 'There must be a finishing date for this event!';
	public $ALERT_EDIT_NO_END_TIME = 'There must be a finishing time for this event!';

	public $FRONT_VIEW = 'View';
	public $FRONT_VIEW_CAT = 'View all events of category';
	public $FRONT_VIEW_CAT_TITLE = 'View events per category';
	public $FRONT_VIEW_DAY_YESTERDAY = 'yesterday';
	public $FRONT_VIEW_DAY_TODAY = 'Today';
	public $FRONT_VIEW_DAY_TOMMOROW = 'tommorow';
	public $FRONT_VIEW_WEEK_NEXT = 'next';
	public $FRONT_VIEW_WEEK_NEXT_TITLE = 'Next week';
	public $FRONT_VIEW_WEEK_PREV = 'prev.';
	public $FRONT_VIEW_WEEK_PREV_TITLE = 'Previous week';
	
	// Just an empty constructor
	public function __construct() {
	}
}
?>
