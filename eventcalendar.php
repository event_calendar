<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Frontend Event Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

//  Check the permissions
if ( $my->usertype == '' ) { 
	$usertype = eUTF::utf8_strtolower($acl->get_group_name('29'));
} else {
	$usertype = eUTF::utf8_strtolower($my->usertype);
}
// Check if the user is allowed to access this component. If not then re-direct him to the main page.
if (($mosConfig_access == '1') | ($mosConfig_access == '3')) {
	if (!($acl->acl_check( 'action', 'view', 'users', $usertype, 'components', 'all' ) || 
	    $acl->acl_check( 'action', 'view', 'users', $usertype, 'components', 'com_eventcalendar' ))) {
		    mosRedirect( 'index.php', _NOT_AUTH );
	}
}

// Includes
require_once($mainframe->getCfg('absolute_path' ).'/components/com_eventcalendar/eventcalendar.class.php'); // Component's general class
require_once($mainframe->getCfg('absolute_path' ).'/components/com_eventcalendar/eventcalendar.html.php'); // Component's html file for the public area

/*************************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S FRONT-END FUNCTIONALITY  */
/*************************************************************************/
class clsEventCalendar {
	
	// Initialize variables
	public $live_path = ''; // component's live path
	public $live_apath = ''; // component's live path (admin)
	public $abs_path = ''; // component's absolute path
	public $abs_apath = ''; // component's absolute path (admin)
	public $lng = null; // component's language
	public $params = array(); // component's parameters
	public $timetable = array(); // user-set timetables
	public $timetable_selected = 0; // selected timetable
	public $weekdays = array(); // for offset calculation
	public $events = array(); // store events
	public $calendar = array(); // store calendar days

	public $task = '';
	public $catid = '';
	public $eventid = '';
	public $date = 0;
		public $year = '';
		public $month = '';
		public $week = '';
		public $day = '';

	/****************************/
	/*  The class' constructor  */
	/****************************/ 
	public function __construct() {
		global $mainframe, $lang, $Itemid, $database;

		// Set absolute and live path
		$this->live_path = $mainframe->getCfg('live_site').'/components/com_eventcalendar';
		$this->live_apath = $mainframe->getCfg('live_site').'/administrator/components/com_eventcalendar';
		$this->abs_path = $mainframe->getCfg('absolute_path').'/components/com_eventcalendar';
		$this->abs_apath = $mainframe->getCfg('absolute_path').'/administrator/components/com_eventcalendar';

		// Load language file
		if (file_exists($this->abs_path.'/language/'.$lang.'.php')) {
			require_once($this->abs_path.'/language/'.$lang.'.php');
		} else {
			require_once($this->abs_path.'/language/english.php');
		}
		$this->lng = new clsEventCalendarLanguage();
		
		// Load params from Event Calendar (component)
			$database->setQuery("SELECT params FROM #__components WHERE link = 'option=com_eventcalendar'", '#__', 1, 0);
			$result = $database->loadResult();
			$com_params = new mosParameters($result);
		// Load params from MENUITEM
			$menu = new mosMenu($database);
			$menu->load($Itemid);
			$menu_params = new mosParameters($menu->params);
		// Create an array with parameters
		$this->params['com'] = $com_params;
		$this->params['menu'] = $menu_params;
		$this->params['menuobj'] = $menu;

		// Unserialize and split timetable values
		$this->timetable = unserialize( $this->params['com']->get('timetable', 'a:1:{i:0;s:10:"0:00-23:59";}'));
		for ( $i = 0; $i < count($this->timetable); $i++ ) {
			$this->timetable[$i] = split( "-", $this->timetable[$i]);
		}
		// Get selected timetable
		$this->timetable_selected = $this->params['com']->get( 'timetable_selected', 0);

		// Use english week-day-names for offset calculation
		$this->weekdays = array("sunday","monday","tuesday","wednesday","thursday","friday","saturday","sunday");
				
		//Set current task and eventid
		$this->task = (string)mosGetParam($_REQUEST, 'task' , $this->params['com']->get('default_view', 'monthview'));
		$this->eventid = intval( mosGetParam($_REQUEST, 'eventid' , '') );
		// Set category id(s)
		$this->catid = mosGetParam($_REQUEST, 'catid' , '');
		$this->catid = $this->parseCatid( $this->catid );
		// Get date from REQUEST, else set current date
		$this->year = intval(mosGetParam($_REQUEST, 'year', date("Y", time())));
		$this->week = mosGetParam($_REQUEST, 'week');
		if ($this->week) {
			$this->date = strtotime("+".($this->week - 1)." week", mktime(0,0,0,01,01,$this->year));
			$this->month =  date("m", $this->date);
			$this->day =  date("d", $this->date);
		} else {
			$this->month = mosGetParam($_REQUEST, 'month', date("m", time()));
			$this->day = mosGetParam($_REQUEST, 'day', date("d", time()));
			$this->date = strtotime($this->year."-".$this->month."-".$this->day);
			$this->week = mosGetParam($_REQUEST, 'week', date("W", $this->date));
		}
	}


	/******************************/
	/*  The class' main function  */
	/******************************/ 	
	public function main() {
		global $mainframe, $Itemid;
		
		switch($this->task) {
			case 'editevent':
				$this->displayEventEdit($this->eventid);
				break;
			case 'save':
				$this->saveEvent();
				break;
			case 'cancel':
				$this->cancelEvent();
				break;			
			case 'eventview': // displays single event
			case 'printevent':
				$this->displayEventView();
				break;
			case 'dayview': //shows daily view
				$this->displayDayView();
				break;
			case 'weekview': //shows weekly view
				$this->displayWeekView();
				break;
			case 'monthview': //shows monthly view
				$this->displayMonthView();
				break;
			case 'catview': //shows event-list
				$this->displayCategoriesList();
				break;
			case 'ajaxrsv': //set reservations
				$this->ajaxReserve();
				break;
			case 'syndication':
				$this->eventSyndication();
				break;
			default: // Redirect to default_view
				mosRedirect( sefRelToAbs("index.php?option=com_eventcalendar&task=".$this->params['com']->get('default_view', 'monthview')."&Itemid=".$Itemid, EVCALBASE . "/") );
				break;
		}
	}

	/*****************/
	/*  Syndication  */
	/*****************/ 	
	private function eventSyndication() {
		global $database, $mainframe, $lang, $mosConfig_offset, $my, $Itemid;

		require_once($mainframe->getCfg('absolute_path').'/includes/feedcreator.class.php' );

		$info = null;
		$rss = null;
		
		$live_bookmark = mosGetParam($_REQUEST, 'livebookmark' , '0');
		$feed = strtoupper(mosGetParam( $_GET, 'feed', 'RSS2.0'));
			$vfeeds = array( 'RSS0.91', 'RSS1.0', 'RSS2.0', 'OPML', 'ATOM0.3' ); //PIE0.1, MBOX, HTML, JS removed
			if (!in_array($feed, $vfeeds)) { $feed = 'RSS2.0'; }
		$now = date('Y-m-d H:i:s', time() + $mosConfig_offset * 3600);
		$iso = preg_split('/\=/', _ISO );
		
		$info['date'] = date( 'r' );
		$info['year'] = date( 'Y' );
		$info['encoding'] = $iso[1];
		$info['link'] = htmlspecialchars($mainframe->getCfg('live_site'));
		$info['cache'] = $this->params['com']->get('rss_cache', '0');
		$info['cache_time'] = intval($this->params['com']->get('rss_cachetime', '3600'));
		$info['count'] = intval($this->params['com']->get('rss_num', '5'));
		$info['multilingual'] = intval($this->params['com']->get('rss_multilang', '0'));
		$info['title'] = $this->params['com']->get('rss_title', 'EventCalendar RSS feeds');
		$info['description'] = $this->params['com']->get('rss_description', 'EventCalendar web syndication.');
		$info['image_file']	= $this->params['com']->get( 'rss_img', 'elxis_rss.png' );
		if ( $info['image_file'] == -1 ) {
			$info['image'] = NULL;
		} else{
			$info['image'] = $mainframe->getCfg('live_site').'/images/M_images/'.$info['image_file'];
		}
		$info['image_alt'] = $this->params['com']->get( 'rss_imgal', 'EventCalendar RSS feeds' );
		$info['limit_text'] = $this->params['com']->get( 'rss_limittext', 0 );
		$info['text_length'] = $this->params['com']->get( 'rss_textlength', 20 );
		$info['feed'] =  $feed;
		//live bookmarks
		$info['live_bookmark'] = $this->params['com']->get( 'rss_live', '' );

		if ( $info['live_bookmark'] ) {
			$info['file'] = $mainframe->getCfg('absolute_path').'/cache/evlive.xml';
		} else {
			$info['file'] = eUTF::utf8_strtolower( eUTF::utf8_str_replace( '.', '', $info['feed'] ) );
			$info['file'] = $mainframe->getCfg('absolute_path').'/cache/ev'.$info['file'].'.xml';
		}

		if ($info['multilingual']) { //multilingual feeds
			$info['file'] = str_replace('.xml', '-'.$lang.'.xml', $info[ 'file' ]);
		}

		$rss = new UniversalFeedCreator();
		$image = new FeedImage();
		
		if ( $info[ 'cache' ] ) {
			$rss->useCached( $info[ 'feed' ], $info[ 'file' ], $info[ 'cache_time' ] );
		}

		$rss->title = $info[ 'title' ];
		$rss->description = $info[ 'description' ];
		$rss->link = $info[ 'link' ];
		$rss->syndicationURL = $info[ 'link' ];
		$rss->cssStyleSheet = NULL;
		$rss->encoding = $info[ 'encoding' ];

		if ( $info[ 'image' ] ) {
			$image->url = $info[ 'image' ];
			$image->link = $info[ 'link' ];
			$image->title = $info[ 'image_alt' ];
			$image->description = $info[ 'description' ];
			
			$rss->image = $image;
		}
		$query = "SELECT e.id, e.title, e.seotitle, e.description FROM #__eventcalendar e"
			   . "\n LEFT JOIN #__categories c ON c.id = e.catid"
			   . "\n WHERE e.published = '1' AND c.published = '1' AND c.access IN (".$my->allowed.")"
			   . "\n AND (e.start_date >= '".date("Y-m-d H:m:s", time())."')"
			   . "\n AND ( (e.language IS NULL) OR (e.language LIKE '%".$lang."%') )"
			   . "\n AND ( (c.language IS NULL) OR (c.language LIKE '%".$lang."%') )"
			   . "\n ORDER BY e.start_date ASC";

		if ( $info['count'] ) {
			$database->setQuery( $query, '#__', $info[ 'count' ], 0 );
		} else {
			$database->setQuery( $query );
		}

		$rows = $database->loadObjectList();

		if ($rows) {
			foreach ( $rows as $row ) {
				$item_title = htmlspecialchars( $row->title );
				$item_title = html_entity_decode( $item_title );

				$query = "SELECT id FROM #__menu"
				."\n WHERE link = 'index.php?option=com_eventcalendar' AND published = '1'"
				."\n AND ((language IS NULL) OR (language LIKE '%".$xartis->maplang."%'))";
				$database->setQuery($query, '#__', 1, 0);
				$_Itemid = intval($database->loadResult());

				$item_link = $mainframe->getCfg('live_site').'/index.php?option=com_eventcalendar&task=eventview&eventid='.$row->id.'&Itemid='.$_Itemid;
				$item_link = sefRelToAbs( $item_link );
				$item_link = str_replace('&amp;', '&', $item_link);

				$item_description = $row->description;
				$item_description = mosHTML::cleanText( $item_description );
				$item_description = html_entity_decode( $item_description );
				if ($info['limit_text']) {
					if ($info[ 'text_length']) {
						$item_description_array = preg_split('/[\s]/', $item_description);
						$count = count( $item_description_array );
						if ( $count > $info['text_length']) {
							$item_description = '';
							for ( $a = 0; $a < $info['text_length']; $a++ ) {
								$item_description .= $item_description_array[$a]. ' ';
							}
							$item_description = eUTF::utf8_trim( $item_description );
							$item_description .= '...';
						}
					} else  {
						$item_description = NULL;
					}
				}

				//load individual item creator class
				$item = new FeedItem();
				//item info
				$item->title = $item_title;
				$item->link = $item_link;
				$item->description = $item_description;
				$item->source = $info['link'];

				//loads item info into rss array
				$rss->addItem( $item );
			}
		}
		
		$rss->saveFeed( $info[ 'feed' ], $info[ 'file' ], true );	
	}

	/************************/
	/*  AJAX Reserveations  */
	/************************/ 	
	private function ajaxReserve() {
		global $database, $my;
		
		$elem = intval(mosGetParam($_REQUEST, 'elem', ''));
		$user_id = intval(mosGetParam($_REQUEST, 'user-id', ''));
		$state = strval(mosGetParam($_REQUEST, 'state', ''));
		
		if ($this->eventid) {
			$event = new mosEventCalendar_Event( $database );
			$event->load( $this->eventid );
			
			if ($state == 'reserve') {
				$event->rsv = ($event->rsv)?$event->rsv.','.$user_id:$user_id;
				if ($event->check($error)) {
					if (!$event->store()) {
						echo "<script type=\"text/javascript\">alert('".$objEventCalendar->lng->GEN_COMPONENT_TITLE.": ".$objEventCalendar->lng->EDT_MSG_ERROR."');</script>"._LEND;
						exit();
					}
				} else {
					echo "<script type=\"text/javascript\">alert('".$objEventCalendar->lng->GEN_COMPONENT_TITLE.": ".$error."');</script>"._LEND;
					exit();
				}				
			} elseif ($state == 'cancel') {
				$users = explode(',', $event->rsv);
				if ( in_array($user_id, $users) ) {
					foreach ($users as $key=>$value) {
						if ($value == $user_id) {
							unset($users[$key]);
						}
					}
				}
				$event->rsv = implode(',', $users);
				if ($event->check($error)) {
					if (!$event->store()) {
						echo "<script type=\"text/javascript\">alert('".$objEventCalendar->lng->GEN_COMPONENT_TITLE.": ".$objEventCalendar->lng->EDT_MSG_ERROR."');</script>"._LEND;
						exit();
					}
				} else {
					echo "<script type=\"text/javascript\">alert('".$objEventCalendar->lng->GEN_COMPONENT_TITLE.": ".$error."');</script>"._LEND;
					exit();
				}
			}
		}
		
		if ($event->rsv) { 
			$query = "SELECT name FROM #__users WHERE id IN (".$event->rsv.")";
			$database->setQuery( $query );
			$users = $database->loadResultArray();

			echo '<i>'.$users[0].'</i>';
			for ($i = 1; $i < count($users); $i++) {
				echo ', '.'<i>'.$users[$i].'</i>';
			}
		} else {
			echo '<i>'.$this->lng->EDT_RSV_NONE.'</i>';
		}

		if ($my->id <> 0) {
			if (in_array($my->id, explode(',', $event->rsv))) {
				echo "<br/><br/>".$this->lng->EDT_RSV_YES."<br/>"; ?>
				<input class="button" type="button" value="<?php echo $this->lng->EDT_RSV_CAN; ?>" id="cancel-rsv" name="cancel-rsv" onclick="showReserve(<?php echo $my->id; ?>, <?php echo $event->id; ?>, 'cancel');" />
			<?php } else {
				echo "<br/><br/>".$this->lng->EDT_RSV_NO."<br/>"; ?>
				<input class="button" type="button" value="<?php echo $this->lng->EDT_RSV_SUB; ?>" id="submit-rsv" name="submit-rsv" onclick="showReserve(<?php echo $my->id; ?>, <?php echo $event->id; ?>, 'reserve');" />
			<?php }
		}
	}



	/***********************************/
	/*  Prepare to display month view  */
	/***********************************/ 	
	private function displayMonthView()  {
		global $fmanager;

		$week_startingday = $this->params['com']->get( 'week_startingday' , 0);
		
		// Get timestamps for first and last day of month
		$firstDay_stamp = mktime( 0, 0, 0, date("m", $this->date), 1, date("Y", $this->date) );
		$lastDay_stamp = mktime( 0, 0, 0, date("m", $this->date) + 1, 1, date("Y", $this->date) );
			$lastDay_stamp = strtotime("yesterday", $lastDay_stamp);

		// Get how many days will be empty in the view before current month starts
		$prevMonth_days = intval( strftime("%w", $firstDay_stamp) ) - $week_startingday;
		$prevMonth_days = ($prevMonth_days < 0)?$prevMonth_days + 7:$prevMonth_days;

		// Get how many days will be empty in the view after current month ends
		$nextMonth_days = intval( 6 - strftime("%w", $lastDay_stamp) + $week_startingday);
		$nextMonth_days = ($nextMonth_days == 7)?0:$nextMonth_days;
		
		// Loading weeknumbers
		$working_date = $firstDay_stamp;
		$weeknrs = array();
		// Get amount of days to be displayed and divide them by seven,
		// then ciel to round and get the number of weeks in the view
		for($i = 0; $i < ( $prevMonth_days + strftime("%d", $lastDay_stamp) + $nextMonth_days ) / 7 ; $i++ ) {
			$weeknrs[] = ($fmanager->iswin) ? strftime("%U", $working_date) : strftime("%V", $working_date);
			$working_date = strtotime("+ 1 week", $working_date);
		}

		// Create calender matrix of dates for days of month
		$working_date = strtotime( '- '.$prevMonth_days.' days', $firstDay_stamp);
		// Fill previous month days
		for ($i = 0; $i < $prevMonth_days; $i++) { 
			$this->calendar[] = $working_date;
			$working_date = strtotime( '+ 1 day', $working_date);
		}
		// Fill current month days
		for ($i = 0; $i < strftime("%d", $lastDay_stamp); $i++) {
			$this->calendar[] = $working_date;
			$working_date = strtotime( '+ 1 day', $working_date);
		}
		// Fill next month days
		for ($i = 0; $i < $nextMonth_days; $i++) { 
			$this->calendar[] = $working_date;
			$working_date = strtotime( '+ 1 day', $working_date);
		}

		// Calculate events
		$this->calcEvents();
		
		// Render month table
		clsEventCalendarHTML::displayMonthViewHTML($weeknrs);
		
		// Disaply footer
		$this->displayFooter();
	}

	/***********************************/
	/*  Prepare to display week view  */
	/***********************************/ 	
	protected function displayWeekView()  {
	
		$week_startingday = $this->params['com']->get( 'week_startingday' , 0);

		// Get timestamps for first and last day of week
		$firstDay_stamp = (date("w", $this->date) == $week_startingday)?$this->date:strtotime(('last '.$this->weekdays[$week_startingday]), $this->date);
		//$lastDay_stamp = (date("w", $this->date) == ((($week_startingday - 1) < 0)?6:$week_startingday - 1))?$this->date:strtotime(('next '.$this->weekdays[((($week_startingday - 1) < 0)?6:$week_startingday - 1)]), $this->date);
			
		$working_date = $firstDay_stamp;
		for ($i = 0; $i <= 6; $i++) {
			$this->calendar[] = $working_date;
			$working_date = strtotime('+1 day', $working_date);
		}

		// Calculate events
		$this->calcEvents();

		// Render week table
		clsEventCalendarHTML::displayWeekViewHTML();
		
		// Display footer
		$this->displayFooter();
	}
		
	/*********************************/
	/*  Prepare to display day view  */
	/*********************************/ 	
	protected function displayDayView()  {

		// Calculate events
		$this->calcEvents();

		// Render day table
		clsEventCalendarHTML::displayDayViewHTML();

		// Display footer
		$this->displayFooter();
	}

	/***********************************/
	/*  Prepare to display event view  */
	/***********************************/ 	
	protected function displayEventView()  {
		global $database, $Itemid;
		
		if ($this->eventid) {
			$event = new mosEventCalendar_Event( $database );
			$event->load( $this->eventid );
			
			$category = new mosCategory( $database );
			$category->load( $event->catid );
						
			clsEventCalendarHTML::displayEventViewHTML( $event, $category );
			
			// Display footer
			if ($this->task != 'printevent') {
				$this->displayFooter();
			}
		} else {
			$menuitem = "&Itemid=".$Itemid;
			if (isset($this->catid)) {
				$catids = (is_array($this->catid))?implode("+", $this->catid):$this->catid;
			}
			$catids = ($catids !== "")?"&catid=".$catids:"";
			
			mosRedirect( sefRelToAbs("index.php?option=com_eventcalendar&task=".$this->params['com']->get('default_view', 'month')."&year=".$this->year."&month=".$this->month."&day=".$this->day.$catids.$menuitem) );
		}
	}
	
	/******************************************/
	/*  Prepare to display the category list  */
	/******************************************/ 	
	protected function displayCategoriesList() {
		global $database, $my;

		// Load categories from database
		if (is_array( $this->catid )) {
			$filter = "\n AND id IN (" . implode( ",", $this->catid ) . ")";
		} else if ($this->catid) {
			$filter = "\n AND id = " . $this->catid;
		}
		$categories = $this->getCategories( $filter );

		// Load events from database
		$query = "SELECT e.id FROM #__eventcalendar e"
		. "\n LEFT JOIN #__categories c ON c.id = e.catid"
		. "\n WHERE e.published = '1' AND c.published = '1' AND c.access IN (".$my->allowed.")";
		if (is_array( $this->catid )) {
			$query .= "\n AND c.id IN (" . implode( ",", $this->catid ) . ")";
		} else if ($this->catid) {
			$query .= "\n AND c.id = " . $this->catid;
		}
		$query .= "\n ORDER BY e.start_date ASC";
		$database->setQuery( $query );
		$results = $database->loadResultArray();

		if ($results) {
			foreach ($results AS $event_id ) {
				$load_event = new mosEventCalendar_Event( $database );
				$load_event->load( $event_id );
				$this->events[] = clone( $load_event );
			}
		}
		clsEventCalendarHTML::displayCategoriesListHTML( $categories );
		
		// Display footer
		$this->displayFooter();
	}


	/********************************************/
	/*  Prepare to display the edit event form  */
	/********************************************/ 	
	protected function displayEventEdit($existing = '') {
		global $my, $database;

		// Validate once more permissions
		if (!$my->id) {
			mosNotAuth();
			return;	
		}
		$permits = explode(',', $my->allowed);
		if ($existing == '') {
			if ( !in_array($this->params['com']->get('who_can_post_events'), $permits) ) {
				mosNotAuth();
				return;	
			}
		} else {
			if ( !in_array($this->params['com']->get('who_can_edit_events'), $permits) ) {
				mosNotAuth();
				return;	
			}
		}		

		// Load event for edit/add
		if (is_numeric($existing)) {
			$event = new mosEventCalendar_Event($database);
			$event->load($existing);
			$existing = $event;
			$existing->checkout( $my->id );
		}
	
		clsEventCalendarHTML::displayEventEditHTML($existing);
	}
	/*********************/
	/*  Save event form  */
	/*********************/ 	
	protected function saveEvent() {
		global $my, $mainframe, $database, $Itemid;
		
		// CSRF prevention
		$tokname = 'token'.$my->id;
		if ( !isset($_POST[$tokname]) || !isset($_SESSION[$tokname]) || ($_POST[$tokname] != $_SESSION[$tokname]) ) {
			die( 'Detected CSRF attack! Someone is forging your requests.' );
		}

		foreach ($_POST['languages'] as $xlang) {
			if (trim($xlang) == '') { $newlangs = ''; }
		}
		if (!isset($newlangs)) {
			$newlangs = implode(',', $_POST['languages']);
		}
		$_POST['language'] = $newlangs;

		// Add a seotitle and a pending state
		require_once($mainframe->getCfg('absolute_path' ).'/administrator/components/com_eventcalendar/eventcalendar.seovs.class.php');
		$seo = new seovsEventCalendar('com_eventcalendar', $_POST['title']);
		$seo->id = $_POST['id'];
		$seo->catid = $_POST['catid'];
		$_POST['seotitle'] = $seo->suggest();
		$_POST['published'] = '-1';

		// Save event
		$event = new mosEventCalendar_Event($database);
		$event->bind( $_POST );
		if ($event->check($error)) {
			if (!$event->store()) {
				echo "<script type=\"text/javascript\">alert('".$this->lng->GEN_COMPONENT_TITLE.": ".$this->lng->EDT_MSG_ERROR."'); window.history.go(-1);</script>"._LEND;
				exit();
			}
		} else {
			echo "<script type=\"text/javascript\">alert('".$this->lng->GEN_COMPONENT_TITLE.": ".$error."'); window.history.go(-1);</script>"._LEND;
			exit();
		}
		
		// Redirect to default view
		$menuitem = "&Itemid=".$Itemid;
		mosRedirect( sefRelToAbs("index.php?option=com_eventcalendar&task=".$this->params['com']->get('default_view', 'monthview') . "&year=".$this->year . "&month=".$this->month . "&day=".$this->day . $menuitem) );
	}
	/***********************/
	/*  Cancel event form  */
	/***********************/ 	
	protected function cancelEvent() {
		global $database, $Itemid;
		
		$event = new mosEventCalendar_Event($database);
		$event->checkin($_POST['id']);
		$this->catid = 0;
		
		// Redirect to default view
		$menuitem = "&Itemid=".$Itemid;
		mosRedirect( sefRelToAbs("index.php?option=com_eventcalendar&task=".$this->params['com']->get('default_view', 'monthview') . "&year=".$this->year . "&month=".$this->month . "&day=".$this->day . $menuitem) );
	}

	/***********************************/
	/*  Prepare to display the footer  */
	/***********************************/ 	
	protected function displayFooter() {
	
		if ( $this->params['com']->get('view_catlist', true) ) { //if the category-list is shown 
			if ( isset($this->catid) ) {
				// mark selected categories from $POST
				$catids = (is_array($this->catid))?$this->catid:array($this->catid);
				clsEventCalendarHTML::displayFooterHTML( $this->getCategories(), $catids );
			}
			else {
				clsEventCalendarHTML::displayFooterHTML( $this->getCategories() );
			}	
		}
	}

	/***********************/
	/*  Helpful Functions  */
	/***********************/
	/* Parse catid */
	protected function parseCatid( $catid ) {
		if ($catid) {
			$catid = split( " ", $catid );

			// Clear empty and non numerics values
			$filterFunct = create_function('$elem','return (isset($elem))?is_numeric($elem):false;');
			$catid = array_filter( $catid, $filterFunct );

			switch (count($catid)) {
				case 0: // no category id is set (return empty)
					return "";
					break;
				case 1: // one category is set (return a simple variable)
					$key = array_keys($catid);
					return $catid[$key[0]];
					break;
				default: // more categories are set (return an array)
					return $catid;
					break;
			}
		}
	}
	
	/* Return an array with event-categories selected by filter string */
	protected function getCategories($filter = '') {
		global $database, $my;

		$query = "SELECT id FROM #__categories"
				. "\n WHERE section = 'com_eventcalendar' AND published = '1' AND access IN (".$my->allowed.")"
				. $filter
				. "\n ORDER BY ordering";
		$database->setQuery ($query);
		$results = $database->loadResultArray();

		$categories = array();
		foreach ($results AS $id) {
			$category = new mosCategory($database);
			$category->load($id);
			$categories[] = clone( $category );
		}
		return $categories;
	}
	
	/* Get single values from params string */
	public function getParam($key, $params) {
		$parameter = new mosParameters( $params );
		return ($parameter->get($key))?$parameter->get($key):false;
	}
	
	/************************/
	/* Recursion Functions  */
	/************************/
	/* Calculate events */
	protected function calcEvents() {
		global $database, $my, $lang;
		
		// Load events from database
		$query = "SELECT e.id, e.start_date, e.end_date FROM #__eventcalendar e"
			   . "\n LEFT JOIN #__categories c ON c.id = e.catid"
			   . "\n WHERE e.published = '1' AND c.published = '1' AND c.access IN (".$my->allowed.")"
			   . "\n AND ( (e.language IS NULL) OR (e.language LIKE '%".$lang."%') )"
			   . "\n AND ( (c.language IS NULL) OR (c.language LIKE '%".$lang."%') )";
		if (is_array( $this->catid )) {
			$query .= "\n AND c.id IN (" . implode( ",", $this->catid ) . ")";
		} else if ($this->catid) {
			$query .= "\n AND c.id = " . $this->catid;
		}
		$query .= "\n ORDER BY e.start_date ASC";
		$database->setQuery( $query );
		$results = $database->loadObjectList();

		// Calculate events
		foreach ($results as $row_event) {
			if ( (strtotime($row_event->start_date) <= $this->calendar[count($this->calendar)-1]) || (strtotime($row_event->end_date) >= $this->calendar[0]) ) {
				$load_event = new mosEventCalendar_Event($database);
				$load_event->load($row_event->id);
				$this->events[] = clone( $load_event );
			}
		}
	}
	
	/* Calculate exceptions */
	public function calcExceptions( $event ) {
		$events = split("\n", $event->recur_except);
		for ($i = 0; $i <= (count($events)-1); $i++) {
			$events[$i] = strtotime($events[$i]);
		}
		return $events;
	}
	
	/* Calculate recursion */
	public function calcRecursion ( $date, $event ) {
		
		// Check if date is in event publishing range
		if ( ((strtotime(date("Y-m-d", strtotime($event->start_date)))) <= $date) && ((strtotime(date("Y-m-d", strtotime($event->end_date)))) >= $date) ) {
		
			switch ($event->recur_type) {
				case 'week':
					return ( date("w", $date) == $event->recur_week );
					break;
				case 'month':
					return ( $event->recur_month == date("j", $date) );
					break;
				case 'year':
					return ( ($event->recur_year_d."/".$event->recur_year_m ) ==  date("j/n", $date) );
					break;
				case 'day':
					return true;
					break;
				default:
					return false;
					break;
			}
		}
	}

}

// Initiate the class and execute it, then unset the 
// object in order to free the allocated PHP memory.
$objEventCalendar = new clsEventCalendar();
$objEventCalendar->main();
unset($objEventCalendar);

?>
