<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Installer Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Initialize variables

// Function automatically triggered by Elxis to fire the installation process 
function com_install() {
	global $database, $alang, $mainframe, $fmanager;

	// Load and execute adodb xml schema file
    if (file_exists($mainframe->getCfg('absolute_path').'/includes/adodb/adodb-xmlschema03.inc.php')) {
    	require_once($mainframe->getCfg('absolute_path').'/includes/adodb/adodb-xmlschema03.inc.php');
    	$schemafile = $mainframe->getCfg('absolute_path').'/administrator/components/com_eventcalendar/schema/eventschema03.xml';
    } else {
    	require_once($mainframe->getCfg('absolute_path').'/includes/adodb/adodb-xmlschema.inc.php');
    	$schemafile = $mainframe->getCfg('absolute_path').'/administrator/components/com_eventcalendar/schema/eventschema.xml';
    }
    $schema = new adoSchema($database->_resource);
    $schema->ContinueOnError(true);
    $schema->SetPrefix($mainframe->getCfg('dbprefix'));
    $schema->ParseSchema($schemafile);
    $schema->ExecuteSchema();

	//Load component's language
    $ipath = $mainframe->getCfg('absolute_path').'/components/com_eventcalendar';
	if (file_exists($ipath.'/language/'.$alang.'.php')) {
		require_once($ipath.'/language/'.$alang.'.php');
	} else if (file_exists($ipath.'/language/'.$mainframe->getCfg('alang').'.php')) {
		require_once($ipath.'/language/'.$mainframe->getCfg('alang').'.php');
	} else { 
		require_once($ipath.'/language/english.php');
	}
	$lng = new clsEventCalendarLanguage();

	$install_errors = array();

	// Set default parameters
	$query = "UPDATE #__components SET params='".
			"newauto=\n".
			"newall=\n".
			"changesauto=\n".
			"changesall=\n".
			"timetable=a:1:{i:0;s:10:\"0:00-23:59\";}\n".
			"timetable_selected=0\n".
			"who_can_post_events=25\n".
			"who_can_edit_events=25\n".
			"week_startingday=0\n".
			"show_weeknumber=1\n".
			"week_number_links=1\n".
			"default_view=monthview\n".
			"view_nav=1\n".
			"view_print=1\n".
			"view_rss=1\n".
			"view_catlist=1\n".
			"view_periodicity=0\n".
			"view_reservation=1\n".
			"col_catlist=3\n".
			"rss_cache=0\n".
			"rss_cachetime=3600\n".
			"rss_num=5\n".
			"rss_title=EventCalendar RSS feeds\n".
			"rss_description=EventCalendar web syndication.\n".
			"rss_img=elxis_rss.png\n".
			"rss_imgalt=EventCalendar RSS feeds\n".
			"rss_limittext=0\n".
			"rss_textlength=20\n".
			"rss_multilang=0\n".
			"rss_live=0\n".
			"pp_mail=mypaypal@email.com\n".
			"p_curlist=EUR\n".
			"css_filename=default.css\n'".
			"\n WHERE admin_menu_link='option=com_eventcalendar' AND link='option=com_eventcalendar'";
	$database->setQuery($query);
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_PARAMS; }

	// Set up new icons for admin menu
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/edit.png' WHERE admin_menu_link='option=com_eventcalendar'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_EVENTS; }

	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/edit.png' WHERE admin_menu_link='option=com_eventcalendar&task=anniversries'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_ANNI; }
	
	$database->setQuery("UPDATE #__components SET admin_menu_img='../administrator/components/com_eventcalendar/images/calendar_menu.png' WHERE admin_menu_link='option=com_eventcalendar' AND link='option=com_eventcalendar'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_MAIN; }
	
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/categories.png' WHERE admin_menu_link='option=com_categories&section=com_eventcalendar'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_CATEGORIES; }
	
	$database->setQuery("UPDATE #__components SET admin_menu_img='js/ThemeOffice/config.png' WHERE admin_menu_link='option=com_eventcalendar&task=cp'");
	if (!$database->query()) { $install_errors[] = $lng->INS_ERROR_MENU_CONFIGURATION; }

	//Add SEO Pro functionality
	$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_eventcalendar/seopro.com_eventcalendar.php';
	$destination = $mainframe->getCfg('absolute_path').'/includes/seopro/com_eventcalendar.php';
	$success = $fmanager->copyFile($source, $destination);
	if (!$success) { $install_errors[] = $lng->INS_ERROR_SEOPRO; }

	//Add IOS Sitemap extention
	if ( (file_exists($mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/')) && (!file_exists($mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/extensions/eventcalendar.sitemap.php')) )  {
		$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_eventcalendar/eventcalendar.sitemap.php';
		$destination = $mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/extensions/eventcalendar.sitemap.php';
		$success = $fmanager->copyFile($source, $destination);
		if (!$success) { $install_errors[] = $lng->INS_ERROR_SITEMAP; }
	}
?>

	<table class="adminheading">
		<tr>
			<th class="install"><?php echo $lng->INS_HEADER; ?></th>
		</tr>
	</table>

	<table class="adminform" style="empty-cells:hide">
	<?php 
	if (count($install_errors) > 0) { ?>
		<tr>
			<td class="menudottedline" style="background-color:red;">
				<?php echo $lng->INS_ERROR_NOTICE_TITLE; ?>:
			</td>
			<td>
				<?php 
				foreach ($install_errors as $install_error) {
					echo $install_error."<br />\n";
				}
				echo $lng->INS_ERROR_NOTICE;
				?>
			</td>
		</tr>
	<?php 
	}
	?>
		<tr>
			<th colspan="2"><?php echo $lng->INS_TITLE; ?></th>
		</tr>
		<tr>
			<td colspan="2"><?php echo $lng->INS_BODY; ?></td>
		</tr>
	</table>
<?php
}
?>
