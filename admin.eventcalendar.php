<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Backend Event Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Check if the user is allowed to access this component. If not then re-direct him to the administration's front-page.
if (!($acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'all' )
	| $acl->acl_check( 'administration', 'edit', 'users', $my->usertype, 'components', 'com_eventcalendar' ))) {
	mosRedirect( 'index2.php', $adminLanguage->A_NOT_AUTH );
}

// Includes
require_once($mainframe->getCfg('absolute_path' ).'/components/com_eventcalendar/eventcalendar.class.php'); // Component's general classes
require_once($mainframe->getCfg('absolute_path' ).'/administrator/components/com_eventcalendar/admin.eventcalendar.html.php'); // Component's html file for the administration area
require_once($mainframe->getCfg('absolute_path' ).'/administrator/includes/pageNavigation.php'); // Elxis core pageNavigation component

/*************************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S BACK-END FUNCTIONALITY  */
/*************************************************************************/
class clsEventCalendarAdmin {
	
	// Initialize variables
	public $task = ''; // component's current task
	public $apath = ''; // component's back-end absolute path
	public $hpath = ''; // component's front-end absolute path
	public $live_apath = ''; // component's back-end absolute path
	public $live_hpath = ''; // component's front-end absolute path
	public $lng = null; // component's language
	public $event_id = '';
	public $cat_id = '';
	public $cid = '';
	public $field = '';
	public $order = '';
	
	/****************************/
	/*  The class' constructor  */
	/****************************/ 
	public function __construct() {
		global $alang, $mainframe; $mosConfig_live_site; // Pass global variables or objects

        // Set variables
        $this->task = htmlspecialchars(mosGetParam($_REQUEST, 'task', 'events'));
        $this->apath = $mainframe->getCfg('absolute_path').'/administrator/components/com_eventcalendar';
        $this->hpath = $mainframe->getCfg('absolute_path').'/components/com_eventcalendar';
        $this->live_apath = $mainframe->getCfg('live_site').'/administrator/components/com_eventcalendar';
        $this->live_hpath = $mainframe->getCfg('live_site').'/components/com_eventcalendar';
		$this->event_id = mosGetParam( $_REQUEST, 'cid', array(0) );
		$this->cat_id = mosGetParam( $_REQUEST, 'catid', '0' );
		$this->cid = $event_id;
		$this->field = mosGetParam($_REQUEST, 'field', false);
		$this->order = mosGetParam($_REQUEST, 'order', 'none');

        // Load component's language
		if (file_exists($this->hpath.'/language/'.$alang.'.php')) { 
			require_once($this->hpath.'/language/'.$alang.'.php');
		} else if (file_exists($this->hpath.'/language/'.$mainframe->getCfg('alang').'.php')) { 
			require_once($this->hpath.'/language/'.$mainframe->getCfg('alang').'.php');
		} else { 
			require_once($this->hpath.'/language/english.php');
		}
		$this->lng = new clsEventCalendarLanguage();
	}

	/******************************/
	/*  The class' main function  */
	/******************************/ 	
	public function main() {
	
		switch ($this->task) {
			// Control Panel
			case 'cp':
				$this->showControlPanel();
				break;
			case 'cp_apply':
			case 'cp_save':
				$this->saveConfiguration();
				break;
			case 'cp_cancel':
				mosRedirect( 'index2.php?option=com_eventcalendar' );
				break;

			// Templates
			case 'cp_addtemp':
				$this->addTemplate();
				break;
			case 'cp_remtemp':
				$this->removeTemplate();
				break;
				
			// Events
			case 'edit':
				$this->event_id = (is_array($this->event_id))?$this->event_id[0]:$this->event_id;
				$this->editEvent($this->event_id);
				break;
			case 'new':
				$this->editEvent();
				break;
			case 'save':
			case 'apply':
				$this->saveEvent(); //
				break;
			case 'cancel':
				$this->cancelEvent(); //
				break;
			case 'publish':
				$this->publishEvent($this->event_id, 1); //
				break;
			case 'unpublish':
				$this->publishEvent($this->event_id, 0); //
				break;
			case 'remove':
				$this->deleteEvent($this->event_id); //
				break;

			// AJAX
			case 'validate':
				$this->validateSEO();
				break;
			case 'suggest':
			    $this->suggestSEO();
				break;
			case 'ajaxpub':
				$this->ajaxchangeContent();
				break;
			
			// Default action
			case 'events':
			default:
				$this->listEvents();
				break;
		}
	}
	
	/**********************/
	/*  Template handler  */
	/**********************/ 		
	protected function addTemplate() {
		global $fmanager, $mosConfig_absolute_path, $adminLanguage;
		
		require_once($mosConfig_absolute_path.'/administrator/includes/pcl/pclzip.lib.php');
		
		$size_str = @ini_get('upload_max_filesize');
		if ($size_str) {
			$i=0;
			while(ctype_digit($size_str[$i])) {
				$size .= $size_str[$i];
				$i++;
			}
			if ($size_str[$i]=="M"||$size_str[$i]=="m") {
				$size = $size*1024*1024;
			} else if ($size_str[$i]=="K"||$size_str[$i]=="k") {
				$size = $size*1024;
			} else {
				$size = 1024*1024*1024;
			}
		} else {
			$size = 2048*1024*1024;
		}
		
		$validFileTypes = array('zip');
		$userfile = $_FILES['userfile'];
		if ( ($userfile['name'] <> '') || ($userfile['size'] <= $size) ) {
			$source = $userfile['tmp_name'];
			if (in_array($fmanager->fileExt($userfile['name']), $validFileTypes)) {
				$dest = $mosConfig_absolute_path.'/tmpr/'.$userfile['name'];
				$fmanager->upload($source, $dest);
			} else {
				mosRedirect( 'index2.php?option=com_eventcalendar&task=cp', $this->lng->CP_MSG_TEMPLATE_ADD_ERROR );
			}
		} else {
			mosRedirect( 'index2.php?option=com_eventcalendar&task=cp', $this->lng->CP_MSG_TEMPLATE_ADD_ERROR );
		}
	
		$zipfile = $dest;

		@chdir($this->hpath.'/templates');
		if (is_file($zipfile)) {
			$path_parts = pathinfo($zipfile);
			if (strtolower($path_parts['extension']) == 'zip') {
				$zip = new PclZip($zipfile);
				$list = $zip->extract(".");
			} else {
				mosRedirect( 'index2.php?option=com_eventcalendar&task=cp', $adminLanguage->A_CMP_ME_FINOZIP );
			}
		} else {
			mosRedirect( 'index2.php?option=com_eventcalendar&task=cp', $adminLanguage->A_CMP_ME_FILNEX );
		}	
		mosRedirect( 'index2.php?option=com_eventcalendar&task=cp', $this->lng->CP_MSG_TEMPLATE_ADD_OK );
	}

	protected function removeTemplate() {
		global $fmanager;
		
		$css_filename = mosGetParam( $_REQUEST, 'css_filename', '' );

		if ($css_filename) {
			if ( ($fmanager->deleteFile($this->hpath."/templates/".$css_filename)) && ($fmanager->deleteFolder($this->hpath."/templates/f_".$css_filename."/")) ) {
				mosRedirect( 'index2.php?option=com_eventcalendar&task=cp', $this->lng->CP_MSG_TEMPLATE_REM_OK );
			}
		} else {
			mosRedirect( 'index2.php?option=com_eventcalendar&task=cp', $this->lng->CP_MSG_TEMPLATE_REM_ERROR );
		}
	}
	
	/***********************************/
	/*  Prepare to show control panel  */
	/***********************************/ 	
	protected function showControlPanel() {
		global $mainframe, $database; // Pass global variables or objects

		// Initialize variables
		$borders = null; // store the borders of Public Backend user group
		$groups = null; // store Public Backend groups
		$text = ''; // get params from database
		$config_values = null; // store params
		
		// Get Public Backend user groups
		$groups = $mainframe->backGroups();

		// Load params values
		$database->setQuery( "SELECT params FROM #__components WHERE link = 'option=com_eventcalendar'" );
		$text = $database->loadResult();
		$config_values = new mosParameters( $text );

		// Show CP html
		clsEventCalendarAdminHTML::showControlPanelHTML( $config_values, $groups );
	}
	
	/************************/
	/*  Save configuration  */
	/************************/ 	
	protected function saveConfiguration() {
		global $database, $mainframe, $fmanager; // Pass global variables or objects

		// Initialize variables
		$params = new mosParameters('');
		
		// Get group_role_permissions from Post-Data
		$group_roles_new_auto = mosGetParam( $_REQUEST, 'newauto', array('') );
		$group_roles_new_all = mosGetParam( $_REQUEST, 'newall', array('') );
		$group_roles_changes_auto = mosGetParam( $_REQUEST, 'changesauto', array('') );
		$group_roles_changes_all = mosGetParam( $_REQUEST, 'changesall', array('') );
			$params->set( 'newauto', implode( ',', $group_roles_new_auto ) );
			$params->set( 'newall', implode( ',', $group_roles_new_all ) );
			$params->set( 'changesauto', implode( ',', $group_roles_changes_auto ) );
			$params->set( 'changesall',	implode( ',', $group_roles_changes_all ) );

		// Get timetable from Post Data and implode it to an array
		$timetable = mosGetParam( $_REQUEST, 'timetable', array() );
		$timetable = serialize( $timetable );
			$params->set( 'timetable', $timetable);
		$timetable_selected = mosGetParam( $_REQUEST, 'timetable_selected', 0 );
			$params->set( 'timetable_selected', $timetable_selected);

		// Get the rest settings
		$who_can_post_events = intval(mosGetParam( $_REQUEST, 'who_can_post_events', 2 ));
		$who_can_edit_events = intval(mosGetParam( $_REQUEST, 'who_can_edit_events', 2 ));
		$week_startingday = intval(mosGetParam( $_REQUEST, 'week_startingday', 0 ));
		$show_weeknumber = intval(mosGetParam( $_REQUEST, 'show_weeknumber', 1 ));
		$week_number_links = intval(mosGetParam( $_REQUEST, 'week_number_links', 0 ));
		$default_view = mosGetParam( $_REQUEST, 'default_view', 'monthview' );
		$view_nav = mosGetParam( $_REQUEST, 'view_nav', true );
		$view_print = mosGetParam( $_REQUEST, 'view_print', true );
		$view_rss = mosGetParam( $_REQUEST, 'view_rss', true );
		$view_catlist = mosGetParam( $_REQUEST, 'view_catlist', true );
		$view_periodicity = mosGetParam( $_REQUEST, 'view_periodicity', true );
		$view_reservation = mosGetParam( $_REQUEST, 'view_reservation', false );
		$col_catlist = mosGetParam( $_REQUEST, 'col_catlist', 2 );
		$rss_cache = mosGetParam( $_REQUEST, 'rss_cache', '0' );
		$rss_cachetime = mosGetParam( $_REQUEST, 'rss_cachetime', '3600' );
		$rss_num = mosGetParam( $_REQUEST, 'rss_num', '5' );
		$rss_title = mosGetParam( $_REQUEST, 'rss_title', 'EventCalendar RSS feeds' );
		$rss_description = mosGetParam( $_REQUEST, 'rss_description', 'EventCalendar web syndication.' );
		$rss_img = mosGetParam( $_REQUEST, 'rss_img', 'elxis_rss.png' );
		$rss_imgalt = mosGetParam( $_REQUEST, 'rss_imgalt', 'EventCalendar RSS feeds' );
		$rss_limittext = mosGetParam( $_REQUEST, 'rss_limittext', '0' );
		$rss_textlength = mosGetParam( $_REQUEST, 'rss_textlength', '20' );
		$rss_multilang = mosGetParam( $_REQUEST, 'rss_multilang', '0' );
		$rss_live = mosGetParam( $_REQUEST, 'rss_live', '0' );
		$pp_mail = mosGetParam( $_REQUEST, 'pp_mail', 'mypaypal@email.com' );
		$pp_curlist = mosGetParam( $_REQUEST, 'pp_curlist', 'EUR' );
		$css_filename = mosGetParam( $_REQUEST, 'css_filename', '' );
		$date_format = mosGetParam( $_REQUEST, 'date_format', 'd-m-Y' );
			$params->set( 'who_can_post_events', $who_can_post_events );
			$params->set( 'who_can_edit_events', $who_can_edit_events );
			$params->set( 'week_startingday', $week_startingday );
			$params->set( 'show_weeknumber', $show_weeknumber );
			$params->set( 'week_number_links', $week_number_links );
			$params->set( 'default_view', $default_view );
			$params->set( 'view_nav', $view_nav );
			$params->set( 'view_print', $view_print );
			$params->set( 'view_rss', $view_rss );
			$params->set( 'view_catlist', $view_catlist );
			$params->set( 'view_periodicity', $view_periodicity );
			$params->set( 'view_reservation', $view_reservation );
			$params->set( 'col_catlist', $col_catlist );
			$params->set( 'rss_cache', $rss_cache );
			$params->set( 'rss_cachetime', $rss_cachetime );
			$params->set( 'rss_num', $rss_num );
			$params->set( 'rss_title', $rss_title );
			$params->set( 'rss_description', $rss_description );
			$params->set( 'rss_img', $rss_img );
			$params->set( 'rss_imgalt', $rss_imgalt );
			$params->set( 'rss_multilang', $rss_multilang );
			$params->set( 'rss_limittext', $rss_limittext );
			$params->set( 'rss_textlength', $rss_textlength );
			$params->set( 'rss_live', $rss_live );
			$params->set( 'pp_mail', $pp_mail );
			$params->set( 'pp_curlist', $pp_curlist );
			$params->set( 'css_filename', $css_filename );
			$params->set( 'date_format', $date_format );

		// Store params to database
		$params_as_text = array();
		foreach ($params->_params as $k=>$v) {
			$params_as_text[] = "$k=$v";
		}
		$database->setQuery( "UPDATE #__components SET params = \n'" . mosParameters::textareaHandling( $params_as_text ) . "'\n WHERE link = 'option=com_eventcalendar'" );
		if ($database->query()) {
			$msg = $this->lng->CP_MSG_CONFIGURATION_STORED;
		} else {
			$msg = $this->lng->CP_MSG_CONFIGURATION_ERROR;
		}

		// Activate IOS Sitemap extension
		if (mosGetParam( $_REQUEST, 'sitemap_ext') == 'on') {
			$source = $mainframe->getCfg('absolute_path').'/administrator/components/com_eventcalendar/eventcalendar.sitemap.php';
			$destination = $mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/extensions/eventcalendar.sitemap.php';
			$success = $fmanager->copyFile($source, $destination);
		}

		// Check if task is apply or save and redirect respectively
		switch ( $this->task ) {
			case 'cp_apply':
				mosRedirect( 'index2.php?option=com_eventcalendar&task=cp', $msg );
				break;
			case 'cp_save':
			default:
				mosRedirect( 'index2.php?option=com_eventcalendar', $msg );
				break;
		}
	}
	
	/*********************************/
	/*  Prepare to show events list  */
	/*********************************/
	protected function listEvents () {
		global $mainframe, $database, $mosConfig_list_limit; // Pass global variables or objects

		if ( $this->field && $this->order <> "none" ) {
			$ordering = "\n ORDER BY e.$this->field " . $this->order;
		} else {
			$ordering = "\n ORDER BY e.start_date DESC, e.recur_week";
		}

		if (isset($searchstring)) $searcharray[] = $searchstring;
		
		if(($this->cat_id <> 0) && is_numeric($this->cat_id) ) {
			$searcharray[] = " e.catid=" . $this->cat_id;
		} else if ($this->cat_id === "old") {
			$today = getDate();
			$searcharray[] = "e.end_date < $today[0]";
		}

		if ($this->task == "unpublished") {$searcharray[] = "e.published = '0'";}

		//filter-options
		$search = mosGetParam($_REQUEST, "search", false);
		if ($search) $searcharray[] = "(e.title LIKE '%$search%' OR e.description LIKE '%$search%')";
		$searchstring = (@$searcharray)?implode(" AND ", $searcharray):"1";
		$database->setQuery( "SELECT COUNT(*) FROM #__eventcalendar e WHERE $searchstring");

		// page navigation
		$total = $database->loadResult();
		$limitstart = $mainframe->getUserStateFromRequest( "view{eventcalendar}", 'limitstart', 0 );
		$limit = $mainframe->getUserStateFromRequest( "viewlistlimit", 'limit', $mosConfig_list_limit );    
		$pageNav = new mosPageNav( $total, $limitstart, $limit );

		$database->setQuery(
			"SELECT e.*, u.name AS editor, c.title AS cat_name, c.params AS cat_params FROM #__eventcalendar e" .
			"\n LEFT JOIN #__users u ON u.id = e.checked_out" .
			"\n LEFT JOIN #__categories c ON c.id = e.catid" .
			"\n WHERE " . $searchstring .
			$ordering, '#__', $pageNav->limit, $pageNav->limitstart
		);
		$results = $database->loadObjectList();
		$categories[] = mosHTML::makeOption( "0", $this->lng->LST_ALL_EVENTS );
		$categories[] = mosHTML::makeOption( "old", $this->lng->LST_OLD_EVENTS );
		
		$query = "SELECT id AS value, name AS text"
			. "\n FROM #__categories"
			. "\n WHERE section = 'com_eventcalendar'"
			. "\n AND published = '1'";
		$database->setQuery( $query );
		$categories = array_merge( $categories, $database->loadObjectList() );

		$catlist = mosHTML::selectList( $categories, 'catid', 'class="inputbox" size="1" onchange="document.adminForm.submit();"', 'value', 'text', $this->cat_id );

		clsEventCalendarAdminHTML::listEventsHTML($results, $pageNav, $catlist );	
	}

	/*******************************/
	/*  Prepare to edit/add event  */
	/*******************************/
	protected function editEvent($existing = NULL) {
		global $my, $database;

		if (is_numeric($existing)) {
			$event = new mosEventCalendar_Event($database);
			$event->load($existing);
			$existing = $event;
			$existing->checkout( $my->id );
		}
		clsEventCalendarAdminHTML::editEventHTML($existing);	
	}
	
	/****************/
	/*  Save event  */
	/****************/
	protected function saveEvent() {
		global $objEventCalendar, $database, $adminLanguage, $my;

		//CSRF prevention
		$tokname = 'token'.$my->id;
		if ( !isset($_POST[$tokname]) || !isset($_SESSION[$tokname]) || ($_POST[$tokname] != $_SESSION[$tokname]) ) {
			die( 'Detected CSRF attack! Someone is forging your requests.' );
		}

		foreach ($_POST['languages'] as $xlang) {
			if (trim($xlang) == '') { $newlangs = ''; }
		}
		if (!isset($newlangs)) {
			$newlangs = implode(',', $_POST['languages']);
		}
		$_POST['language'] = $newlangs;

		$event = new mosEventCalendar_Event($database);
		$event->bind( $_POST );
		if ($event->check($error)) {
			if ($event->store()) {
				// Update category color
				$cat_col = new mosCategory( $database );
				$cat_col->load($_POST['catid']);
				$cat_col_par = new mosParameters( $cat_col->params );

				if($_POST['color'.$_POST['id'].'display'] != "")
					$cat_col_par->set("color", $_POST['color'.$_POST['id'].'display']);
	
				$cat_col_par_txt = array();
				foreach ($cat_col_par->_params as $k=>$v) {
					$cat_col_par_txt[] = "$k=$v";
				}
				$cat_col->params = mosParameters::textareaHandling( $cat_col_par_txt );
				if ($cat_col->store()) {								
					$msg = $objEventCalendar->lng->EDT_MSG_STORED;
				} else {
					echo "<script type=\"text/javascript\">alert('".$objEventCalendar->lng->GEN_COMPONENT_TITLE.": ".$objEventCalendar->lng->EDT_MSG_ERROR."'); window.history.go(-1);</script>"._LEND;
					exit();
				}
			} else {
				echo "<script type=\"text/javascript\">alert('".$objEventCalendar->lng->GEN_COMPONENT_TITLE.": ".$objEventCalendar->lng->EDT_MSG_ERROR."'); window.history.go(-1);</script>"._LEND;
				exit();
			}
		} else {
			echo "<script type=\"text/javascript\">alert('".$objEventCalendar->lng->GEN_COMPONENT_TITLE.": ".$error."'); window.history.go(-1);</script>"._LEND;
			exit();
		}

		// Validate SEO
		require_once($this->apath.'/eventcalendar.seovs.class.php');
		$seo = new seovsEventCalendar('com_eventcalendar', '', $event->seotitle);
		$seo->id = intval($event->id);
		$seo->catid = intval($event->catid);
		$seoval = $seo->validate();
		if (!$seoval) {
			echo "<script type=\"text/javascript\">alert('".$adminLanguage->A_SEOTITLE.": ".$seo->message."'); window.history.go(-1);</script>"._LEND;
			exit();
		}
    
		// Check if task is apply or save and redirect respectively
		switch ( $this->task ) {
			case 'apply':
				mosRedirect( 'index2.php?option=com_eventcalendar&task=edit&hidemainmenu=1&cid='.$event->id, $msg );
				break;
			case 'save':
			default:
				mosRedirect( 'index2.php?option=com_eventcalendar', $msg );
				break;
		}	
	}
	
	/*********************/
	/*  Cancel add/edit  */
	/*********************/
	protected function cancelEvent() {
		global $database;
		
		$event = new mosEventCalendar_Event($database);
		$event->checkin($_POST['id']); //the id is defined globally
		$this->cat_id = 0;
		
		$this->listEvents();
	}
	
	/******************/
	/*  Delete event  */
	/******************/
	protected function deleteEvent($itemids) {
		global $database;
		
		$event = new mosEventCalendar_Event($database);
		foreach($itemids AS $itemid) {
			$event->delete($itemid);	
		}
		
		$this->listEvents();
	}
	
	/***********************/
	/*  Un-/Publish event  */
	/***********************/
	protected function publishEvent($event_id, $value) {
		global $database;
		
		$event = new mosEventCalendar_Event($database);
		foreach($event_id AS $id) {
		  $event->load($id);
		  $event->published = ''.intval($value).'';
		  $event->store();
		}
		$this->listEvents();
	}

	/*****************************/
	/* Validate SEO Title (AJAX) */
	/*****************************/
	protected function validateSEO() {
		global $mainframe;

		$coid = intval(mosGetParam($_POST, 'coid', 0));
		$cocatid = intval(mosGetParam($_POST, 'cocatid', 0));
		$seotitle = eUTF::utf8_trim(mosGetParam($_POST, 'seotitle', ''));

		require_once($this->apath.'/eventcalendar.seovs.class.php');
		$seo = new seovsEventCalendar('com_eventcalendar', '', $seotitle);
		$seo->id = $coid;
		$seo->catid = $cocatid;
		$seo->validate();
		echo $seo->message;
		exit();
	}


	/****************************/
	/* Suggest SEO Title (AJAX) */
	/****************************/
	protected function suggestSEO() {
		global $mainframe;

		$coid = intval(mosGetParam($_POST, 'coid', 0));
		$cocatid = intval(mosGetParam($_POST, 'cocatid', 0));
		$cotitle = mosGetParam($_POST, 'cotitle', '');

		require_once($this->apath.'/eventcalendar.seovs.class.php');
		$seo = new seovsEventCalendar('com_eventcalendar', $cotitle);
		$seo->id = $coid;
		$seo->catid = $cocatid;
		$sname = $seo->suggest();

		@ob_end_clean();
		@header('Content-Type: text/plain; Charset: utf-8');
		if ($sname) {
			echo '|1|'.$sname;
		} else {
			echo '|0|'.$seo->message;
		}
		exit();
	}

	/*******************************/
	/* Change publish state (AJAX) */
	/*******************************/
	function ajaxchangeContent() {
		global $database, $my, $adminLanguage;

		$elem = intval(mosGetParam($_REQUEST, 'elem', 0));
		$id = intval(mosGetParam($_REQUEST, 'id', 0));
		$state = intval(mosGetParam($_REQUEST, 'state', 0));

		if (!$id) {
			echo '<img src="../includes/js/ThemeOffice/warning.png" width="16" height="16" border="0" title="'.$adminLanguage->A_ERROR.': Invalid Item id" />'._LEND;
			exit();
		}

		$error = 0;
		$database->setQuery( "UPDATE #__eventcalendar SET published='$state' WHERE id='$id' AND (checked_out=0 OR (checked_out='".$my->id."'))");
		if (!$database->query()) { $error = 1; }

		if ($error) { $state = $state ? 0 : 1; }
		$img = $state ? 'publish_g.png' : 'publish_x.png';
		$alt = $state ? $adminLanguage->A_PUBLISHED : $adminLanguage->A_UNPUBLISHED;	?>
		<a href="javascript: void(0);" 
		onclick="changeContentState('<?php echo $elem; ?>', '<?php echo $id; ?>', '<?php echo ($state) ? 0 : 1; ?>');" title="<?php echo $alt; ?>">
		<img src="images/<?php echo $img; ?>" width="12" height="12" border="0" alt="<?php echo $alt; ?>" /></a>
		<?php exit();
	}

}

// Initiate the class/ and execute it, then unset the 
// object in order to free the allocated PHP memory.
$objEventCalendar = new clsEventCalendarAdmin();
$objEventCalendar->main();
unset($objEventCalendar);

?>
