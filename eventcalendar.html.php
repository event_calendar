<?php 

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Frontend Event Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link			
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Includes

/******************************************************************************/
/*  THE CLASS THAT WILL CONTAIN THE COMPONENT'S FRONT-END HTML FUNCTIONALITY  */
/******************************************************************************/
class clsEventCalendarHTML {

	/************************/
	/*  Display the header  */
	/************************/ 
	protected function displayHeaderHTML($menuitem, $catids="") {
		global $objEventCalendar, $my, $mosConfig_live_site;
		
		// Draw a simple html header
		$permits = explode(',', $my->allowed); ?>
		
		<script type="text/javascript">
			function shadeOn( item ) {
				item.src = "<?php echo $objEventCalendar->live_path; ?>/templates/f_<?php echo $objEventCalendar->params['com']->get('css_filename'); ?>/view_" + item.id + "_f2.png";
			}

			function shadeOff( item ) {
				item.src = "<?php echo $objEventCalendar->live_path; ?>/templates/f_<?php echo $objEventCalendar->params['com']->get('css_filename'); ?>/view_" + item.id + ".png";
			}
		</script>
			
		<table class="event_calendar_header_table"><tr>
			<?php if ( ($objEventCalendar->task != 'editevent') && ($objEventCalendar->task != 'printevent') ) { ?>
				<td class="edit">
					<?php if (in_array($objEventCalendar->params['com']->get('who_can_post_events'), $permits) )  { ?>
						<img src="<?php echo $objEventCalendar->live_path; ?>/templates/f_<?php echo $objEventCalendar->params['com']->get('css_filename'); ?>/new.png" width="16" height="16" border="0"/>
						<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=editevent".$menuitem) ?>" title="<?php echo _E_ADDCONTENT; ?>"><?php echo _E_ADD; ?></a>
					<?php } ?>
					<?php if ( (in_array($objEventCalendar->params['com']->get('who_can_edit_events'), $permits)) && ($objEventCalendar->task == 'eventview') ) { ?>
						&nbsp;&nbsp;
						<img src="<?php echo $objEventCalendar->live_path; ?>/templates/f_<?php echo $objEventCalendar->params['com']->get('css_filename'); ?>/edit.png" width="16" height="16" border="0"/>
						<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=editevent&eventid=".$objEventCalendar->eventid.$menuitem) ?>" title="<?php echo _E_EDITCONTENT; ?>"><?php echo _E_EDIT; ?></a>
					<?php } ?>
				</td>
			<?php } ?>
			<?php if ( $objEventCalendar->params['com']->get('view_nav', true) ) { ?>
				<td class="nav">
					<b><?php echo ($objEventCalendar->task != 'printevent')?$objEventCalendar->lng->FRONT_VIEW.':':''; ?>
					
					<?php switch ($objEventCalendar->task) {
						case 'monthview': ?>
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=dayview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month."&day=".$objEventCalendar->day.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_day.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_DAY; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="day" id="day" /></a>&nbsp;&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=weekview&year=".$objEventCalendar->year."&week=".$objEventCalendar->week.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_week.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_WEEK; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="week" id="week" /></a>&nbsp;&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_month.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_MONTH; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="month" id="month" /></a>&nbsp;&nbsp;
							<?php break;
						case 'weekview': ?>
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=dayview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month."&day=".$objEventCalendar->day.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_day.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_DAY; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="day" id="day" /></a>&nbsp;&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=weekview&year=".$objEventCalendar->year."&week=".$objEventCalendar->week.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_week.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_WEEK; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="week" id="week" /></a>&nbsp;&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_month.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_MONTH; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="month" id="month" /></a>&nbsp;&nbsp;
							<?php break;
						case 'dayview': ?>
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=dayview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month."&day=".$objEventCalendar->day.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_day.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_DAY; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="day" id="day" /></a>&nbsp;&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=weekview&year=".$objEventCalendar->year."&week=".$objEventCalendar->week.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_week.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_WEEK; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="week" id="week" /></a>&nbsp;&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_month.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_MONTH; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="month" id="month" /></a>&nbsp;&nbsp;
							<?php break;
						case 'eventview':
							if ($objEventCalendar->task != 'printevent') { ?>
								<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=dayview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month."&day=".$objEventCalendar->day.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_day.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_DAY; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="day" id="day" /></a>&nbsp;&nbsp;
								<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=weekview&year=".$objEventCalendar->year."&week=".$objEventCalendar->week.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_week.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_WEEK; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="week" id="week" /></a>&nbsp;&nbsp;
								<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month.$catids.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_month.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_MONTH; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="month" id="month" /></a>&nbsp;&nbsp;
							<?php }
							if ( $objEventCalendar->params['com']->get('view_print', true) ) {
								echo ($objEventCalendar->task != 'printevent')?'|&nbsp;':'';
								$image = mosAdminMenus::ImageCheck( 'printButton.png', '/components/com_eventcalendar/templates/f_'.$objEventCalendar->params['com']->get('css_filename', 'default.css').'/', NULL, NULL, _CMN_PRINT, 1, 'right' );
								$link = 'index2.php?option=com_eventcalendar&task=printevent&eventid='.$objEventCalendar->eventid.'&Itemid='.$Itemid;
								$status = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';
																			 
								if ( $objEventCalendar->task == 'printevent' ) {
									echo '<a href="javascript:void(0);" onclick="javascript:window.print(); return false" title="'._CMN_PRINT.'">'._LEND;
								} else {
									echo '<a href="javascript:void(0);" onclick="javascript:window.open(\''.$link.'\', \'PrintWindow\', \''.$status.'\');" title="'._CMN_PRINT.'">'._LEND;
								}
								echo $image._LEND;
								echo '</a> '._LEND;
							}
							break;
						case 'catview': ?>
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=dayview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month."&day=".$objEventCalendar->day.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_day.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_DAY; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="day" id="day" /></a>&nbsp;&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=weekview&year=".$objEventCalendar->year."&week=".$objEventCalendar->week.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_week.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_WEEK; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="week" id="week" /></a>&nbsp;&nbsp;
							<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".$objEventCalendar->year."&month=".$objEventCalendar->month.$menuitem ) ?>"><img src="<?php echo $objEventCalendar->live_path.'/templates/f_'.$objEventCalendar->params['com']->get('css_filename').'/view_month.png'; ?>" width="16" height="16" border="0" title="<?php echo $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_MONTH; ?>" onmouseover="shadeOn(this)" onmouseout="shadeOff(this)" name="month" id="month" /></a>&nbsp;&nbsp;
							<?php break;
					} 
					if ( $objEventCalendar->params['com']->get('view_rss', true) ) {
						echo '|&nbsp;';
						$image = mosAdminMenus::ImageCheck( 'rss.png', '/components/com_eventcalendar/templates/f_'.$objEventCalendar->params['com']->get('css_filename', 'default.css').'/', NULL, NULL, 'RSS feeds', 1, 'right' );
						echo '<a href='.sefReltoAbs('index.php?option=com_eventcalendar&task=syndication', EVCALBASE.'/syndication.html').' title="RSS feeds">'.$image.'</a>';
					}
					?></b>
				</td>
			<?php } ?>
		</tr></table>
	<?php
	}
	
	/************************/
	/*  Display month view  */
	/************************/
	static public function displayMonthViewHTML( $weeknrs ) {
		global $mainframe, $objEventCalendar, $Itemid;

		$mainframe->setPageTitle( $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_MONTH." ".$objEventCalendar->lng->FRONT_VIEW );

		// Print components css information and header inclusions
		$linktag = '<link href="'.$objEventCalendar->live_path.'/templates/'.$objEventCalendar->params['com']->get('css_filename', 'default.css').'" rel="stylesheet" type="text/css" />';
		if (mosGetParam( $_REQUEST, 'pop', 0)) {
			echo $linktag;
		} else {
			$mainframe->addCustomHeadTag($linktag);
		}
		
		// Load core HTML library
		mosCommonHTML::loadOverlib();

		// Set some variables
		$menuitem = "&Itemid=".$Itemid;
		if (isset($objEventCalendar->catid)) {
			$catids = (is_array($objEventCalendar->catid))?implode("+", $objEventCalendar->catid):$objEventCalendar->catid;
		}
		$catids = ($catids == "")?"":"&catid=".$catids;
		
		// Draw a simple html header
		clsEventCalendarHTML::displayHeaderHTML($menuitem, $catids);

		// Display month table
		$thisMonth = mktime( 0, 0, 0, $objEventCalendar->month, 1, $objEventCalendar->year );
		$lastMonth = strtotime( "1 month ago", $thisMonth );
		$nextMonth = strtotime( "+ 1 month", $thisMonth );
		?>
		<table class="event_calendar_month_table">
			<tr>
				<?php if ($objEventCalendar->params['com']->get('show_weeknumber', true)) { ?>
					<td rowspan="2"></td>
			 	<?php } ?>
				<td colspan="2" class="nav_other_month"><a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".date("Y", $lastMonth)."&month=".date("m", $lastMonth). $catids . $menuitem ) ?>"><?php echo strftime("%B", $lastMonth) ?></a></td>
				<td colspan="3" class="nav_cur_month"><?php echo strftime( "%B", $thisMonth ) . " " . strftime("%Y", $thisMonth) ?></td>
				<td colspan="2" class="nav_other_month"><a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=monthview&year=".date("Y", $nextMonth)."&month=".date("m", $nextMonth). $catids . $menuitem ) ?>"><?php echo strftime("%B", $nextMonth) ?></a></td>
			</tr>
			<tr>
				<?php
				for($dcount = 0; $dcount < 7; $dcount++) {?> 
					<td class="weekdays_name<?php echo (strftime("%w", $objEventCalendar->calendar[$dcount]) == 0)?'_sunday':''; ?>"><?php echo strftime("%A", $objEventCalendar->calendar[$dcount]) ?></td>
				<?php }?>
			</tr>
			<?php for ($wcount = 1; $wcount <= count($weeknrs); $wcount++) { ?>
				<tr>
					<?php
					if ($objEventCalendar->params['com']->get('show_weeknumber', true)) { ?>
						<td class="week_nrs">
							<?php 
								if ($objEventCalendar->params['com']->get('week_number_links', true)) { //(($objEventCalendar->calendar[0] <> NULL)?$objEventCalendar->calendar[0]:$objEventCalendar->calendar[6] - 604800)
									?>
									<a href="<?php echo sefRelToAbs( "index.php?option=com_eventcalendar&task=weekview&year=".$objEventCalendar->year."&week=".$weeknrs[$wcount-1]. $catids . $menuitem) ?>" class="event_calendar_month_table_week_nrs">
										<?php echo $weeknrs[$wcount-1]; ?>
									</a>	
								<?php } else {
									echo $weeknrs[$wcount-1];
								} ?>
						</td>
						<?php
					}
					for ($wdcount = 1; $wdcount <= 7; $wdcount++) {
						$working_day = $objEventCalendar->calendar[(($wcount * 7) - 7) + ($wdcount - 1)];
						if(isset($working_day)) {
							if (date("m", $working_day) < date("m", $objEventCalendar->date)) { ?>
								<td class="other_month_day" valign="top">
								<span class="<?php echo (date("w", $working_day) == 0)?'event_sunday':''; ?>"><?php echo strftime("%B", $working_day) ?></span>
								<?php
							} elseif (date("m", $working_day) > date("m", $objEventCalendar->date)) { ?>
								<td class="other_month_day" valign="top">
								<span class="<?php echo (date("w", $working_day) == 0)?'event_sunday':''; ?>"><?php echo strftime("%B", $working_day) ?></span>
								<?php
							} else { ?>
								<td class="month_day<?php echo (date("Y-m-d", $working_day) == date("Y-m-d", time()))?'_today':''; ?>" valign="top">
								<span class="<?php echo (date("w", $working_day) == 0)?'event_sunday':''; ?>"> <?php echo strftime("%d", $working_day) ?><hr class="event" /></span>
								<?php
								foreach ($objEventCalendar->events as $day_event) {
									// Calculate recursion
									if  ($objEventCalendar->calcRecursion($working_day, $day_event)) {
										// Check if working_day is an excepted
										$exceptions = $objEventCalendar->calcExceptions( $day_event );
										if (array_search($working_day, $exceptions) === false) { ?>
											<div class="event" style="border-color:<?php echo $day_event->getColor() ?>; font-size: x-small; line-height: 10px;">
												<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem) ?>" title="<?php echo $day_event->title ?>"><?php echo eUTF::utf8_substr(eUTF::utf8_trim($day_event->title), 0, 15).'...'; ?></a>
											</div>
											<?php
										}
									}
								}
							} ?>
						 </td>
					<?php
					}
				}?>
				</tr>
			<?php } ?>
		</table> 
	<?php
	}

	/***********************/
	/*  Display week view  */
	/***********************/
	static public function displayWeekViewHTML() {
		global $mainframe, $objEventCalendar, $Itemid;

		$mainframe->setPageTitle( $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_WEEK." ".$objEventCalendar->lng->FRONT_VIEW );

		// Print components css information and header inclusions
		$linktag = '<link href="'.$objEventCalendar->live_path.'/templates/'.$objEventCalendar->params['com']->get('css_filename', 'default.css').'" rel="stylesheet" type="text/css" />';
		if (mosGetParam( $_REQUEST, 'pop', 0)) {
			echo $linktag;
		}
		else {
			$mainframe->addCustomHeadTag($linktag);
		}

		// Load core HTML library
		mosCommonHTML::loadOverlib();

		// Set some variables
		$menuitem = "&Itemid=".$Itemid;
		if (isset($objEventCalendar->catid)) {
			$catids = (is_array($objEventCalendar->catid))?implode("+", $objEventCalendar->catid):$objEventCalendar->catid;
		}
		$catids = ($catids == "")?"":"&catid=".$catids;
		
		// Draw a simple html header
		clsEventCalendarHTML::displayHeaderHTML($menuitem, $catids);
		
		// Display week table
		$week_startingday = $objEventCalendar->params['com']->get( 'week_startingday' , 0);
		$firstDay_stamp = (date("w", $objEventCalendar->date) == $week_startingday)?$objEventCalendar->date:strtotime(('last '.$objEventCalendar->weekdays[$week_startingday]), $objEventCalendar->date);
		$lastDay_stamp = (date("w", $objEventCalendar->date) == ((($week_startingday - 1) < 0)?6:$week_startingday - 1))?$objEventCalendar->date:strtotime(('next '.$objEventCalendar->weekdays[((($week_startingday - 1) < 0)?6:$week_startingday - 1)]), $objEventCalendar->date);
		
		// Catch a small week offset error
		if (($objEventCalendar->week - 1) == 0 ) {
			$prevweek = 52;
			$prevweekyear = $objEventCalendar->year - 1;
		} else {
			$prevweek = $objEventCalendar->week - 1;
			$prevweekyear = $objEventCalendar->year;
		}
		if (($objEventCalendar->week + 1) == 53 ) {
			$nextweek = 1;
			$nextweekyear = $objEventCalendar->year + 1;
		} else {
			$nextweek = $objEventCalendar->week + 1;
			$nextweekyear = $objEventCalendar->year;			
		} ?>
		
		<table class="event_calendar_week_table">
			<tr>
				<td></td>
				<td class="nav_other_week" colspan="2"><a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=weekview&year=".$prevweekyear."&week=".$prevweek. $catids . $menuitem ) ?>" title="<?php echo $objEventCalendar->lng->FRONT_VIEW_WEEK_PREV_TITLE ?>">« <?php echo $objEventCalendar->lng->FRONT_VIEW_WEEK_PREV ?></a></td>		
				<td class="nav_cur_week" colspan="3"><?php echo date("Y/m/d", $firstDay_stamp).' - '.date("Y/m/d", $lastDay_stamp); ?> </td>
				<td class="nav_other_week" colspan="2"><a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=weekview&year=".$nextweekyear."&week=".$nextweek. $catids . $menuitem ) ?>" title="<?php echo $objEventCalendar->lng->FRONT_VIEW_WEEK_NEXT_TITLE ?>"><?php echo $objEventCalendar->lng->FRONT_VIEW_WEEK_NEXT ?> »</a></td>
			</tr>
			<tr>
				<td class="week_nrs"><?php echo (strlen($objEventCalendar->week) == 1)?'0'.$objEventCalendar->week:$objEventCalendar->week; ?></td>
				<?php
				for($dcount = 0; $dcount < 7; $dcount++) {?> 
					<td class="weekdays_name<?php echo (strftime("%w", $objEventCalendar->calendar[$dcount]) == 0)?'_sunday':''; ?>"><?php echo strftime("%A", $objEventCalendar->calendar[$dcount]) ?></td>
				<?php }?>
			</tr>
			<?php
			$start_time = $objEventCalendar->timetable[$objEventCalendar->timetable_selected][0];
			$end_time = $objEventCalendar->timetable[$objEventCalendar->timetable_selected][1];
			$hcount = $end_time - $start_time;
			
			for ($i = 0; $i <= $hcount; $i++) { ?>
				<tr>
					<td class="week_hour"><?php echo ($start_time + $i) . ':00'; ?></td>
					<?php
					$working_day = $firstDay_stamp;
					for ($x = 0; $x <=6; $x++) { ?>
						<td class="week_day<?php echo (date("Y-m-d", $working_day) == date("Y-m-d", time()))?'_today':''; ?>">
						<?php
							foreach ($objEventCalendar->events as $day_event) {
								// Calculate recursion
								if  ($objEventCalendar->calcRecursion($working_day, $day_event)) {
									// Check if working_day is an excepted
									$exceptions = $objEventCalendar->calcExceptions( $day_event);
									if (array_search($working_day, $exceptions) === false) {
										switch ($day_event->recur_type) {
											case 'day':
												// If one day event
												if ( date("Y-m-d", strtotime($day_event->start_date)) == date("Y-m-d", strtotime($day_event->end_date)) ) {
													// Check timetable
													if ( (($i + $start_time) >= date("H", strtotime($day_event->start_date))) && (($i + $start_time) <= date("H", strtotime($day_event->end_date))) ) { ?>
														<div class="event" style="border-color:<?php echo $day_event->getColor() ?>;font-size: x-small; line-height: 10px;">
															<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo eUTF::utf8_substr(eUTF::utf8_trim($day_event->title), 0, 15) . '...'; ?></a>
														</div>
														<?php
													}
												// If multiple days event
												} else {
													// Calculate hour for starting day
													if (date("Y-m-d", strtotime($day_event->start_date)) == date("Y-m-d", $working_day)) {
														if ( ($i + $start_time) >= date("H", strtotime($day_event->start_date)) ) { ?>
															<div class="event" style="border-color:<?php echo $day_event->getColor() ?>;font-size: x-small; line-height: 10px;">
																<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo eUTF::utf8_substr(eUTF::utf8_trim($day_event->title), 0, 15) . '...'; ?></a>
															</div>
															<?php
														}
													// Calculate hour for ending day
													} else if ( date("Y-m-d", strtotime($day_event->end_date)) == date("Y-m-d", $working_day) ) {
														if ( ($i + $start_time) <= date("H", strtotime($day_event->end_date)) ) { ?>
															<div class="event" style="border-color:<?php echo $day_event->getColor() ?>;font-size: x-small; line-height: 10px;">
																<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo eUTF::utf8_substr(eUTF::utf8_trim($day_event->title), 0, 15) . '...'; ?></a>
															</div>
															<?php
														}
													// Fill interval days
													} else { ?>
														<div class="event" style="border-color:<?php echo $day_event->getColor() ?>;font-size: x-small; line-height: 10px;">
															<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo eUTF::utf8_substr(eUTF::utf8_trim($day_event->title), 0, 15) . '...'; ?></a>
														</div>
														<?php											
													}
												}
												break;
											default:
												if ( (($i + $start_time) >= date("H", strtotime($day_event->start_date))) && (($i + $start_time) <= date("H", strtotime($day_event->end_date))) ) { ?>
													<div class="event" style="border-color:<?php echo $day_event->getColor() ?>;font-size: x-small; line-height: 10px;">
														<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo eUTF::utf8_substr(eUTF::utf8_trim($day_event->title), 0, 15) . '...'; ?></a>
													</div>
													<?php
												}
												break;
										} // close switch
									}
								}
							} ?>
						</td>
						<?php
						$working_day = strtotime("+1 day", $working_day);
					} ?>
				</tr>
				<?php
			} ?>
			
		</table>
	<?php
	}

	/**********************/
	/*  Display day view  */
	/**********************/
	static public function displayDayViewHTML() {
		global $mainframe, $objEventCalendar, $Itemid;

		$mainframe->setPageTitle( $objEventCalendar->lng->CP_TAB_GENERAL_DEFAULT_VIEW_DAY." ".$objEventCalendar->lng->FRONT_VIEW );

		// Print components css information and header inclusions
		$linktag = '<link href="'.$objEventCalendar->live_path.'/templates/'.$objEventCalendar->params['com']->get('css_filename', 'default.css').'" rel="stylesheet" type="text/css" />';
		if (mosGetParam( $_REQUEST, 'pop', 0)) {
			echo $linktag;
		} else {
			$mainframe->addCustomHeadTag($linktag);
		}

		// Load core HTML library
		mosCommonHTML::loadOverlib();

		// Set some variables
		$menuitem = "&Itemid=".$Itemid;
		if (isset($objEventCalendar->catid)) {
			$catids = (is_array($objEventCalendar->catid))?implode("+", $objEventCalendar->catid):$objEventCalendar->catid;
		}
		$catids = ($catids == "")?"":"&catid=".$catids;
		
		// Draw a simple html header
		clsEventCalendarHTML::displayHeaderHTML($menuitem, $catids);

		// Display day table
		$week_startingday = $objEventCalendar->params['com']->get( 'week_startingday' , 0);
		$firstDay_stamp = (date("w", $objEventCalendar->date) == $week_startingday)?$objEventCalendar->date:strtotime(('last '.$objEventCalendar->weekdays[$week_startingday]), $objEventCalendar->date);?>		
		
		<table class="event_calendar_day_table">
			<tr>
				<td></td>
				<td class="nav_other_day"><a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=dayview&year=".date("Y", strtotime("-1 day", $objEventCalendar->date))."&month=".date("m", strtotime("-1 day", $objEventCalendar->date))."&day=".date("d", strtotime("-1 day", $objEventCalendar->date)). $catid . $menuitem ) ?>"><?php echo '« ' . ((date("Y-m-d", $objEventCalendar->date) == date("Y-m-d", time()))?$objEventCalendar->lng->FRONT_VIEW_DAY_YESTERDAY:$objEventCalendar->lng->FRONT_VIEW_WEEK_PREV) ?></a></th>
				<td class="nav_cur_day"><?php echo ((date("Y-m-d", $objEventCalendar->date) == date("Y-m-d", time()))?($objEventCalendar->lng->FRONT_VIEW_DAY_TODAY.'<br />'.date("Y/m/d", $objEventCalendar->date)):(date("Y/m/d", $objEventCalendar->date))) ?></th>
				<td class="nav_other_day"><a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=dayview&year=".date("Y", strtotime("+1 day", $objEventCalendar->date))."&month=".date("m", strtotime("+1 day", $objEventCalendar->date))."&day=".date("d", strtotime("+1 day", $objEventCalendar->date)). $catid . $menuitem ) ?>"><?php echo ((date("Y-m-d", $objEventCalendar->date) == date("Y-m-d", time()))?$objEventCalendar->lng->FRONT_VIEW_DAY_TOMMOROW:$objEventCalendar->lng->FRONT_VIEW_WEEK_NEXT) . ' »' ?></a></th>
			</tr>
			<?php
			$start_time = $objEventCalendar->timetable[$objEventCalendar->timetable_selected][0];
			$end_time = $objEventCalendar->timetable[$objEventCalendar->timetable_selected][1];
			$hcount = $end_time - $start_time;
			
			$workingday = strtotime(date("Y-m-d", $objEventCalendar->date));
			for ($i = 0; $i <= $hcount; $i++) { ?>
				<tr>
					<td class="day_hour" width="5%"><?php echo ($start_time + $i) . ':00'; ?></td>
					<td class="day" colspan="3">
						<?php
							foreach ($objEventCalendar->events as $day_event) {
								// Calculate recursion
								if  ($objEventCalendar->calcRecursion($workingday, $day_event)) {
									// Check if working_day is an excepted
									$exceptions = $objEventCalendar->calcExceptions( $day_event);
									if (array_search($workingday, $exceptions) === false) {
										switch ($day_event->recur_type) {
											case 'day':
												// If one-day event
												if ( date("Y-m-d", strtotime($day_event->start_date)) == date("Y-m-d", strtotime($day_event->end_date)) ) {
													// Check timetable
													if ( (($i + $start_time) >= date("H", strtotime($day_event->start_date))) && (($i + $start_time) <= date("H", strtotime($day_event->end_date))) ) { ?>
														<div class="event" style="border-color:<?php echo $day_event->getColor() ?>;font-size: x-small; line-height: 10px;">
															<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo $day_event->title; ?></a>
														</div>
														<?php
													}
												// If multiple days event
												} else {
													// Calculate hour for starting day
													if (date("Y-m-d", strtotime($day_event->start_date)) == date("Y-m-d", $workingday)) {
														if ( ($i + $start_time) >= date("H", strtotime($day_event->start_date)) ) { ?>
															<div class="event" style="border-color:<?php echo $day_event->getColor() ?>;font-size: x-small; line-height: 10px;">
																<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo $day_event->title; ?></a>
															</div>
															<?php
														}
													// Calculate hour for ending day
													} else if ( date("Y-m-d", strtotime($day_event->end_date)) == date("Y-m-d", $workingday) ) {
														if ( ($i + $start_time) <= date("H", strtotime($day_event->end_date)) ) { ?>
															<div class="event" style="border-color:<?php echo $day_event->getColor(); ?>;font-size: x-small; line-height: 10px;">
																<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo $day_event->title; ?></a>
															</div>
															<?php
														}
													// Fill interval days
													} else { ?>
														<div class="event" style="border-color:<?php echo $day_event->getColor(); ?>;font-size: x-small; line-height: 10px;">
															<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo $day_event->title; ?></a>
														</div>
														<?php											
													}
												}
												break;
											default:
												if ( (($i + $start_time) >= date("H", strtotime($day_event->start_date))) && (($i + $start_time) <= date("H", strtotime($day_event->end_date))) ) { ?>
													<div class="event" style="border-color:<?php echo $day_event->getColor(); ?>;font-size: x-small; line-height: 10px;">
														<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $day_event->id . $menuitem ) ?>" title="<?php echo $day_event->title ?>"><?php echo $day_event->title; ?></a>
													</div>
													<?php
												}
												break;
										} // close switch
									}
								}
							}	
						?>
					</td>
				</tr>
				<?php
			} ?>
		</table>
		<?php
	}
	
	/*******************************/
	/*  Display the category list  */
	/*******************************/ 
	static public function displayEventViewHTML( $event, $category ) {
		global $objEventCalendar, $mainframe, $Itemid, $my, $database, $lang;

		$mainframe->setPageTitle( $event->title );
	
		// Print components css information and header inclusions
		$linktag = '<link href="'.$objEventCalendar->live_path.'/templates/'.$objEventCalendar->params['com']->get('css_filename', 'default.css').'" rel="stylesheet" type="text/css" />';
		if (mosGetParam( $_REQUEST, 'pop', 0)) {
			echo $linktag;
		}
		else {
			$mainframe->addCustomHeadTag($linktag);
		}

		// Load core HTML library
		mosCommonHTML::loadOverlib();

		// Set some variables
		$menuitem = "&Itemid=".$Itemid;
		if (isset($objEventCalendar->catid)) {
			$catids = (is_array($objEventCalendar->catid))?implode("+", $objEventCalendar->catid):$objEventCalendar->catid;
		}
		$catids = ($catids == "")?"":"&catid=".$catids;
		
		// Draw a simple html header
		clsEventCalendarHTML::displayHeaderHTML($menuitem, $catids);

		// Display event table
		?>
		<script type="text/javascript" src="<?php echo $objEventCalendar->live_apath; ?>/eventcalendar.ajax.js"></script>
		
		<table class="event_calendar_event_table">
			<tr>
				<td class="title" style="border-color:<?php echo $objEventCalendar->getParam('color', $category->params); ?>">
					<h3><?php echo $event->title ?> <i><small>(<?php echo $category->name ?>)</small></i></h3>
				</td>
			</tr><tr>
				<td style="height: 10px;"></td>
			</tr><tr>
				<td>
					<?php if ($event->contact) { ?>
						<b><?php echo $objEventCalendar->lng->EDT_INFO_PERSON ?>:</b> <?php echo $event->contact ?>
					<?php } ?>
				</td>
			</tr><tr>
				<td style="height: 10px;"></td>
			</tr><tr>
				<?php if ($event->description) { ?>
					<td class="description"><?php echo $event->description ?></td>
				<?php } ?>
			</tr><tr>
				<td style="height: 10px;"></td>
			</tr><tr>
				<td>
					<b><?php echo $objEventCalendar->lng->EDT_START ?>:</b> <?php echo date($objEventCalendar->params['com']->get('date_format', 'd-m-Y'), strtotime($event->start_date)) ?> και ώρα <?php echo date("H:i:s", strtotime($event->start_date)) ?>
					<br/>
					<b><?php echo $objEventCalendar->lng->EDT_END ?>:</b> <?php echo date($objEventCalendar->params['com']->get('date_format', 'd-m-Y'), strtotime($event->end_date)) ?> και ώρα <?php echo date("H:i:s", strtotime($event->end_date)) ?>
				</td>
			</tr><tr>
				<td style="height: 10px;"></td>
			</tr>
			<?php if ($objEventCalendar->params['com']->get('view_periodicity')) { ?>
				<tr>
					<td class="recursion">
						<b><?php echo $objEventCalendar->lng->EDT_REPEAT_TYPE ?>: </b>
						<?php
							switch ($event->recur_type) {
								case "week":
									echo $objEventCalendar->lng->LST_REC_WEEK." ";
									echo (strrpos($event->recur_week,"1") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_MON;
									echo (strrpos($event->recur_week,"2") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_TUE;
									echo (strrpos($event->recur_week,"3") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_WED;
									echo (strrpos($event->recur_week,"4") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_THU;
									echo (strrpos($event->recur_week,"5") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_FRI;
									echo (strrpos($event->recur_week,"6") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SUN;
									echo (strrpos($event->recur_week,"0") === false)?"":$objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SAT;
									break;
								case "month":
									echo $objEventCalendar->lng->LST_REC_MONTH." ";
									if ($event->recur_month == 1) {
										echo $event->recur_month.$objEventCalendar->lng->LST_REC_MONTH_SUFFIX_1;
									} else if ($event->recur_month == 2) {
										echo $event->recur_month.$objEventCalendar->lng->LST_REC_MONTH_SUFFIX_2;
									} else {
										echo $event->recur_month.$objEventCalendar->lng->LST_REC_MONTH_SUFFIX_ALL;
									}
									break;
								case "year":
									echo $objEventCalendar->lng->LST_REC_YEAR." ".$event->recur_year_d."/".$event->recur_year_m;
									break;
								case "day":
								default:
									echo $objEventCalendar->lng->LST_REC_DAY;
									break;
							}
						?>
					</td>
				</tr>
			<?php }
			
			if ($objEventCalendar->params['com']->get('view_reservation')) { ?>
				<tr><td style="height: 10px;"></td></tr>
				<tr>		
					<td>
						<b><?php echo $objEventCalendar->lng->EDT_RSV ?>: </b>
						<div class="reserve" id="reserve" name="reserve">
							<?php 
							
							if (intval($event->pp_price) == 0) {
								if ($event->rsv) { 
									$query = "SELECT name FROM #__users WHERE id IN (".$event->rsv.")";
									$database->setQuery( $query );
									$users = $database->loadResultArray();
								
									echo '<i>'.$users[0].'</i>';
									for ($i = 1; $i < count($users); $i++) {
										echo ', '.'<i>'.$users[$i].'</i>';
									}
								} else {
									echo '<i>'.$objEventCalendar->lng->EDT_RSV_NONE.'</i>';
								}
								if ($my->id <> 0) {
									if ( in_array($my->id, explode(',', $event->rsv)) ) {
										echo "<br/><br/>".$objEventCalendar->lng->EDT_RSV_YES."<br/>"; ?>
										<input class="button" type="button" value="<?php echo $objEventCalendar->lng->EDT_RSV_CAN; ?>" id="cancel-rsv" name="cancel-rsv" onclick="showReserve(<?php echo $my->id; ?>, <?php echo $event->id; ?>, 'cancel');" />
									<?php } else {
										echo "<br/><br/>".$objEventCalendar->lng->EDT_RSV_NO."<br/>"; ?>
										<input class="button" type="button" value="<?php echo $objEventCalendar->lng->EDT_RSV_SUB; ?>" id="submit-rsv" name="submit-rsv" onclick="showReserve(<?php echo $my->id; ?>, <?php echo $event->id; ?>, 'reserve');" />
									<?php }
								}
							} else {
								$r = rand(100, 999); ?>
								<form class="ppBuyNowForm" name="paypalsubmit<?php echo $r; ?>" action="https://www.paypal.com/cgi-bin/webscr" method="post">
									<img class="ppBuyNowImg" src="<?php echo $objEventCalendar->live_path; ?>/templates/f_<?php echo $objEventCalendar->params['com']->get('css_filename'); ?>/default.gif" border="0" align="left" alt="<?php echo $event->title; ?>" style="margin: 4px;" />
									<?php echo $objEventCalendar->lng->EDT_PP_PRICE; ?>: <span class="ppBuyNowPrice"><?php echo $event->pp_price." ".$objEventCalendar->params['com']->get('pp_curlist', 'EUR'); ?></span><br />
									<?php if (file_exists( $objEventCalendar->abs_path."/templates/f_".$objEventCalendar->params['com']->get('css_filename')."/".$lang.".gif" )) {
										$butlang = $lang;
									} else {
										$butlang = 'english';
									} ?>
									<input type="hidden" name="charset" value="utf-8" />
									<input type="hidden" name="cmd" value="_xclick" />
										<?php $parts = split('@', $objEventCalendar->params['com']->get('pp_mail', 'mypaypal@email.com')); ?>
										<script type='text/javascript'>
											<!--
											document.write('<input type="hidden" name="business" value="');
											document.write('<?php echo $parts[0]; ?>');
											document.write('&#64;');
											document.write('<?php echo $parts[1]; ?>');
											document.write('" />');
											//-->
										</script> 
									<input type="hidden" name="item_name" value="<?php echo $event->title; ?>" />
									<input type="hidden" name="amount" value="<?php echo $event->pp_price; ?>" />
									<input type="hidden" name="no_shipping" value="2" />
									<input type="hidden" name="no_note" value="1" />
									<input type="hidden" name="currency_code" value="<?php echo $objEventCalendar->params['com']->get('pp_curlist', 'EUR'); ?>" />
									<input type="hidden" name="tax" value="0" />
									<input type="hidden" name="quantity" value="1" />
									<input type="image" style="margin-top: 10px;" src="<?php echo $objEventCalendar->live_path; ?>/templates/f_<?php echo $objEventCalendar->params['com']->get('css_filename').'/'.$butlang.'.gif'?>" name="submit" title="<?php echo $objEventCalendar->lng->EDT_PP_BUY; ?>" alt="<?php echo $objEventCalendar->lng->EDT_PP_BUY; ?>" />
								</form>
							<?php } ?>
						</div>
					</td>
				</tr>
			<?php } ?>
			
			<tr><td style="height: 10px;"></td></tr>
			<tr>
				<td class="contact" style="border-color: <?php echo $objEventCalendar->getParam('color', $category->params); ?>">
					<?php if ($event->url) {
						if ($objEventCalendar->task != 'printevent') { ?>
							<a href="<?php echo $event->url ?>" target="_blank"><?php echo $objEventCalendar->lng->EDT_INFO_WEB ?></a>
						<?php } else {
							echo '<b>' . $objEventCalendar->lng->EDT_INFO_WEB . ': </b>' . $event->url;
						}
					}
					echo ($objEventCalendar->task != 'printevent')?'&nbsp;':'<br />';
					if ($event->email) {
						$email = $event->email;
						$parts = split('@', $email);
						if ($objEventCalendar->task != 'printevent') { ?>
							<script type="text/javascript">
								<!--
								document.write('<a href="ma');
								document.write('ilto:');
								document.write('<?php echo $parts[0]; ?>');
								document.write('&#64;');
								document.write('<?php echo $parts[1]; ?>');
								document.write('"><?php echo $objEventCalendar->lng->EDT_INFO_MAIL ?>');
								document.write('</a>');
								//-->
							</script>
						<?php } else { ?>
							<script type="text/javascript">
								<!--
								document.write('<b><?php echo $objEventCalendar->lng->EDT_INFO_MAIL ?>: </b>');
								document.write('<?php echo $parts[0]; ?>');
								document.write('&#64;');
								document.write('<?php echo $parts[1]; ?>');
								//-->
							</script>
						<?php } ?>
					<?php } ?>
				</td>
			</tr>
		</table>
		
		<?php
	}
	
	/*******************************/
	/*  Display the category list  */
	/*******************************/ 
	static public function displayCategoriesListHTML( $categories ) {
		global $objEventCalendar, $mainframe, $Itemid;

		$mainframe->setPageTitle( $objEventCalendar->lng->FRONT_VIEW_CAT_TITLE );

		// Print components css information and header inclusions
		$linktag = '<link href="'.$objEventCalendar->live_path.'/templates/'.$objEventCalendar->params['com']->get('css_filename', 'default.css').'" rel="stylesheet" type="text/css" />';
		if (mosGetParam( $_REQUEST, 'pop', 0)) {
			echo $linktag;
		}
		else {
			$mainframe->addCustomHeadTag($linktag);
		}

		// Load core HTML library
		mosCommonHTML::loadOverlib();

		// Set some variables
		$menuitem = "&Itemid=".$Itemid;
		
		// Draw a simple html header
		clsEventCalendarHTML::displayHeaderHTML($menuitem);

		// Display category table
		?>
		<table class="event_calendar_categories_table">
			<tr><td colspan="2"><h3><?php echo $objEventCalendar->lng->FRONT_VIEW_CAT_TITLE; ?>:</h3></td></tr> <?php
			foreach ($categories as $category) { ?>
				<tr><td colspan="2">
					<div class="categories" style="border-color:<?php echo $objEventCalendar->getParam('color', $category->params); ?>">
						<b><?php echo $category->title; ?></b>
					</div>
					<hr class="categories" style="border-color:<?php echo $objEventCalendar->getParam('color', $category->params); ?>;"/>
				</td></tr>
				<tr><td colspan="2" height="10px"></td></tr>
				<?php if ($category->description) { ?> 
					<tr><td colspan="2" class="description"><?php echo $category->description; ?></td></tr>
					<tr><td colspan="2" height="10px"></td></tr>
				 <?php } ?>
				<tr>
					<td class="events" width="100%"> <?php
						foreach ($objEventCalendar->events as $cat_event) {
							if ($cat_event->catid == $category->id) { ?>
								<li><a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=eventview&eventid=" . $cat_event->id . $menuitem) ?>"><?php echo date("Y-m-d", strtotime($cat_event->start_date)) . ': ' . $cat_event->title; ?></a></li> <?php
							}
						} ?>
					</td>
				</tr>
				<tr><td height="20px"></td></tr> <?php
			} ?>
		</table> <?php
	}
	
	
	/*********************************/
	/*  Display the edit event form  */
	/*********************************/ 
	static public function displayEventEditHTML($event) {
		global $my, $mainframe, $objEventCalendar, $Itemid;

		$mainframe->setPageTitle( $objEventCalendar->lng->EDT_SUBMIT );

		//CSRF prevention
        $tokname = 'token'.$my->id;
		$mytoken = md5(uniqid(rand(), TRUE));
        $_SESSION[$tokname] = $mytoken;
		
		// Print components css information and header inclusions
		$linktag = '<link href="'.$objEventCalendar->live_path.'/templates/'.$objEventCalendar->params['com']->get('css_filename', 'default.css').'" rel="stylesheet" type="text/css" />';
		if (mosGetParam( $_REQUEST, 'pop', 0)) {
			echo $linktag;
		} else {
			$mainframe->addCustomHeadTag($linktag);
		}
		
		// Load core HTML library
		mosCommonHTML::loadOverlib();
		mosCommonHTML::loadCalendar();

		// Set some variables
		$menuitem = "&Itemid=".$Itemid;

		// Display edit/add event table
		$seoCancel = sefRelToAbs("index.php?option=com_eventcalendar&task=cancel".$menuitem, EVCALBASE."/editevent/cancel.html");
		$seoAction = sefRelToAbs("index.php?option=com_eventcalendar&task=save".$menuitem, EVCALBASE."/editevent/save.html");
		?>
		<script type="text/javascript">
			function submitbutton(pressbutton) {
				var form = document.adminForm;
				if (pressbutton == 'cancel') {
					document.location.href = '<?php echo $seoCancel; ?>';
					return;
				}

				if (form.title.value == "") {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_TITLE ?>');
				} else if ( form.catid.value == "0" ) {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_CATEGORY ?>');
				} else if ( form.start_date.value == "" ) {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_START_DATE ?>');
				} else if ( form.end_date.value == "" ) {
					alert('<?php echo $objEventCalendar->lng->ALERT_EDIT_NO_END_DATE ?>');
				} else {
					submitform( pressbutton );
				}
			}

			//JavaScript functions for recur-exception-adding/-removing:
			//adds an entry to the except-dates-list from the edit-form-field
			function addToList() {
				var dates = document.adminForm.dateexcept.value;
				var neu = new Option (dates, dates, false, false);
				document.adminForm.daten.options[document.adminForm.daten.options.length] = neu;
				document.adminForm.dateexcept.value = '';
				document.adminForm.dateexcept.focus();
				writeHiddenEntry();
			}

			//removes an entry from the excepts-list
			function remList() {
				document.adminForm.daten.options[document.adminForm.daten.selectedIndex] = null;
				writeHiddenEntry();
			}

			//writes hidden entries for the except-dates | here the dates are stored in raw / as timestamp
			function writeHiddenEntry() {
				document.adminForm.recur_except.value = '';
				var i = 0;
				for(i=0;i<document.adminForm.daten.length;i++) {
					var nextEntry = document.adminForm.daten.options[i].value;
					document.adminForm.recur_except.value = document.adminForm.recur_except.value + ',' + nextEntry;
				}
			}
			
			//show or hide recursion options
			function recurChange(index) {
				trWeek = document.getElementById('weekly');
				trMonth = document.getElementById('monthly');
				trYear = document.getElementById('yearly');
				
				if (index == 0) {
					trWeek.style.visibility = 'collapse';
					trMonth.style.visibility = 'collapse';
					trYear.style.visibility = 'collapse';
				} else if (index == 1) {
					trWeek.style.visibility = 'visible';
					trMonth.style.visibility = 'collapse';
					trYear.style.visibility = 'collapse';
				} else if (index == 2) {
					trWeek.style.visibility = 'collapse';
					trMonth.style.visibility = 'visible';
					trYear.style.visibility = 'collapse';
				} else if (index == 3) {
					trWeek.style.visibility = 'collapse';
					trMonth.style.visibility = 'collapse';
					trYear.style.visibility = 'visible';
				}
			}
		</script>
		
		<h1 class="contentheading"><?php echo $objEventCalendar->lng->EDT_SUBMIT; ?>: <?php echo ($event->id != '')?_E_EDIT:_E_ADD; ?></h1>
		
		<form action="<?php echo $seoAction ?> "method="post" name="adminForm">

		<table width="100%">
			<tr>
				<td><?php echo _E_TITLE; ?>:</td>
				<td colspan="3">
					<input class="inputbox" type="text" name="title" size="50" maxlength="100" value="<?php echo (isset($event))?$event->title:'' ?>" />
				</td>
			</tr>
			<tr>
				<td><?php echo _E_CATEGORY; ?>:</td>
				<td colspan="3">
					<?php $active = (isset($event))?$event->catid:NULL;
					echo mosAdminMenus::ComponentCategory('catid','com_eventcalendar', $active); ?>
				</td>
			</tr>
			<tr>
				<td><?php echo $objEventCalendar->lng->EDT_START; ?>:</td>
				<td colspan="3">
					<input type="text" name="start_date" id="start_date" size="20" class="inputbox" value="<?php echo (isset($event))?$event->start_date:'' ?>" />
					&nbsp;<input type="button" value=" ... " class="button" onclick="return showCalendar('start_date')" />
				</td>
			</tr>
			<tr>
				<td><?php echo $objEventCalendar->lng->EDT_END; ?>:</td>
				<td colspan="3">
					<input type="text" name="end_date" id="end_date" size="20" class="inputbox" value="<?php echo (isset($event))?$event->end_date:'' ?>" />
					&nbsp;<input type="button" value=" ... " class="button" onclick="return showCalendar('end_date')" />
				</td>
			</tr>
			<tr>
				<td valign="top"><?php echo _E_LANGUAGE ?>:</td>
				<td colspan="3">
					<?php echo mosAdminMenus::SelectLanguages( 'languages', (isset($event)?$event->language:''), _E_ALL_LANGUAGES ); ?>
				</td>
				
			</tr>
			<tr>
				<td valign="top"><?php echo _CMN_DESCRIPTION; ?>:</td>
				<td colspan="3">
					<?php
						//parameters : areaname, content, hidden field, width, height, rows, cols
						editorArea( 'editor1',  ((isset($event))?$event->description:'') , 'description', '450', '300', '60', '20' );
					?>
				</td>
			</tr>					
		</table>
		<h3 class="contentheading"><?php echo$objEventCalendar->lng->EDT_DETAILS ?>:</h2>
		<?php
		$tabulator = new mosTabs( 0 ); 
		$tabulator->startPane( "Edit" );
		$tabulator->startTab( $objEventCalendar->lng->EDT_REPEAT_OPT, "repeat" );
		?>
			<table>
				<tr>
					<td><?php echo $objEventCalendar->lng->EDT_REPEAT_TYPE ?>:</td>
					<td colspan="3">
						<select name="recur_type" size="1" class="selectbox" onchange="recurChange(this.selectedIndex)">
							<option value="day" <?php echo (isset($event) && $event->recur_type == 'day' || !isset($event))?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->LST_REC_DAY ?></option>
							<option value="week" <?php echo (isset($event) && $event->recur_type == 'week')?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->LST_REC_WEEK ?></option>
							<option value="month" <?php echo (isset($event) && $event->recur_type == 'month')?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->LST_REC_MONTH ?></option>
							<option value="year" <?php echo (isset($event) && $event->recur_type == 'year')?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->LST_REC_YEAR ?></option>
						</select>
					</td>
				</tr>
				<tr name="weekly" id="weekly" style="visibility: <?php echo ($event->recur_type == 'week')?'visible':'collapse'; ?>">
					<td valign="top"><?php echo $objEventCalendar->lng->EDT_WEEKLY_OPT ?>:</td>
					<td colspan="3">
						<select name="recur_week" size="7" multiple="multiple">
							<option value="1" <?php echo (isset($event) && strrpos($event->recur_week,'1') !== false || !isset($event))?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_MON ?></option>
							<option value="2" <?php echo (isset($event) && strrpos($event->recur_week,'2') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_TUE ?></option>
							<option value="3" <?php echo (isset($event) && strrpos($event->recur_week,'3') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_WED ?></option>
							<option value="4" <?php echo (isset($event) && strrpos($event->recur_week,'4') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_THU ?></option>
							<option value="5" <?php echo (isset($event) && strrpos($event->recur_week,'5') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_FRI ?></option>
							<option value="6" <?php echo (isset($event) && strrpos($event->recur_week,'6') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SUN ?></option>
							<option value="0" <?php echo (isset($event) && strrpos($event->recur_week,'0') !== false)?'selected="selected"':"" ?>><?php echo $objEventCalendar->lng->CP_TAB_DATEFORMAT_START_SAT ?></option>
						</select>
					</td>
				</tr>
				<tr name="monthly" id="monthly" style="visibility: <?php echo ($event->recur_type == 'month')?'visible':'collapse'; ?>">
					<td><?php echo $objEventCalendar->lng->EDT_MONTHLY_OPT ?>:</td>
					<td colspan="3">
						<select name="recur_month" size="1" class="selectbox">
							<option value="1" <?php echo (isset($event) && strrpos($event->recur_month,'1') !== false || !isset($event))?'selected="selected"':"" ?>>1</option>
							<option value="2" <?php echo (isset($event) && strrpos($event->recur_month,'2') !== false)?'selected="selected"':"" ?>>2</option>
							<option value="3" <?php echo (isset($event) && strrpos($event->recur_month,'3') !== false)?'selected="selected"':"" ?>>3</option>
							<option value="4" <?php echo (isset($event) && strrpos($event->recur_month,'4') !== false)?'selected="selected"':"" ?>>4</option>
							<option value="5" <?php echo (isset($event) && strrpos($event->recur_month,'5') !== false)?'selected="selected"':"" ?>>5</option>
							<option value="6" <?php echo (isset($event) && strrpos($event->recur_month,'6') !== false)?'selected="selected"':"" ?>>6</option>
							<option value="7" <?php echo (isset($event) && strrpos($event->recur_month,'7') !== false)?'selected="selected"':"" ?>>7</option>
							<option value="8" <?php echo (isset($event) && strrpos($event->recur_month,'8') !== false)?'selected="selected"':"" ?>>8</option>
							<option value="9" <?php echo (isset($event) && strrpos($event->recur_month,'9') !== false)?'selected="selected"':"" ?>>9</option>
							<option value="10" <?php echo (isset($event) && strrpos($event->recur_month,'10') !== false)?'selected="selected"':"" ?>>11</option>
							<option value="11" <?php echo (isset($event) && strrpos($event->recur_month,'11') !== false)?'selected="selected"':"" ?>>12</option>
							<option value="12" <?php echo (isset($event) && strrpos($event->recur_month,'12') !== false)?'selected="selected"':"" ?>>12</option>
							<option value="13" <?php echo (isset($event) && strrpos($event->recur_month,'13') !== false)?'selected="selected"':"" ?>>13</option>
							<option value="14" <?php echo (isset($event) && strrpos($event->recur_month,'14') !== false)?'selected="selected"':"" ?>>14</option>
							<option value="15" <?php echo (isset($event) && strrpos($event->recur_month,'15') !== false)?'selected="selected"':"" ?>>15</option>
							<option value="16" <?php echo (isset($event) && strrpos($event->recur_month,'16') !== false)?'selected="selected"':"" ?>>16</option>
							<option value="17" <?php echo (isset($event) && strrpos($event->recur_month,'17') !== false)?'selected="selected"':"" ?>>17</option>
							<option value="18" <?php echo (isset($event) && strrpos($event->recur_month,'18') !== false)?'selected="selected"':"" ?>>18</option>
							<option value="19" <?php echo (isset($event) && strrpos($event->recur_month,'19') !== false)?'selected="selected"':"" ?>>19</option>
							<option value="20" <?php echo (isset($event) && strrpos($event->recur_month,'20') !== false)?'selected="selected"':"" ?>>20</option>
							<option value="21" <?php echo (isset($event) && strrpos($event->recur_month,'21') !== false)?'selected="selected"':"" ?>>21</option>
							<option value="22" <?php echo (isset($event) && strrpos($event->recur_month,'22') !== false)?'selected="selected"':"" ?>>22</option>
							<option value="23" <?php echo (isset($event) && strrpos($event->recur_month,'23') !== false)?'selected="selected"':"" ?>>23</option>
							<option value="24" <?php echo (isset($event) && strrpos($event->recur_month,'24') !== false)?'selected="selected"':"" ?>>24</option>
							<option value="25" <?php echo (isset($event) && strrpos($event->recur_month,'25') !== false)?'selected="selected"':"" ?>>25</option>
							<option value="26" <?php echo (isset($event) && strrpos($event->recur_month,'26') !== false)?'selected="selected"':"" ?>>26</option>
							<option value="27" <?php echo (isset($event) && strrpos($event->recur_month,'27') !== false)?'selected="selected"':"" ?>>27</option>
							<option value="28" <?php echo (isset($event) && strrpos($event->recur_month,'28') !== false)?'selected="selected"':"" ?>>28</option>
							<option value="29" <?php echo (isset($event) && strrpos($event->recur_month,'29') !== false)?'selected="selected"':"" ?>>29</option>
							<option value="30" <?php echo (isset($event) && strrpos($event->recur_month,'30') !== false)?'selected="selected"':"" ?>>30</option>
							<option value="31" <?php echo (isset($event) && strrpos($event->recur_month,'31') !== false)?'selected="selected"':"" ?>>31</option>
						</select>
					</td>
				</tr>
				<tr name="yearly" id="yearly" style="visibility: <?php echo ($event->recur_type == 'year')?'visible':'collapse'; ?>">
					<td><?php echo $objEventCalendar->lng->EDT_YEARLY_OPT ?>:</td>
					<td colspan="3">
						<select name="recur_year_d" size="1" class="selectbox">
							<option value="1" <?php echo (isset($event) && strrpos($event->recur_year_d,'1') !== false || !isset($event))?'selected="selected"':"" ?>>1</option>
							<option value="2" <?php echo (isset($event) && strrpos($event->recur_year_d,'2') !== false)?'selected="selected"':"" ?>>2</option>
							<option value="3" <?php echo (isset($event) && strrpos($event->recur_year_d,'3') !== false)?'selected="selected"':"" ?>>3</option>
							<option value="4" <?php echo (isset($event) && strrpos($event->recur_year_d,'4') !== false)?'selected="selected"':"" ?>>4</option>
							<option value="5" <?php echo (isset($event) && strrpos($event->recur_year_d,'5') !== false)?'selected="selected"':"" ?>>5</option>
							<option value="6" <?php echo (isset($event) && strrpos($event->recur_year_d,'6') !== false)?'selected="selected"':"" ?>>6</option>
							<option value="7" <?php echo (isset($event) && strrpos($event->recur_year_d,'7') !== false)?'selected="selected"':"" ?>>7</option>
							<option value="8" <?php echo (isset($event) && strrpos($event->recur_year_d,'8') !== false)?'selected="selected"':"" ?>>8</option>
							<option value="9" <?php echo (isset($event) && strrpos($event->recur_year_d,'9') !== false)?'selected="selected"':"" ?>>9</option>
							<option value="10" <?php echo (isset($event) && strrpos($event->recur_year_d,'10') !== false)?'selected="selected"':"" ?>>11</option>
							<option value="11" <?php echo (isset($event) && strrpos($event->recur_year_d,'11') !== false)?'selected="selected"':"" ?>>12</option>
							<option value="12" <?php echo (isset($event) && strrpos($event->recur_year_d,'12') !== false)?'selected="selected"':"" ?>>12</option>
							<option value="13" <?php echo (isset($event) && strrpos($event->recur_year_d,'13') !== false)?'selected="selected"':"" ?>>13</option>
							<option value="14" <?php echo (isset($event) && strrpos($event->recur_year_d,'14') !== false)?'selected="selected"':"" ?>>14</option>
							<option value="15" <?php echo (isset($event) && strrpos($event->recur_year_d,'15') !== false)?'selected="selected"':"" ?>>15</option>
							<option value="16" <?php echo (isset($event) && strrpos($event->recur_year_d,'16') !== false)?'selected="selected"':"" ?>>16</option>
							<option value="17" <?php echo (isset($event) && strrpos($event->recur_year_d,'17') !== false)?'selected="selected"':"" ?>>17</option>
							<option value="18" <?php echo (isset($event) && strrpos($event->recur_year_d,'18') !== false)?'selected="selected"':"" ?>>18</option>
							<option value="19" <?php echo (isset($event) && strrpos($event->recur_year_d,'19') !== false)?'selected="selected"':"" ?>>19</option>
							<option value="20" <?php echo (isset($event) && strrpos($event->recur_year_d,'20') !== false)?'selected="selected"':"" ?>>20</option>
							<option value="21" <?php echo (isset($event) && strrpos($event->recur_year_d,'21') !== false)?'selected="selected"':"" ?>>21</option>
							<option value="22" <?php echo (isset($event) && strrpos($event->recur_year_d,'22') !== false)?'selected="selected"':"" ?>>22</option>
							<option value="23" <?php echo (isset($event) && strrpos($event->recur_year_d,'23') !== false)?'selected="selected"':"" ?>>23</option>
							<option value="24" <?php echo (isset($event) && strrpos($event->recur_year_d,'24') !== false)?'selected="selected"':"" ?>>24</option>
							<option value="25" <?php echo (isset($event) && strrpos($event->recur_year_d,'25') !== false)?'selected="selected"':"" ?>>25</option>
							<option value="26" <?php echo (isset($event) && strrpos($event->recur_year_d,'26') !== false)?'selected="selected"':"" ?>>26</option>
							<option value="27" <?php echo (isset($event) && strrpos($event->recur_year_d,'27') !== false)?'selected="selected"':"" ?>>27</option>
							<option value="28" <?php echo (isset($event) && strrpos($event->recur_year_d,'28') !== false)?'selected="selected"':"" ?>>28</option>
							<option value="29" <?php echo (isset($event) && strrpos($event->recur_year_d,'29') !== false)?'selected="selected"':"" ?>>29</option>
							<option value="30" <?php echo (isset($event) && strrpos($event->recur_year_d,'30') !== false)?'selected="selected"':"" ?>>30</option>
							<option value="31" <?php echo (isset($event) && strrpos($event->recur_year_d,'31') !== false)?'selected="selected"':"" ?>>31</option>
						</select>
						/
						<select name="recur_year_m" size="1" class="selectbox">
							<option value="1" <?php echo (isset($event) && strrpos($event->recur_year_m,'1') !== false)?'selected="selected"':"" ?>>1</option>
							<option value="2" <?php echo (isset($event) && strrpos($event->recur_year_m,'2') !== false)?'selected="selected"':"" ?>>2</option>
							<option value="3" <?php echo (isset($event) && strrpos($event->recur_year_m,'3') !== false)?'selected="selected"':"" ?>>3</option>
							<option value="4" <?php echo (isset($event) && strrpos($event->recur_year_m,'4') !== false)?'selected="selected"':"" ?>>4</option>
							<option value="5" <?php echo (isset($event) && strrpos($event->recur_year_m,'5') !== false)?'selected="selected"':"" ?>>5</option>
							<option value="6" <?php echo (isset($event) && strrpos($event->recur_year_m,'6') !== false)?'selected="selected"':"" ?>>6</option>
							<option value="7" <?php echo (isset($event) && strrpos($event->recur_year_m,'7') !== false)?'selected="selected"':"" ?>>7</option>
							<option value="8" <?php echo (isset($event) && strrpos($event->recur_year_m,'8') !== false)?'selected="selected"':"" ?>>8</option>
							<option value="9" <?php echo (isset($event) && strrpos($event->recur_year_m,'9') !== false)?'selected="selected"':"" ?>>9</option>
							<option value="10" <?php echo (isset($event) && strrpos($event->recur_year_m,'10') !== false)?'selected="selected"':"" ?>>11</option>
							<option value="11" <?php echo (isset($event) && strrpos($event->recur_year_m,'11') !== false)?'selected="selected"':"" ?>>12</option>
							<option value="12" <?php echo (isset($event) && strrpos($event->recur_year_m,'12') !== false)?'selected="selected"':"" ?>>12</option>
						</select>
					</td>
				</tr>
			</table>
		<?php
		$tabulator->endTab();
		$tabulator->startTab( $objEventCalendar->lng->EDT_EXCEPT, "except" );
		?>
			<table>
				<tr>
					<td><?php echo $objEventCalendar->lng->EDT_EXCEPT_ADD ?>:</td>
					<td colspan="3">
						<input style="margin-top:6px;" type="text" size="10" name="dateexcept" id="exceptions" class="inputbox"/>
						&nbsp;<input type="button" value="  ... " onclick="return showCalendar('exceptions','dd.mm.yyyy')" class="button" />
						&nbsp;&nbsp;<input type="button" value=" + " onclick="addToList()" class="button" /><input type="button" value=" - " onclick="remList()" class="button" />
					</td>
				</tr>
				<tr valign="top">
					<td><?php echo $objEventCalendar->lng->EDT_EXCEPT_LIST ?>:</td>
					<td colspan="2">
						<select size="10" class="selectbox" style="width:173px;" name="daten">
						<?php 
							if (isset($event) && $event->recur_except) {
								$except_dates = split("\n",$event->recur_except);
								$exceptdates = "";
								foreach ($except_dates AS $except) {
									if ($except > 1) {
										echo '<option value="'.$except.'">'.$except.'</option>';
										$exceptdates .= ','.$except;
									}
								}
							}
						?>
						</select> 
						<input type="hidden" name="recur_except" value="<?php echo (isset($event) && $event->recur_except)?$exceptdates:"" ?>" />
					</td>
				</tr>
			</table>
		<?php	
		$tabulator->endTab();
		$tabulator->startTab( $objEventCalendar->lng->EDT_INFO, "info" );
		?>
			<table class="adminform">
				<tr>
					<td><?php echo $objEventCalendar->lng->EDT_INFO_PERSON ?>:</td>
					<td><input class="inputbox" type="text" size="50" name="contact" value="<?php echo (isset($event))?htmlspecialchars( $event->contact, ENT_QUOTES ):'' ?>" /></td>
				</tr>
				<tr>
					<td><?php echo $objEventCalendar->lng->EDT_INFO_WEB ?>:</td>
					<td><input class="inputbox" type="text" size="50" name="url" value="<?php echo (isset($event))?$event->url:'' ?>" /></td>
				</tr>
				<tr>
					<td><?php echo $objEventCalendar->lng->EDT_INFO_MAIL ?>:</td>
					<td><input class="inputbox" type="text" size="50" name="email" value="<?php echo (isset($event))?$event->email:'' ?>" /></td>
				</tr>
			</table>
		<?php	
		$tabulator->endTab();
		$tabulator->startTab( $objEventCalendar->lng->CP_TAB_PP, "paypal" );
		?>
			<table class="adminform">
				<tr>
					<td><?php echo $objEventCalendar->lng->EDT_PP_PRICE ?>:&nbsp;&nbsp;&nbsp;</td>
					<td><input class="inputbox" type="text" size="10" name="pp_price" value="<?php echo (isset($event) AND ($event->pp_price))?htmlspecialchars( $event->pp_price, ENT_QUOTES ):'0.0' ?>" /></td>
					<td><?php echo mosToolTip($objEventCalendar->lng->EDT_PP_PRICE_TOOLTIP, $objEventCalendar->lng->EDT_PP_PRICE);?></td>
				</tr>
			</table>
		<?php	
		$tabulator->endTab();
		$tabulator->endPane();
		?>
		
		
		<input type="hidden" name="option" value="com_eventcalendar" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
		<input type="hidden" name="id" value="<?php echo (isset($event))?$event->id:""; ?>" />
		<input type="hidden" name="<?php echo $tokname; ?>" value="<?php echo $mytoken; ?>" autocomplete="off" /> 
		<br/>
		<p align="center">
			<input type="button" name="submitit" value="<?php echo _E_SAVE; ?>" title="<?php echo _E_SAVE; ?>" class="button" onclick="submitbutton('save');" /> &nbsp; 
			<input type="button" name="subcontent" value="<?php echo _GEM_CANCEL; ?>" title="<?php echo _GEM_CANCEL; ?>" class="button" onclick="submitbutton('cancel');" />
		</p>
			
		</form>
		<div style="clear:both;"></div>
	<?php
	}
	
	/********************/
	/*  Display footer  */
	/********************/
	static public function displayFooterHTML( $categories, $marked = "" ) {
		global $objEventCalendar, $Itemid;

		$menuitem = "&Itemid=".$Itemid;
		if (isset($objEventCalendar->catid)) {
			$catids = (is_array($objEventCalendar->catid))?implode("+", $objEventCalendar->catid):$objEventCalendar->catid;
		}
		$catids = ($catids !== "")?"&catid=".$catids:""; ?>

		<table class="event_calendar_footer_table">
			<?php if ($categories) {
				$cat_col = $objEventCalendar->params['com']->get('col_catlist', 3);
				$cat_total = count($categories); ?>
				<tr><td colspan="<?php echo ($cat_col + 1); ?>" height="5px"></td></tr>
				<tr>
					<td colspan="<?php echo $cat_col; ?>"><b><?php echo $objEventCalendar->lng->FRONT_VIEW_CAT; ?>:</b></td>
					<td class="all_events" colspan="<?php echo ($cat_col + 1); ?>">
						[<a href="<?php echo sefRelToAbs("index.php?option=com_eventcalendar&task=catview". $menuitem); ?>">
							<small><?php echo _SEL_CATEGORY; ?></small>
						</a>]
					</td>
				</tr>
				<tr><td width="5%"></td>
				<?php $k = 0; $y = 1;
				for ($x=1; $x <= $cat_col; $x++) { ?>
					<td width="<?php echo (95 / $cat_col) ?>%"> <?php
					for ($i=$k; $i < (( $cat_total / $cat_col) * $y); $i++) { ?>
						<div class="event" style="border-color:<?php echo $objEventCalendar->getParam('color', $categories[$i]->params) ?>">
							<a href="<?php echo sefRelToAbs( "/index.php?option=com_eventcalendar&task=catview&catid=" . $categories[$i]->id . $menuitem); ?>">
								<?php
								echo ($marked)?((array_search($categories[$i]->id, $marked) === false)?'':'<b>'):'';
									echo $categories[$i]->title;
								echo ($marked)?((array_search($categories[$i]->id, $marked) === false)?'':'</b>'):'';
								?>
							</a>
						</div> <?php
					}
					$k = $i; $y++; ?>
					</td> <?php
				} ?>
				</tr> <?php
			} ?>
		</table> <?php
	}

}?>
