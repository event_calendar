<?php

/*
 * Event Calendar for Elxis CMS 2008.x and 2009.x
 *
 * Installer Handler
 *
 * @version		1.1
 * @package		eventCalendar
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009-2010 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Function automatically triggered by Elxis to fire the uninstall process 
function com_uninstall() {
    global $database, $fmanager, $mainframe;

	// Drop components table
	$tbl = $database->_table_prefix.'eventcalendar';
	if (in_array($database->_resource->databaseType, array('oci8', 'oci805', 'oci8po', 'oracle'))) { $tbl = strtoupper($tpl); }
	$dict = NewDataDictionary($database->_resource);
	$sql = $dict->DropTableSQL($tbl);
	$database->_resource->Execute($sql[0]);

	// Clear components categories
	$database->setQuery("DELETE FROM #__categories WHERE section = 'com_eventcalendar'");
	$database->query();

    //Remove the SEO PRO extension from the includes/seopro/ directory
    $seofile = $mainframe->getCfg('absolute_path').'/includes/seopro/com_eventcalendar.php';
    if (file_exists($seofile)) { $fmanager->deleteFile($seofile); }

    //Remove the IOS Sitemap extension from the IOS Sitemap extensions directory
    $sitemapfile = $mainframe->getCfg('absolute_path').'/administrator/components/com_sitemap/extensions/eventcalendar.sitemap.php';
    if (file_exists($sitemapfile)) { $fmanager->deleteFile($sitemapfile); }
}

?>
